﻿using System.Windows.Forms;
using VintageStudios.Planar2D.Shielder;

namespace Shielder
{
    public class Item : ListViewItem, IPrintable
    {
        private ItemInfo info;
        private IndexedImage img;

        /// <summary>
        /// Returns the ItemInfo found within this Item class.
        /// </summary>
        public ItemInfo ItemInfo
        {
            get
            {
                return info;
            }
        }
        public Item(ItemInfo info, IndexedImage img)
        {
            this.info = info;
            this.Text = info.Name;
            this.img = img;
        }
        public override string ToString()
        {
            return info.Name;
        }
        public string Print()
        {
            return info.Print();
        }
    }
}
