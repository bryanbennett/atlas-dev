﻿using System.Windows.Forms;
using VintageStudios.Planar2D.Shielder;

namespace Shielder
{
    public class Monster : ListViewItem
    {
        private MonsterInfo info;
        private IndexedImage sprite;

        //properties
        public MonsterInfo MonsterInfo
        {
            get
            {
                return info;
            }
        }
        
        public Monster(MonsterInfo info, IndexedImage sprite)
        {
            this.info = info;
            this.Text = info.MonsterName;
            this.sprite = sprite;
        }
        public string Print()
        {
            return info.Print();
        }
        public override string ToString()
        {
            return info.MonsterName;
        }
    }
}
