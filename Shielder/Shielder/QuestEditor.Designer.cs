﻿namespace Shielder
{
    partial class QuestEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.rtbPrompt = new System.Windows.Forms.RichTextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.numericMinLevel = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxItemReward = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numericGoldReward = new System.Windows.Forms.NumericUpDown();
            this.numericXPReward = new System.Windows.Forms.NumericUpDown();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radKill = new System.Windows.Forms.RadioButton();
            this.radCollect = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbxTargetMonster = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbxTargetItem = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericMinLevel)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericGoldReward)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericXPReward)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(126, 7);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 1;
            // 
            // rtbPrompt
            // 
            this.rtbPrompt.Location = new System.Drawing.Point(12, 86);
            this.rtbPrompt.Name = "rtbPrompt";
            this.rtbPrompt.Size = new System.Drawing.Size(689, 96);
            this.rtbPrompt.TabIndex = 3;
            this.rtbPrompt.Text = "<prompt>";
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(545, 316);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save Quest";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(626, 316);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // numericMinLevel
            // 
            this.numericMinLevel.Location = new System.Drawing.Point(168, 37);
            this.numericMinLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMinLevel.Name = "numericMinLevel";
            this.numericMinLevel.Size = new System.Drawing.Size(58, 20);
            this.numericMinLevel.TabIndex = 7;
            this.numericMinLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(82, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Minimum Level:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericXPReward);
            this.groupBox1.Controls.Add(this.numericGoldReward);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbxItemReward);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 188);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 122);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rewards";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Item:";
            // 
            // cbxItemReward
            // 
            this.cbxItemReward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxItemReward.FormattingEnabled = true;
            this.cbxItemReward.Location = new System.Drawing.Point(75, 18);
            this.cbxItemReward.Name = "cbxItemReward";
            this.cbxItemReward.Size = new System.Drawing.Size(183, 21);
            this.cbxItemReward.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Gold:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Experience:";
            // 
            // numericGoldReward
            // 
            this.numericGoldReward.Location = new System.Drawing.Point(73, 49);
            this.numericGoldReward.Maximum = new decimal(new int[] {
            1241513983,
            370409800,
            542101,
            0});
            this.numericGoldReward.Name = "numericGoldReward";
            this.numericGoldReward.Size = new System.Drawing.Size(185, 20);
            this.numericGoldReward.TabIndex = 10;
            // 
            // numericXPReward
            // 
            this.numericXPReward.Location = new System.Drawing.Point(73, 76);
            this.numericXPReward.Maximum = new decimal(new int[] {
            -402653185,
            -1613725636,
            54210108,
            0});
            this.numericXPReward.Name = "numericXPReward";
            this.numericXPReward.Size = new System.Drawing.Size(185, 20);
            this.numericXPReward.TabIndex = 14;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Shielder.Properties.Resources.Parchment;
            this.pictureBox1.Location = new System.Drawing.Point(12, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radCollect);
            this.groupBox2.Controls.Add(this.radKill);
            this.groupBox2.Location = new System.Drawing.Point(299, 188);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(128, 122);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Condition Type:";
            // 
            // radKill
            // 
            this.radKill.AutoSize = true;
            this.radKill.Checked = true;
            this.radKill.Location = new System.Drawing.Point(18, 21);
            this.radKill.Name = "radKill";
            this.radKill.Size = new System.Drawing.Size(38, 17);
            this.radKill.TabIndex = 0;
            this.radKill.TabStop = true;
            this.radKill.Text = "Kill";
            this.radKill.UseVisualStyleBackColor = true;
            this.radKill.CheckedChanged += new System.EventHandler(this.radKill_CheckedChanged);
            // 
            // radCollect
            // 
            this.radCollect.AutoSize = true;
            this.radCollect.Location = new System.Drawing.Point(18, 44);
            this.radCollect.Name = "radCollect";
            this.radCollect.Size = new System.Drawing.Size(57, 17);
            this.radCollect.TabIndex = 1;
            this.radCollect.Text = "Collect";
            this.radCollect.UseVisualStyleBackColor = true;
            this.radCollect.CheckedChanged += new System.EventHandler(this.radCollect_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbxTargetItem);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.cbxTargetMonster);
            this.groupBox3.Location = new System.Drawing.Point(433, 188);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(268, 122);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Target Object:";
            // 
            // cbxTargetMonster
            // 
            this.cbxTargetMonster.FormattingEnabled = true;
            this.cbxTargetMonster.Location = new System.Drawing.Point(69, 15);
            this.cbxTargetMonster.Name = "cbxTargetMonster";
            this.cbxTargetMonster.Size = new System.Drawing.Size(191, 21);
            this.cbxTargetMonster.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Monster:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Item:";
            // 
            // cbxTargetItem
            // 
            this.cbxTargetItem.Enabled = false;
            this.cbxTargetItem.FormattingEnabled = true;
            this.cbxTargetItem.Location = new System.Drawing.Point(69, 41);
            this.cbxTargetItem.Name = "cbxTargetItem";
            this.cbxTargetItem.Size = new System.Drawing.Size(191, 21);
            this.cbxTargetItem.TabIndex = 16;
            // 
            // QuestEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 345);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericMinLevel);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.rtbPrompt);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "QuestEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quest Editor";
            this.Load += new System.EventHandler(this.QuestEditor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericMinLevel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericGoldReward)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericXPReward)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.RichTextBox rtbPrompt;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.NumericUpDown numericMinLevel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxItemReward;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericXPReward;
        private System.Windows.Forms.NumericUpDown numericGoldReward;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radCollect;
        private System.Windows.Forms.RadioButton radKill;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbxTargetItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbxTargetMonster;
    }
}