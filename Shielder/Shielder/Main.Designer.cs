﻿namespace Shielder
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newPackageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openPackageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePackageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resourcesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setItemPathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setSpritePathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.mEditor = new System.Windows.Forms.TabPage();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btnNewMob = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDeleteSelectedMob = new System.Windows.Forms.ToolStripButton();
            this.listViewMonster = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imgMonster = new System.Windows.Forms.ImageList(this.components);
            this.iEditor = new System.Windows.Forms.TabPage();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnNewItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDeleteSelectedItem = new System.Windows.Forms.ToolStripButton();
            this.listViewItem = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imgItem = new System.Windows.Forms.ImageList(this.components);
            this.qEditor = new System.Windows.Forms.TabPage();
            this.listViewQuest = new System.Windows.Forms.ListView();
            this.imgQuest = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.btnNewQuest = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDeleteQuest = new System.Windows.Forms.ToolStripButton();
            this.pInformation = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblItems = new System.Windows.Forms.Label();
            this.lblPackageName = new System.Windows.Forms.Label();
            this.lblMonsters = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.dialogPackage = new System.Windows.Forms.OpenFileDialog();
            this.folderDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.folderDialog2 = new System.Windows.Forms.FolderBrowserDialog();
            this.label4 = new System.Windows.Forms.Label();
            this.lblQuests = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblItemPath = new System.Windows.Forms.Label();
            this.lblSpritePath = new System.Windows.Forms.Label();
            this.menuStrip.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.mEditor.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.iEditor.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.qEditor.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.pInformation.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.resourcesToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(987, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newPackageToolStripMenuItem,
            this.openPackageToolStripMenuItem,
            this.savePackageToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newPackageToolStripMenuItem
            // 
            this.newPackageToolStripMenuItem.Name = "newPackageToolStripMenuItem";
            this.newPackageToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.newPackageToolStripMenuItem.Text = "New Package";
            this.newPackageToolStripMenuItem.Click += new System.EventHandler(this.newPackageToolStripMenuItem_Click);
            // 
            // openPackageToolStripMenuItem
            // 
            this.openPackageToolStripMenuItem.Name = "openPackageToolStripMenuItem";
            this.openPackageToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.openPackageToolStripMenuItem.Text = "Open Package";
            this.openPackageToolStripMenuItem.Click += new System.EventHandler(this.openPackageToolStripMenuItem_Click);
            // 
            // savePackageToolStripMenuItem
            // 
            this.savePackageToolStripMenuItem.Enabled = false;
            this.savePackageToolStripMenuItem.Name = "savePackageToolStripMenuItem";
            this.savePackageToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.savePackageToolStripMenuItem.Text = "Save Package";
            this.savePackageToolStripMenuItem.Click += new System.EventHandler(this.savePackageToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // resourcesToolStripMenuItem
            // 
            this.resourcesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setItemPathToolStripMenuItem,
            this.setSpritePathToolStripMenuItem});
            this.resourcesToolStripMenuItem.Name = "resourcesToolStripMenuItem";
            this.resourcesToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.resourcesToolStripMenuItem.Text = "Resources";
            // 
            // setItemPathToolStripMenuItem
            // 
            this.setItemPathToolStripMenuItem.Name = "setItemPathToolStripMenuItem";
            this.setItemPathToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.setItemPathToolStripMenuItem.Text = "Set Item Path";
            this.setItemPathToolStripMenuItem.Click += new System.EventHandler(this.setItemPathToolStripMenuItem_Click);
            // 
            // setSpritePathToolStripMenuItem
            // 
            this.setSpritePathToolStripMenuItem.Name = "setSpritePathToolStripMenuItem";
            this.setSpritePathToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.setSpritePathToolStripMenuItem.Text = "Set Sprite Path";
            this.setSpritePathToolStripMenuItem.Click += new System.EventHandler(this.setSpritePathToolStripMenuItem_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.mEditor);
            this.tabControl.Controls.Add(this.iEditor);
            this.tabControl.Controls.Add(this.qEditor);
            this.tabControl.Controls.Add(this.pInformation);
            this.tabControl.Location = new System.Drawing.Point(12, 28);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(963, 579);
            this.tabControl.TabIndex = 1;
            this.tabControl.Visible = false;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // mEditor
            // 
            this.mEditor.Controls.Add(this.toolStrip2);
            this.mEditor.Controls.Add(this.listViewMonster);
            this.mEditor.Location = new System.Drawing.Point(4, 22);
            this.mEditor.Name = "mEditor";
            this.mEditor.Padding = new System.Windows.Forms.Padding(3);
            this.mEditor.Size = new System.Drawing.Size(955, 553);
            this.mEditor.TabIndex = 0;
            this.mEditor.Text = "Monster Editor";
            this.mEditor.UseVisualStyleBackColor = true;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNewMob,
            this.toolStripSeparator1,
            this.btnDeleteSelectedMob});
            this.toolStrip2.Location = new System.Drawing.Point(3, 3);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(949, 25);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btnNewMob
            // 
            this.btnNewMob.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnNewMob.Image = ((System.Drawing.Image)(resources.GetObject("btnNewMob.Image")));
            this.btnNewMob.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNewMob.Name = "btnNewMob";
            this.btnNewMob.Size = new System.Drawing.Size(63, 22);
            this.btnNewMob.Text = "New Mob";
            this.btnNewMob.Click += new System.EventHandler(this.btnNewMob_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDeleteSelectedMob
            // 
            this.btnDeleteSelectedMob.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDeleteSelectedMob.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSelectedMob.Image")));
            this.btnDeleteSelectedMob.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeleteSelectedMob.Name = "btnDeleteSelectedMob";
            this.btnDeleteSelectedMob.Size = new System.Drawing.Size(91, 22);
            this.btnDeleteSelectedMob.Text = "Delete Selected";
            this.btnDeleteSelectedMob.Click += new System.EventHandler(this.btnDeleteSelectedMob_Click);
            // 
            // listViewMonster
            // 
            this.listViewMonster.BackColor = System.Drawing.SystemColors.Window;
            this.listViewMonster.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listViewMonster.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listViewMonster.GridLines = true;
            this.listViewMonster.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewMonster.LargeImageList = this.imgMonster;
            this.listViewMonster.Location = new System.Drawing.Point(6, 31);
            this.listViewMonster.MultiSelect = false;
            this.listViewMonster.Name = "listViewMonster";
            this.listViewMonster.Size = new System.Drawing.Size(946, 540);
            this.listViewMonster.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewMonster.TabIndex = 0;
            this.listViewMonster.UseCompatibleStateImageBehavior = false;
            this.listViewMonster.DoubleClick += new System.EventHandler(this.listViewMonster_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 92;
            // 
            // imgMonster
            // 
            this.imgMonster.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imgMonster.ImageSize = new System.Drawing.Size(96, 128);
            this.imgMonster.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // iEditor
            // 
            this.iEditor.Controls.Add(this.toolStrip1);
            this.iEditor.Controls.Add(this.listViewItem);
            this.iEditor.Location = new System.Drawing.Point(4, 22);
            this.iEditor.Name = "iEditor";
            this.iEditor.Padding = new System.Windows.Forms.Padding(3);
            this.iEditor.Size = new System.Drawing.Size(955, 553);
            this.iEditor.TabIndex = 1;
            this.iEditor.Text = "Item Editor";
            this.iEditor.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNewItem,
            this.toolStripSeparator3,
            this.btnDeleteSelectedItem});
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(949, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnNewItem
            // 
            this.btnNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnNewItem.Image = ((System.Drawing.Image)(resources.GetObject("btnNewItem.Image")));
            this.btnNewItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNewItem.Name = "btnNewItem";
            this.btnNewItem.Size = new System.Drawing.Size(62, 22);
            this.btnNewItem.Text = "New Item";
            this.btnNewItem.Click += new System.EventHandler(this.btnNewItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDeleteSelectedItem
            // 
            this.btnDeleteSelectedItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDeleteSelectedItem.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteSelectedItem.Image")));
            this.btnDeleteSelectedItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeleteSelectedItem.Name = "btnDeleteSelectedItem";
            this.btnDeleteSelectedItem.Size = new System.Drawing.Size(91, 22);
            this.btnDeleteSelectedItem.Text = "Delete Selected";
            this.btnDeleteSelectedItem.Click += new System.EventHandler(this.btnDeleteSelectedItem_Click);
            // 
            // listViewItem
            // 
            this.listViewItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listViewItem.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.listViewItem.GridLines = true;
            this.listViewItem.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewItem.LargeImageList = this.imgItem;
            this.listViewItem.Location = new System.Drawing.Point(7, 31);
            this.listViewItem.MultiSelect = false;
            this.listViewItem.Name = "listViewItem";
            this.listViewItem.Size = new System.Drawing.Size(942, 534);
            this.listViewItem.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewItem.TabIndex = 1;
            this.listViewItem.TileSize = new System.Drawing.Size(208, 32);
            this.listViewItem.UseCompatibleStateImageBehavior = false;
            this.listViewItem.DoubleClick += new System.EventHandler(this.listViewItem_DoubleClick);
            // 
            // imgItem
            // 
            this.imgItem.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imgItem.ImageSize = new System.Drawing.Size(32, 32);
            this.imgItem.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // qEditor
            // 
            this.qEditor.Controls.Add(this.listViewQuest);
            this.qEditor.Controls.Add(this.toolStrip3);
            this.qEditor.Location = new System.Drawing.Point(4, 22);
            this.qEditor.Name = "qEditor";
            this.qEditor.Padding = new System.Windows.Forms.Padding(3);
            this.qEditor.Size = new System.Drawing.Size(955, 553);
            this.qEditor.TabIndex = 3;
            this.qEditor.Text = "Quest Editor";
            this.qEditor.UseVisualStyleBackColor = true;
            // 
            // listViewQuest
            // 
            this.listViewQuest.LargeImageList = this.imgQuest;
            this.listViewQuest.Location = new System.Drawing.Point(6, 31);
            this.listViewQuest.MultiSelect = false;
            this.listViewQuest.Name = "listViewQuest";
            this.listViewQuest.Size = new System.Drawing.Size(943, 516);
            this.listViewQuest.TabIndex = 1;
            this.listViewQuest.UseCompatibleStateImageBehavior = false;
            this.listViewQuest.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewQuest_MouseDoubleClick);
            // 
            // imgQuest
            // 
            this.imgQuest.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgQuest.ImageStream")));
            this.imgQuest.TransparentColor = System.Drawing.Color.Transparent;
            this.imgQuest.Images.SetKeyName(0, "Parchment.png");
            // 
            // toolStrip3
            // 
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNewQuest,
            this.toolStripSeparator2,
            this.btnDeleteQuest});
            this.toolStrip3.Location = new System.Drawing.Point(3, 3);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(949, 25);
            this.toolStrip3.TabIndex = 0;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // btnNewQuest
            // 
            this.btnNewQuest.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnNewQuest.Image = ((System.Drawing.Image)(resources.GetObject("btnNewQuest.Image")));
            this.btnNewQuest.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNewQuest.Name = "btnNewQuest";
            this.btnNewQuest.Size = new System.Drawing.Size(69, 22);
            this.btnNewQuest.Text = "New Quest";
            this.btnNewQuest.Click += new System.EventHandler(this.btnNewQuest_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDeleteQuest
            // 
            this.btnDeleteQuest.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDeleteQuest.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteQuest.Image")));
            this.btnDeleteQuest.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeleteQuest.Name = "btnDeleteQuest";
            this.btnDeleteQuest.Size = new System.Drawing.Size(91, 22);
            this.btnDeleteQuest.Text = "Delete Selected";
            this.btnDeleteQuest.Click += new System.EventHandler(this.btnDeleteQuest_Click);
            // 
            // pInformation
            // 
            this.pInformation.Controls.Add(this.groupBox1);
            this.pInformation.Location = new System.Drawing.Point(4, 22);
            this.pInformation.Name = "pInformation";
            this.pInformation.Size = new System.Drawing.Size(955, 553);
            this.pInformation.TabIndex = 2;
            this.pInformation.Text = "Package Information";
            this.pInformation.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblSpritePath);
            this.groupBox1.Controls.Add(this.lblItemPath);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblQuests);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lblItems);
            this.groupBox1.Controls.Add(this.lblPackageName);
            this.groupBox1.Controls.Add(this.lblMonsters);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(3, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(929, 177);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Package Contents";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Package Name:";
            // 
            // lblItems
            // 
            this.lblItems.AutoSize = true;
            this.lblItems.Location = new System.Drawing.Point(111, 126);
            this.lblItems.Name = "lblItems";
            this.lblItems.Size = new System.Drawing.Size(27, 13);
            this.lblItems.TabIndex = 5;
            this.lblItems.Text = "N/A";
            // 
            // lblPackageName
            // 
            this.lblPackageName.AutoSize = true;
            this.lblPackageName.Location = new System.Drawing.Point(111, 16);
            this.lblPackageName.Name = "lblPackageName";
            this.lblPackageName.Size = new System.Drawing.Size(27, 13);
            this.lblPackageName.TabIndex = 1;
            this.lblPackageName.Text = "N/A";
            // 
            // lblMonsters
            // 
            this.lblMonsters.AutoSize = true;
            this.lblMonsters.Location = new System.Drawing.Point(111, 101);
            this.lblMonsters.Name = "lblMonsters";
            this.lblMonsters.Size = new System.Drawing.Size(27, 13);
            this.lblMonsters.TabIndex = 4;
            this.lblMonsters.Text = "N/A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Monsters:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Items:";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 615);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(987, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(103, 17);
            this.lblStatus.Text = "No package open.";
            // 
            // dialogPackage
            // 
            this.dialogPackage.FileName = ".apx";
            this.dialogPackage.Filter = "Shielder files(.apx)|*.apx|All files|*.*";
            this.dialogPackage.Title = "Load Package";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(62, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Quests:";
            // 
            // lblQuests
            // 
            this.lblQuests.AutoSize = true;
            this.lblQuests.Location = new System.Drawing.Point(111, 149);
            this.lblQuests.Name = "lblQuests";
            this.lblQuests.Size = new System.Drawing.Size(27, 13);
            this.lblQuests.TabIndex = 7;
            this.lblQuests.Text = "N/A";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Item Folder Path:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Sprite Folder Path:";
            // 
            // lblItemPath
            // 
            this.lblItemPath.AutoSize = true;
            this.lblItemPath.Location = new System.Drawing.Point(111, 40);
            this.lblItemPath.Name = "lblItemPath";
            this.lblItemPath.Size = new System.Drawing.Size(27, 13);
            this.lblItemPath.TabIndex = 10;
            this.lblItemPath.Text = "N/A";
            // 
            // lblSpritePath
            // 
            this.lblSpritePath.AutoSize = true;
            this.lblSpritePath.Location = new System.Drawing.Point(111, 68);
            this.lblSpritePath.Name = "lblSpritePath";
            this.lblSpritePath.Size = new System.Drawing.Size(27, 13);
            this.lblSpritePath.TabIndex = 11;
            this.lblSpritePath.Text = "N/A";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 637);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shielder";
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.mEditor.ResumeLayout(false);
            this.mEditor.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.iEditor.ResumeLayout(false);
            this.iEditor.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.qEditor.ResumeLayout(false);
            this.qEditor.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.pInformation.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage mEditor;
        private System.Windows.Forms.ListView listViewMonster;
        private System.Windows.Forms.TabPage iEditor;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ImageList imgItem;
        private System.Windows.Forms.ListView listViewItem;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ImageList imgMonster;
        private System.Windows.Forms.TabPage pInformation;
        private System.Windows.Forms.ToolStripMenuItem newPackageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openPackageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePackageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btnNewMob;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnDeleteSelectedMob;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.OpenFileDialog dialogPackage;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnNewItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnDeleteSelectedItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblItems;
        private System.Windows.Forms.Label lblPackageName;
        private System.Windows.Forms.Label lblMonsters;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FolderBrowserDialog folderDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderDialog2;
        private System.Windows.Forms.ToolStripMenuItem resourcesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setItemPathToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setSpritePathToolStripMenuItem;
        private System.Windows.Forms.TabPage qEditor;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton btnNewQuest;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnDeleteQuest;
        private System.Windows.Forms.ListView listViewQuest;
        private System.Windows.Forms.ImageList imgQuest;
        private System.Windows.Forms.Label lblSpritePath;
        private System.Windows.Forms.Label lblItemPath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblQuests;
        private System.Windows.Forms.Label label4;

    }
}

