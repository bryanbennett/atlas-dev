﻿namespace Shielder
{
    partial class MonsterEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtMonsterName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericMonsterGold = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.numericExperience = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.numericMovtSpeed = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numericMonsterAttackSpeed = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numericMinDamage = new System.Windows.Forms.NumericUpDown();
            this.numericMaxDamage = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numericHealth = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxSpritesheet = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkPoisonStackable = new System.Windows.Forms.CheckBox();
            this.numericDropChance = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.numericTicksPerSecond = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.chkPoisonous = new System.Windows.Forms.CheckBox();
            this.numericDmgPerTick = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.cbxItemDropped = new System.Windows.Forms.ComboBox();
            this.chkItemDropper = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbxFamiliar = new System.Windows.Forms.ComboBox();
            this.chkSummoner = new System.Windows.Forms.CheckBox();
            this.chkInvincible = new System.Windows.Forms.CheckBox();
            this.chkHostile = new System.Windows.Forms.CheckBox();
            this.chkFlying = new System.Windows.Forms.CheckBox();
            this.btnSaveMob = new System.Windows.Forms.Button();
            this.btnMonsterCancel = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pbxMonster = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.numericLevel = new System.Windows.Forms.NumericUpDown();
            this.chkNPC = new System.Windows.Forms.CheckBox();
            this.chkStoic = new System.Windows.Forms.CheckBox();
            this.chkNightCrawler = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.numericStartTime = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.numericEndTime = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.numericAtkRange = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.numericSight = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericMonsterGold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericExperience)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMovtSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMonsterAttackSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMinDamage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxDamage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHealth)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericDropChance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTicksPerSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDmgPerTick)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMonster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAtkRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSight)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Spritesheet Preview:";
            // 
            // txtMonsterName
            // 
            this.txtMonsterName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMonsterName.Location = new System.Drawing.Point(85, 23);
            this.txtMonsterName.Name = "txtMonsterName";
            this.txtMonsterName.Size = new System.Drawing.Size(170, 20);
            this.txtMonsterName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Name:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.numericSight);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.numericAtkRange);
            this.groupBox1.Controls.Add(this.numericLevel);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.numericMonsterGold);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.numericExperience);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.numericMovtSpeed);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.numericMonsterAttackSpeed);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.numericMinDamage);
            this.groupBox1.Controls.Add(this.numericMaxDamage);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.numericHealth);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbxSpritesheet);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtMonsterName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(270, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(275, 439);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Properties";
            // 
            // numericMonsterGold
            // 
            this.numericMonsterGold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericMonsterGold.Location = new System.Drawing.Point(135, 287);
            this.numericMonsterGold.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericMonsterGold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMonsterGold.Name = "numericMonsterGold";
            this.numericMonsterGold.Size = new System.Drawing.Size(120, 20);
            this.numericMonsterGold.TabIndex = 20;
            this.numericMonsterGold.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(97, 289);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Gold:";
            // 
            // numericExperience
            // 
            this.numericExperience.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericExperience.Location = new System.Drawing.Point(135, 262);
            this.numericExperience.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericExperience.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericExperience.Name = "numericExperience";
            this.numericExperience.Size = new System.Drawing.Size(120, 20);
            this.numericExperience.TabIndex = 18;
            this.numericExperience.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(66, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Experience:";
            // 
            // numericMovtSpeed
            // 
            this.numericMovtSpeed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericMovtSpeed.DecimalPlaces = 2;
            this.numericMovtSpeed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericMovtSpeed.Location = new System.Drawing.Point(135, 236);
            this.numericMovtSpeed.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericMovtSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericMovtSpeed.Name = "numericMovtSpeed";
            this.numericMovtSpeed.Size = new System.Drawing.Size(120, 20);
            this.numericMovtSpeed.TabIndex = 16;
            this.numericMovtSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(59, 238);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Mov\'t Speed:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(54, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Attack Speed:";
            // 
            // numericMonsterAttackSpeed
            // 
            this.numericMonsterAttackSpeed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericMonsterAttackSpeed.Location = new System.Drawing.Point(135, 154);
            this.numericMonsterAttackSpeed.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericMonsterAttackSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMonsterAttackSpeed.Name = "numericMonsterAttackSpeed";
            this.numericMonsterAttackSpeed.Size = new System.Drawing.Size(120, 20);
            this.numericMonsterAttackSpeed.TabIndex = 13;
            this.numericMonsterAttackSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(59, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Min Damage:";
            // 
            // numericMinDamage
            // 
            this.numericMinDamage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericMinDamage.Location = new System.Drawing.Point(135, 128);
            this.numericMinDamage.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericMinDamage.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMinDamage.Name = "numericMinDamage";
            this.numericMinDamage.Size = new System.Drawing.Size(120, 20);
            this.numericMinDamage.TabIndex = 11;
            this.numericMinDamage.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericMaxDamage
            // 
            this.numericMaxDamage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericMaxDamage.Location = new System.Drawing.Point(135, 102);
            this.numericMaxDamage.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericMaxDamage.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMaxDamage.Name = "numericMaxDamage";
            this.numericMaxDamage.Size = new System.Drawing.Size(120, 20);
            this.numericMaxDamage.TabIndex = 10;
            this.numericMaxDamage.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(56, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Max Damage:";
            // 
            // numericHealth
            // 
            this.numericHealth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericHealth.Location = new System.Drawing.Point(135, 76);
            this.numericHealth.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericHealth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericHealth.Name = "numericHealth";
            this.numericHealth.Size = new System.Drawing.Size(120, 20);
            this.numericHealth.TabIndex = 8;
            this.numericHealth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(88, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Health:";
            // 
            // cbxSpritesheet
            // 
            this.cbxSpritesheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSpritesheet.Location = new System.Drawing.Point(85, 49);
            this.cbxSpritesheet.Name = "cbxSpritesheet";
            this.cbxSpritesheet.Size = new System.Drawing.Size(170, 21);
            this.cbxSpritesheet.Sorted = true;
            this.cbxSpritesheet.TabIndex = 6;
            this.cbxSpritesheet.SelectedIndexChanged += new System.EventHandler(this.cbxSpritesheet_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Spritesheet:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numericEndTime);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.numericStartTime);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.chkNightCrawler);
            this.groupBox2.Controls.Add(this.chkStoic);
            this.groupBox2.Controls.Add(this.chkNPC);
            this.groupBox2.Controls.Add(this.chkPoisonStackable);
            this.groupBox2.Controls.Add(this.numericDropChance);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.numericTicksPerSecond);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.chkPoisonous);
            this.groupBox2.Controls.Add(this.numericDmgPerTick);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cbxItemDropped);
            this.groupBox2.Controls.Add(this.chkItemDropper);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.cbxFamiliar);
            this.groupBox2.Controls.Add(this.chkSummoner);
            this.groupBox2.Controls.Add(this.chkInvincible);
            this.groupBox2.Controls.Add(this.chkHostile);
            this.groupBox2.Controls.Add(this.chkFlying);
            this.groupBox2.Location = new System.Drawing.Point(551, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(275, 439);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Behaviors";
            // 
            // chkPoisonStackable
            // 
            this.chkPoisonStackable.AutoSize = true;
            this.chkPoisonStackable.Enabled = false;
            this.chkPoisonStackable.Location = new System.Drawing.Point(149, 293);
            this.chkPoisonStackable.Name = "chkPoisonStackable";
            this.chkPoisonStackable.Size = new System.Drawing.Size(74, 17);
            this.chkPoisonStackable.TabIndex = 30;
            this.chkPoisonStackable.Text = "Stackable";
            this.chkPoisonStackable.UseVisualStyleBackColor = true;
            // 
            // numericDropChance
            // 
            this.numericDropChance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericDropChance.Enabled = false;
            this.numericDropChance.Location = new System.Drawing.Point(99, 192);
            this.numericDropChance.Name = "numericDropChance";
            this.numericDropChance.Size = new System.Drawing.Size(44, 20);
            this.numericDropChance.TabIndex = 23;
            this.numericDropChance.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(52, 194);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "%Drop:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(48, 269);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Ticks Per Second:";
            // 
            // numericTicksPerSecond
            // 
            this.numericTicksPerSecond.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericTicksPerSecond.Enabled = false;
            this.numericTicksPerSecond.Location = new System.Drawing.Point(149, 267);
            this.numericTicksPerSecond.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericTicksPerSecond.Name = "numericTicksPerSecond";
            this.numericTicksPerSecond.Size = new System.Drawing.Size(120, 20);
            this.numericTicksPerSecond.TabIndex = 28;
            this.numericTicksPerSecond.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(51, 243);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "Damage per Tick:";
            // 
            // chkPoisonous
            // 
            this.chkPoisonous.AutoSize = true;
            this.chkPoisonous.Location = new System.Drawing.Point(12, 221);
            this.chkPoisonous.Name = "chkPoisonous";
            this.chkPoisonous.Size = new System.Drawing.Size(75, 17);
            this.chkPoisonous.TabIndex = 27;
            this.chkPoisonous.Text = "Poisonous";
            this.chkPoisonous.UseVisualStyleBackColor = true;
            this.chkPoisonous.CheckedChanged += new System.EventHandler(this.chkPoisonous_CheckedChanged);
            // 
            // numericDmgPerTick
            // 
            this.numericDmgPerTick.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericDmgPerTick.Enabled = false;
            this.numericDmgPerTick.Location = new System.Drawing.Point(149, 241);
            this.numericDmgPerTick.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericDmgPerTick.Name = "numericDmgPerTick";
            this.numericDmgPerTick.Size = new System.Drawing.Size(120, 20);
            this.numericDmgPerTick.TabIndex = 22;
            this.numericDmgPerTick.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(63, 167);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Item:";
            // 
            // cbxItemDropped
            // 
            this.cbxItemDropped.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxItemDropped.Enabled = false;
            this.cbxItemDropped.Location = new System.Drawing.Point(99, 164);
            this.cbxItemDropped.Name = "cbxItemDropped";
            this.cbxItemDropped.Size = new System.Drawing.Size(170, 21);
            this.cbxItemDropped.Sorted = true;
            this.cbxItemDropped.TabIndex = 25;
            // 
            // chkItemDropper
            // 
            this.chkItemDropper.AutoSize = true;
            this.chkItemDropper.Location = new System.Drawing.Point(12, 141);
            this.chkItemDropper.Name = "chkItemDropper";
            this.chkItemDropper.Size = new System.Drawing.Size(87, 17);
            this.chkItemDropper.TabIndex = 24;
            this.chkItemDropper.Text = "Item Dropper";
            this.chkItemDropper.UseVisualStyleBackColor = true;
            this.chkItemDropper.CheckedChanged += new System.EventHandler(this.chkItemDropper_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(48, 115);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Familiar:";
            // 
            // cbxFamiliar
            // 
            this.cbxFamiliar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxFamiliar.Enabled = false;
            this.cbxFamiliar.Location = new System.Drawing.Point(99, 112);
            this.cbxFamiliar.Name = "cbxFamiliar";
            this.cbxFamiliar.Size = new System.Drawing.Size(170, 21);
            this.cbxFamiliar.Sorted = true;
            this.cbxFamiliar.TabIndex = 22;
            // 
            // chkSummoner
            // 
            this.chkSummoner.AutoSize = true;
            this.chkSummoner.Location = new System.Drawing.Point(12, 88);
            this.chkSummoner.Name = "chkSummoner";
            this.chkSummoner.Size = new System.Drawing.Size(76, 17);
            this.chkSummoner.TabIndex = 3;
            this.chkSummoner.Text = "Summoner";
            this.chkSummoner.UseVisualStyleBackColor = true;
            this.chkSummoner.CheckedChanged += new System.EventHandler(this.chkSummoner_CheckedChanged);
            // 
            // chkInvincible
            // 
            this.chkInvincible.AutoSize = true;
            this.chkInvincible.Location = new System.Drawing.Point(12, 65);
            this.chkInvincible.Name = "chkInvincible";
            this.chkInvincible.Size = new System.Drawing.Size(71, 17);
            this.chkInvincible.TabIndex = 2;
            this.chkInvincible.Text = "Invincible";
            this.chkInvincible.UseVisualStyleBackColor = true;
            // 
            // chkHostile
            // 
            this.chkHostile.AutoSize = true;
            this.chkHostile.Checked = true;
            this.chkHostile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHostile.Location = new System.Drawing.Point(12, 19);
            this.chkHostile.Name = "chkHostile";
            this.chkHostile.Size = new System.Drawing.Size(58, 17);
            this.chkHostile.TabIndex = 1;
            this.chkHostile.Text = "Hostile";
            this.chkHostile.UseVisualStyleBackColor = true;
            // 
            // chkFlying
            // 
            this.chkFlying.AutoSize = true;
            this.chkFlying.Location = new System.Drawing.Point(12, 42);
            this.chkFlying.Name = "chkFlying";
            this.chkFlying.Size = new System.Drawing.Size(53, 17);
            this.chkFlying.TabIndex = 0;
            this.chkFlying.Text = "Flying";
            this.chkFlying.UseVisualStyleBackColor = true;
            // 
            // btnSaveMob
            // 
            this.btnSaveMob.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveMob.Location = new System.Drawing.Point(590, 470);
            this.btnSaveMob.Name = "btnSaveMob";
            this.btnSaveMob.Size = new System.Drawing.Size(98, 23);
            this.btnSaveMob.TabIndex = 7;
            this.btnSaveMob.Text = "Save Monster";
            this.btnSaveMob.UseVisualStyleBackColor = true;
            this.btnSaveMob.Click += new System.EventHandler(this.btnSaveMob_Click);
            // 
            // btnMonsterCancel
            // 
            this.btnMonsterCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMonsterCancel.Location = new System.Drawing.Point(722, 470);
            this.btnMonsterCancel.Name = "btnMonsterCancel";
            this.btnMonsterCancel.Size = new System.Drawing.Size(98, 23);
            this.btnMonsterCancel.TabIndex = 8;
            this.btnMonsterCancel.Text = "Cancel";
            this.btnMonsterCancel.UseVisualStyleBackColor = true;
            this.btnMonsterCancel.Click += new System.EventHandler(this.btnMonsterCancel_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackgroundImage = global::Shielder.Properties.Resources.grassback;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.pbxMonster);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 25);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(252, 389);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // pbxMonster
            // 
            this.pbxMonster.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxMonster.BackColor = System.Drawing.Color.Transparent;
            this.pbxMonster.Location = new System.Drawing.Point(3, 3);
            this.pbxMonster.Name = "pbxMonster";
            this.pbxMonster.Size = new System.Drawing.Size(240, 384);
            this.pbxMonster.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxMonster.TabIndex = 0;
            this.pbxMonster.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(97, 318);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 21;
            this.label16.Text = "Level:";
            // 
            // numericLevel
            // 
            this.numericLevel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericLevel.Location = new System.Drawing.Point(135, 316);
            this.numericLevel.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericLevel.Name = "numericLevel";
            this.numericLevel.Size = new System.Drawing.Size(120, 20);
            this.numericLevel.TabIndex = 22;
            this.numericLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chkNPC
            // 
            this.chkNPC.AutoSize = true;
            this.chkNPC.Location = new System.Drawing.Point(99, 19);
            this.chkNPC.Name = "chkNPC";
            this.chkNPC.Size = new System.Drawing.Size(48, 17);
            this.chkNPC.TabIndex = 31;
            this.chkNPC.Text = "NPC";
            this.chkNPC.UseVisualStyleBackColor = true;
            // 
            // chkStoic
            // 
            this.chkStoic.AutoSize = true;
            this.chkStoic.Location = new System.Drawing.Point(99, 42);
            this.chkStoic.Name = "chkStoic";
            this.chkStoic.Size = new System.Drawing.Size(50, 17);
            this.chkStoic.TabIndex = 32;
            this.chkStoic.Text = "Stoic";
            this.chkStoic.UseVisualStyleBackColor = true;
            // 
            // chkNightCrawler
            // 
            this.chkNightCrawler.AutoSize = true;
            this.chkNightCrawler.Location = new System.Drawing.Point(12, 317);
            this.chkNightCrawler.Name = "chkNightCrawler";
            this.chkNightCrawler.Size = new System.Drawing.Size(86, 17);
            this.chkNightCrawler.TabIndex = 33;
            this.chkNightCrawler.Text = "NightCrawler";
            this.chkNightCrawler.UseVisualStyleBackColor = true;
            this.chkNightCrawler.CheckedChanged += new System.EventHandler(this.chkNightCrawler_CheckedChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(34, 341);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Between";
            // 
            // numericStartTime
            // 
            this.numericStartTime.Enabled = false;
            this.numericStartTime.Location = new System.Drawing.Point(84, 339);
            this.numericStartTime.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.numericStartTime.Name = "numericStartTime";
            this.numericStartTime.Size = new System.Drawing.Size(50, 20);
            this.numericStartTime.TabIndex = 35;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(136, 341);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(25, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "and";
            // 
            // numericEndTime
            // 
            this.numericEndTime.Enabled = false;
            this.numericEndTime.Location = new System.Drawing.Point(167, 339);
            this.numericEndTime.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.numericEndTime.Name = "numericEndTime";
            this.numericEndTime.Size = new System.Drawing.Size(50, 20);
            this.numericEndTime.TabIndex = 37;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(54, 183);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(76, 13);
            this.label19.TabIndex = 24;
            this.label19.Text = "Attack Range:";
            // 
            // numericAtkRange
            // 
            this.numericAtkRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericAtkRange.Location = new System.Drawing.Point(135, 181);
            this.numericAtkRange.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericAtkRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericAtkRange.Name = "numericAtkRange";
            this.numericAtkRange.Size = new System.Drawing.Size(120, 20);
            this.numericAtkRange.TabIndex = 23;
            this.numericAtkRange.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(95, 209);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 13);
            this.label20.TabIndex = 26;
            this.label20.Text = "Sight:";
            // 
            // numericSight
            // 
            this.numericSight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numericSight.Location = new System.Drawing.Point(135, 207);
            this.numericSight.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericSight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericSight.Name = "numericSight";
            this.numericSight.Size = new System.Drawing.Size(120, 20);
            this.numericSight.TabIndex = 25;
            this.numericSight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // MonsterEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(832, 503);
            this.Controls.Add(this.btnMonsterCancel);
            this.Controls.Add(this.btnSaveMob);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MonsterEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Monster Editor";
            this.Load += new System.EventHandler(this.MonsterEditor_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericMonsterGold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericExperience)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMovtSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMonsterAttackSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMinDamage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxDamage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericHealth)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericDropChance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTicksPerSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDmgPerTick)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMonster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAtkRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxMonster;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TextBox txtMonsterName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxSpritesheet;
        private System.Windows.Forms.NumericUpDown numericMonsterGold;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericExperience;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericMovtSpeed;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericMonsterAttackSpeed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericMinDamage;
        private System.Windows.Forms.NumericUpDown numericMaxDamage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericHealth;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbxFamiliar;
        private System.Windows.Forms.CheckBox chkSummoner;
        private System.Windows.Forms.CheckBox chkInvincible;
        private System.Windows.Forms.CheckBox chkHostile;
        private System.Windows.Forms.CheckBox chkFlying;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbxItemDropped;
        private System.Windows.Forms.CheckBox chkItemDropper;
        private System.Windows.Forms.CheckBox chkPoisonStackable;
        private System.Windows.Forms.NumericUpDown numericDropChance;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numericTicksPerSecond;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkPoisonous;
        private System.Windows.Forms.NumericUpDown numericDmgPerTick;
        private System.Windows.Forms.Button btnSaveMob;
        private System.Windows.Forms.Button btnMonsterCancel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown numericSight;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown numericAtkRange;
        private System.Windows.Forms.NumericUpDown numericLevel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numericEndTime;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numericStartTime;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkNightCrawler;
        private System.Windows.Forms.CheckBox chkStoic;
        private System.Windows.Forms.CheckBox chkNPC;
    }
}