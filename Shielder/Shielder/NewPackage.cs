﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shielder
{
    public partial class NewPackage : Form
    {
        private Main main;
        public NewPackage(Main main)
        {
            InitializeComponent();
            this.main = main;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string name = txtPackageName.Text;
            string location = txtLocation.Text;

            if (name.Equals("") || location.Equals(""))
            {
                MessageBox.Show("You must enter in a package name and a save location.", "Required Input");
                return;
            }

            main.PackageName = name;
            main.SaveLocation = location;
            main.PackageIsOpen = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            dialog.ShowDialog();
            txtLocation.Text = dialog.SelectedPath;
        }

        private void NewPackage_Load(object sender, EventArgs e)
        {

        }
    }
}
