﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Shielder.Properties;
using System.Resources;
using System.Globalization;
using System.Collections;
using System.Reflection;
using System.Diagnostics;
using System.Deployment.Application;
using System.IO;
using System.Text.RegularExpressions;
using VintageStudios.Planar2D.Shielder;

namespace Shielder
{
    public partial class Main : Form
    {
        private bool packageIsOpen = false;
        private string saveLocation;
        private string packageName;
        private Dictionary<String, IndexedImage> spritesheets;
        private Dictionary<String, IndexedImage> itemImages;
        private string pathToItems;
        private string pathToSprites;
        private string relativePath;
        private string[] itemFiles;
        private string[] spriteFiles;
        private string settingsPath;

        public Main()
        {
            InitializeComponent();
            ValidateEnvironment();
        }
        private void ValidateEnvironment()
        {
            relativePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            settingsPath = relativePath + "\\settings.txt";
            bool environmentInit;

            if (File.Exists(settingsPath))
            {
                environmentInit = true;
            }
            else
            {
                environmentInit = false;
            }

            if (!environmentInit)
            {
                MessageBox.Show("Your Shielder environment has not been setup yet for first time use.  Follow the directions within the following dialogs.");
                ChoosePathToItemFolder();
                ChoosePathToSpriteFolder();
                SaveSettings();
                LoadItemImages();
                LoadSpriteImages();
            }
            else
            {
                UserSettings settings = new UserSettings(settingsPath);
                pathToItems = settings.ItemPath;
                pathToSprites = settings.SpritePath;
                LoadItemImages();
                LoadSpriteImages();
            }
        }
        private void SaveSettings()
        {
            string[] print = new string[2];
            string temp = "ItemPath = " + pathToItems;
            print[0] = temp;
            temp = "SpritePath = " + pathToSprites;
            print[1] = temp;
            System.IO.File.WriteAllLines(@settingsPath, print);
        }
        private void ChoosePathToItemFolder()
        {
            folderDialog1.Description = "Please select a folder to use as the folder for the images of items.  This is where all images for items should be stored.";
            if (folderDialog1.ShowDialog() == DialogResult.OK)
            {
                pathToItems = folderDialog1.SelectedPath;
                Properties.Settings.Default.pathToItems = pathToItems;
                Properties.Settings.Default.Save();
            }
        }
        private void ChoosePathToSpriteFolder()
        {
            folderDialog2.Description = "Please select a folder to use as the folder for the images of spritesheets.  This is where all images for spritesheets should be stored.";
            if (folderDialog2.ShowDialog() == DialogResult.OK)
            {
                pathToSprites = folderDialog2.SelectedPath;
                Properties.Settings.Default.pathToSprites = pathToSprites;
                Properties.Settings.Default.Save();
            }
        }
        private void LoadItemImages()
        {
            itemImages = new Dictionary<string, IndexedImage>();
            itemFiles = Directory.GetFiles(pathToItems);
            int count = itemFiles.Length;

            for (int i = 0; i < count; i++)
            {
                string keyPath = itemFiles[i];
                string[] split = keyPath.Split('\\');
                string key = split[split.Length - 1];
                Image img = Image.FromFile(keyPath);
                IndexedImage temp = new IndexedImage(img, key);
                itemImages.Add(key, temp);
                imgItem.Images.Add(key, img);
            }
        }
        private void LoadSpriteImages()
        {
            spritesheets = new Dictionary<string, IndexedImage>();
            spriteFiles = Directory.GetFiles(pathToSprites);
            int count = spriteFiles.Length;

            for (int i = 0; i < count; i++)
            {
                string keyPath = spriteFiles[i];
                string[] split = keyPath.Split('\\');
                string key = split[split.Length - 1];
                Image img = Image.FromFile(keyPath);
                IndexedImage temp = new IndexedImage(img, key);
                spritesheets.Add(key, temp);
                imgMonster.Images.Add(key, img);
            }
        }
        public Dictionary<string, IndexedImage> getSpriteSheetList()
        {
            return spritesheets;
        }
        public Dictionary<string, IndexedImage> getImageList()
        {
            return itemImages;
        }
        public ListView getMonsterListView()
        {
            return listViewMonster;
        }
        //menu strip
        private void newPackageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewPackage np = new NewPackage(this);
            np.Show();
        }

        private void openPackageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = dialogPackage.ShowDialog();
            if (dr == DialogResult.OK)
            {
                string fileName = dialogPackage.FileName;
                packageName = dialogPackage.SafeFileName;
                saveLocation = fileName;
                string[] apx = fileName.Split('.');
                string apx2 = apx[apx.Length - 1];
                if (!apx2.Equals("apx"))
                {
                    MessageBox.Show("Not a valid Shielder package file.  Please select a file with the .apx extension.", "Not an .APX File");
                    return;
                }

                StreamReader file = new StreamReader(fileName);
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    string[] split = Regex.Split(line, @"\:\ ");
                    if (split[0].Equals("Monster"))
                    {
                        MonsterInfo m;
                        line = file.ReadLine();
                        string[] temp1 = Regex.Split(line, @"\:\ ");
                        string name = temp1[1];
                        line = file.ReadLine();
                        string[] temp2 = Regex.Split(line, @"\:\ ");
                        string imgName = temp2[1];
                        IndexedImage img = spritesheets[imgName];
                        m = new MonsterInfo(name, img.ImageName);
                        while ((line = file.ReadLine()) != null)
                        {
                            string[] split2 = Regex.Split(line, @"\:\ ");
                            if (split2.Length <= 1)
                            {
                                break;
                            }
                            string label = split2[0];
                            string value = split2[1];

                            if (label.Equals("Health"))
                            {
                                m.Health = Int32.Parse(value);
                            }

                            if (label.Equals("MaxDmg"))
                            {
                                m.MaxDamage = Int32.Parse(value);
                            }
                            if (label.Equals("MinDmg"))
                            {
                                m.MinimumDamage = Int32.Parse(value);
                            }
                            if (label.Equals("ASpeed"))
                            {
                                m.AttackSpeed = Decimal.Parse(value);
                            }
                            if (label.Equals("AttackRange"))
                            {
                                m.AttackRange = Decimal.Parse(value);
                            }
                            if (label.Equals("Sight"))
                            {
                                m.Sight = Decimal.Parse(value);
                            }
                            if (label.Equals("MSpeed"))
                            {
                                m.MovementSpeed = Decimal.Parse(value);
                            }
                            if (label.Equals("XP"))
                            {
                                m.Experience = Int32.Parse(value);
                            }
                            if (label.Equals("Gold"))
                            {
                                m.Gold = Int32.Parse(value);
                            }
                            if (label.Equals("Level"))
                            {
                                m.Level = Decimal.Parse(value);
                            }
                            if (label.Equals("Hostile"))
                            {
                                m.Hostile = Boolean.Parse(value);
                            }
                            if (label.Equals("Flying"))
                            {
                                m.Flying = Boolean.Parse(value);
                            }
                            if (label.Equals("Invincible"))
                            {
                                m.Invincible = Boolean.Parse(value);
                            }
                            if (label.Equals("NPC"))
                            {
                                m.NPC = Boolean.Parse(value);
                            }
                            if (label.Equals("Stoic"))
                            {
                                m.Stoic = Boolean.Parse(value);
                            }
                            if (label.Equals("Summoner"))
                            {
                                m.Summoner = Boolean.Parse(value);
                            }
                            if (label.Equals("Familiar"))
                            {
                                m.Familiar = value;
                            }
                            if (label.Equals("ItemDropper"))
                            {
                                m.ItemDropper = Boolean.Parse(value);
                            }
                            if (label.Equals("Item"))
                            {
                                m.DroppedItem = value;
                            }
                            if (label.Equals("DropChance"))
                            {
                                m.DropChance = Decimal.Parse(value);
                            }
                            if (label.Equals("Poisonous"))
                            {
                                m.Poisonous = Boolean.Parse(value);
                            }
                            if (label.Equals("DPT"))
                            {
                                m.DamagePerTick = Int32.Parse(value);
                            }
                            if (label.Equals("TPS"))
                            {
                                m.TicksPerSecond = Int32.Parse(value);
                            }
                            if (label.Equals("Stacks"))
                            {
                                m.StackablePoison = Boolean.Parse(value);
                            }
                            if (label.Equals("Nightcrawler"))
                            {
                                m.NightCrawler = Boolean.Parse(value);
                            }
                            if (label.Equals("StartTime"))
                            {
                                m.StartTime = Decimal.Parse(value);
                            }
                            if (label.Equals("EndTime"))
                            {
                                m.EndTime = Decimal.Parse(value);
                            }
                        }
                        Monster monster = new Monster(m, img);
                        this.AddToListView(monster);
                    }
                    else if (split[0].Equals("Item"))
                    {
                        ItemInfo item;
                        line = file.ReadLine();
                        string[] temp1 = Regex.Split(line, @"\:\ ");
                        string name = temp1[1];
                        line = file.ReadLine();
                        string[] temp2 = Regex.Split(line, @"\:\ ");
                        string imgName = temp2[1];
                        IndexedImage img = itemImages[imgName];
                        item = new ItemInfo(name, img.ImageName);
                        while ((line = file.ReadLine()) != null)
                        {
                            string[] split3 = Regex.Split(line, @"\:\ ");
                            if (split3.Length <= 1)
                            {
                                break;
                            }
                            string label = split3[0];
                            string value = split3[1];
                            
                            if (label.Equals("Level"))
                            {
                                item.Level = Decimal.Parse(value);
                            }
                            if (label.Equals("ItemType"))
                            {
                                item.Type = (ItemType) Enum.Parse(typeof(ItemType), value);
                            }
                            if (label.Equals("MaxDmg"))
                            {
                                item.MaxDamage = Decimal.Parse(value);
                            }
                            if (label.Equals("MinDmg"))
                            {
                                item.MinDamage = Decimal.Parse(value);
                            }
                            if (label.Equals("Defense"))
                            {
                                item.Defense = Decimal.Parse(value);
                            }
                            if (label.Equals("Health"))
                            {
                                item.Health = Decimal.Parse(value);
                            }
                            if (label.Equals("AttackSpeed"))
                            {
                                item.AttackSpeed = Decimal.Parse(value);
                            }
                        }
                        Item temp = new Item(item, img);
                        this.AddToListView(temp);
                    }
                    else if (split[0].Equals("Quest"))
                    {
                        QuestInfo quest;
                        line = file.ReadLine();
                        string[] temp1 = Regex.Split(line, @"\:\ ");
                        string name = temp1[1];
                        Image temp = (Image)Resources.ResourceManager.GetObject("Parchment.png");
                        IndexedImage img = new IndexedImage(temp, "Parchment.png");
                        quest = new QuestInfo(name);
                        while ((line = file.ReadLine()) != null)
                        {
                            string[] split3 = Regex.Split(line, @"\:\ ");
                            if (split3.Length <= 1)
                            {
                                break;
                            }
                            string label = split3[0];
                            string value = split3[1];

                            if (label.Equals("MinLvl"))
                            {
                                quest.MinLevel = Decimal.Parse(value);
                            }
                            if (label.Equals("Prompt"))
                            {
                                quest.Prompt = value;
                            }
                            if (label.Equals("RewardItem"))
                            {
                                quest.RewardItem = value;
                            }
                            if (label.Equals("RewardGold"))
                            {
                                quest.RewardGold = Decimal.Parse(value);
                            }
                            if (label.Equals("RewardXP"))
                            {
                                quest.RewardExperience = Decimal.Parse(value);
                            }
                            if (label.Equals("KillCond"))
                            {
                                quest.KillCondition = Boolean.Parse(value);
                            }
                            if (label.Equals("CollectCond"))
                            {
                                quest.CollectCondition = Boolean.Parse(value);
                            }
                            if (label.Equals("TargetMonster"))
                            {
                                quest.TargetMonster = value;
                            }
                            if (label.Equals("TargetItem"))
                            {
                                quest.TargetItem = value;
                            }
                        }
                        Quest q = new Quest(quest, img);
                        this.AddToListView(q);
                    }
                }
                tabControl.Visible = true;
                this.PackageIsOpen = true;
                lblStatus.Text = fileName;
                file.Close();
            }
        }

        private void savePackageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string print = "";
            int numOfMonsters = listViewMonster.Items.Count;
            int numOfItems = listViewItem.Items.Count;
            int numOfQuests = listViewQuest.Items.Count;
            Assembly assy = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assy.Location);
            string version = fvi.FileVersion;
            print += "Version: " + CurrentVersion + "\r\n";
            print += "Monsters: " + numOfMonsters + "\r\n";
            print += "Items: " + numOfItems + "\r\n";
            print += "Quests: " + numOfQuests + "\r\n";

            for (int i = 0; i < numOfMonsters; i++)
            {
                Monster m = (Monster) listViewMonster.Items[i];
                print += m.Print();
            }
            for (int i = 0; i < numOfItems; i++)
            {
                Item item = (Item) listViewItem.Items[i];
                print += item.Print();
            }
            for (int i = 0; i < numOfQuests; i++)
            {
                Quest quest = (Quest)listViewQuest.Items[i];
                print += quest.Print();
            }
            System.IO.File.WriteAllText(@lblStatus.Text, print);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (packageIsOpen)
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to exit?  A Package is open and may or may not have been saved.", "Exiting Shielder", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    this.Close();
                }
            }
            else
            {
                this.Close();
            }
        }
        public string CurrentVersion
        {
            get
            {
                return ApplicationDeployment.IsNetworkDeployed
                       ? ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()
                       : Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }   
        //mob tab
        private void btnNewMob_Click(object sender, EventArgs e)
        {
            MonsterEditor me = new MonsterEditor(this);
            me.Show();
        }

        private void btnDeleteSelectedMob_Click(object sender, EventArgs e)
        {
            if (listViewMonster.SelectedItems.Count > 0)
            {
                DialogResult dr = MessageBox.Show("Are you sure you wish to delete this monster?", "Monster Deletion", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    listViewMonster.Items.Remove(listViewMonster.SelectedItems[0]);
                }
            }
        }
        private void listViewMonster_DoubleClick(object sender, EventArgs e)
        {
            Monster m = (Monster)listViewMonster.SelectedItems[0];
            MonsterEditor me = new MonsterEditor(this, m);
            me.Show();
        }
        public void AddToListView(Monster m)
        {
            int index = imgMonster.Images.IndexOfKey(m.MonsterInfo.ImageName);
            m.ImageIndex = index;
            listViewMonster.Items.Add(m);
        }
        public void OverwriteMob(Monster m)
        {
            int index = listViewMonster.SelectedItems[0].Index;
            listViewMonster.Items.RemoveAt(index);
            listViewMonster.Items.Insert(index, m);
            index = imgMonster.Images.IndexOfKey(m.MonsterInfo.ImageName);
            m.ImageIndex = index;
        }
        //item tab
        private void btnNewItem_Click(object sender, EventArgs e)
        {
            ItemEditor ie = new ItemEditor(this);
            ie.Show();
        }
        public void AddToListView(Item i)
        {
            int index = imgItem.Images.IndexOfKey(i.ItemInfo.ImageName);
            i.ImageIndex = index;
            listViewItem.Items.Add(i);
        }
        private void btnDeleteSelectedItem_Click(object sender, EventArgs e)
        {
            if (listViewItem.SelectedItems.Count > 0)
            {
                DialogResult dr = MessageBox.Show("Are you sure you wish to delete this item?", "Item Deletion", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    listViewItem.Items.Remove(listViewItem.SelectedItems[0]);
                }
            }
        }
        public void OverwriteItem(Item i)
        {
            int index = listViewItem.SelectedItems[0].Index;
            listViewItem.Items.RemoveAt(index);
            listViewItem.Items.Insert(index, i);
            index = imgItem.Images.IndexOfKey(i.ItemInfo.ImageName);
            i.ImageIndex = index;
        }

        private void listViewItem_DoubleClick(object sender, EventArgs e)
        {
            Item i = (Item)listViewItem.SelectedItems[0];
            ItemEditor ie = new ItemEditor(i, this);
            ie.Show();
        }
        public ImageList getMonsterImageList()
        {
            return imgMonster;
        }
        public ImageList getItemImageList()
        {
            return imgItem;
        }
        public ListView getItemListView()
        {
            return listViewItem;
        }
        public ListView getQuestListView()
        {
            return listViewQuest;
        }
        public void AddToListView(Quest quest)
        {
            int index = 0;
            quest.ImageIndex = index;
            listViewQuest.Items.Add(quest);
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl.SelectedIndex == 2)
            {
                int numOfMonsters = listViewMonster.Items.Count;
                int numOfItems = listViewItem.Items.Count;
                int numOfQuests = listViewQuest.Items.Count;
                lblPackageName.Text = packageName;
                lblMonsters.Text = numOfMonsters.ToString();
                lblItems.Text = numOfItems.ToString();
                lblQuests.Text = numOfQuests.ToString();
                lblItemPath.Text = pathToItems;
                lblSpritePath.Text = pathToSprites;
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }
        private void setItemPathToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChoosePathToItemFolder();
            SaveSettings();
            LoadItemImages();
        }

        private void setSpritePathToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChoosePathToSpriteFolder();
            SaveSettings();
            LoadSpriteImages();
        }

        private void btnNewQuest_Click(object sender, EventArgs e)
        {
            QuestEditor editor = new QuestEditor(this);
            editor.Show();
        }

        private void btnDeleteQuest_Click(object sender, EventArgs e)
        {
            if (listViewQuest.SelectedItems.Count > 0)
            {
                DialogResult dr = MessageBox.Show("Are you sure you wish to delete this quest?", "Quest Deletion", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    listViewQuest.Items.Remove(listViewQuest.SelectedItems[0]);
                }
            }
        }

        private void listViewQuest_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Quest q = (Quest)listViewQuest.SelectedItems[0];
            QuestEditor qe = new QuestEditor(q, this);
            qe.Show();
        }
        public void OverWriteQuest(Quest q)
        {
            int index = listViewQuest.SelectedItems[0].Index;
            listViewQuest.Items.RemoveAt(index);
            listViewQuest.Items.Insert(index, q);
            index = 0;
            q.ImageIndex = index;
        }



        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        public string PackageName
        {
            get
            {
                return packageName;
            }
            set
            {
                packageName = value;
            }
        }
        public string SaveLocation
        {
            get
            {
                return saveLocation;
            }
            set
            {
                saveLocation = value;
            }
        }
        public bool PackageIsOpen
        {
            get
            {
                return packageIsOpen;
            }
            set
            {
                packageIsOpen = value;
                if (value)
                {
                    tabControl.Visible = true;
                    savePackageToolStripMenuItem.Enabled = true;
                    lblStatus.Text = saveLocation + "\\" + packageName + ".apx";
                    openPackageToolStripMenuItem.Enabled = false;
                }
                else
                {
                    tabControl.Visible = false;
                    savePackageToolStripMenuItem.Enabled = false;
                    lblStatus.Text = "No package open.";
                    openPackageToolStripMenuItem.Enabled = true;
                }
            }
        }
    }
}
