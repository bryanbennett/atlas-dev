﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace VintageStudios.Planar2D.Input
{
    public interface IClickable
    {
        /// <summary>
        /// Gets or sets the bounds of the clickable component.
        /// </summary>
        Rectangle Bounds
        {
            get;
            set;
        }
        /// <summary>
        /// Method to handle mouse click events.
        /// </summary>
        /// <param name="mouseState"></param>
        void onClick(MouseState mouseState);
        /// <summary>
        /// Method to handle mouse release events.
        /// </summary>
        /// <param name="mouseState"></param>
        void onRelease(MouseState mouseState);
        /// <summary>
        /// Method to handle mouse drag events.
        /// </summary>
        /// <param name="mouseState"></param>
        void onDrag(MouseState mouseState);
        /// <summary>
        /// Method to handle the mouse hovering over the clickable component.
        /// </summary>
        /// <param name="mouseState"></param>
        void onHover(MouseState mouseState);
        /// <summary>
        /// Resets the component.  Usually called to return it after a drag or hover event.
        /// </summary>
        void Reset();
    }
}
