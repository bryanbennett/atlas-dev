﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VintageStudios.Planar2D.Shielder
{
    public class QuestInfo
    {
        private string name;
        private decimal minLevel = 1;
        private string rewardItem = "";
        private decimal rewardGold = 0;
        private decimal rewardXP = 0;
        private bool kill;
        private bool collect;
        private string targetMonster = "";
        private string targetItem = "";
        private string prompt = "Prompt";

        public QuestInfo(string name)
        {
            this.name = name;
        }
        public QuestInfo(QuestInfo master)
        {
            Name = master.Name;
            MinLevel = master.MinLevel;
            Prompt = master.Prompt;
            KillCondition = master.KillCondition;
            CollectCondition = master.CollectCondition;
            TargetMonster = master.TargetMonster;
            targetItem = master.targetItem;
            RewardExperience = master.RewardExperience;
            RewardGold = master.RewardGold;
            RewardItem = master.RewardItem;
        }
        public string Print()
        {
            string print = "\r\nQuest\r\n";

            print += "Name: " + Name + "\r\n";
            print += "MinLvl: " + MinLevel + "\r\n";
            print += "Prompt: " + Prompt + "\r\n";
            print += "RewardItem: " + RewardItem + "\r\n";
            print += "RewardGold: " + RewardGold + "\r\n";
            print += "RewardXP: " + RewardExperience + "\r\n";
            if (KillCondition)
            {
                print += "KillCond: " + KillCondition + "\r\n";
                print += "TargetMonster: " + TargetMonster + "\r\n";
            }
            if (CollectCondition)
            {
                print += "CollectCond: " + CollectCondition + "\r\n";
                print += "TargetItem: " + targetItem + "\r\n";
            }

            return print;
        }


        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>>>
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Prompt
        {
            get
            {
                return prompt;
            }
            set
            {
                prompt = value;
            }
        }
        public decimal MinLevel
        {
            get
            {
                return minLevel;
            }
            set
            {
                minLevel = value;
            }
        }
        public string RewardItem
        {
            get
            {
                return rewardItem;
            }
            set
            {
                rewardItem = value;
            }
        }
        public decimal RewardGold
        {
            get
            {
                return rewardGold;
            }
            set
            {
                rewardGold = value;
            }
        }
        public decimal RewardExperience
        {
            get
            {
                return rewardXP;
            }
            set
            {
                rewardXP = value;
            }
        }
        public bool CollectCondition
        {
            get
            {
                return collect;
            }
            set
            {
                collect = value;
            }
        }
        public bool KillCondition
        {
            get
            {
                return kill;
            }
            set
            {
                kill = value;
            }
        }
        public string TargetMonster
        {
            get
            {
                return targetMonster;
            }
            set
            {
                targetMonster = value;
            }
        }
        public string TargetItem
        {
            get
            {
                return targetItem;
            }
            set
            {
                targetItem = value;
            }
        }


    }
}
