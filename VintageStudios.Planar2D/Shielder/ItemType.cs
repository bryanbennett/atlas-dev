﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VintageStudios.Planar2D.Shielder
{
    public enum ItemType
    {
        Weapon_1H,
        Weapon_2H,
        Armor,
        Necklace,
        Boots,
        Helm,
        Gloves,
        Ring,
        Shield,
        Container,
        Belt,
    }
}
