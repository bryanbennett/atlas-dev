﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VintageStudios.Planar2D.Shielder
{
    /// <summary>
    /// Provides an interface in which the information returned by the Print() method is to be printed to a file.
    /// </summary>
    public interface IPrintable
    {
        /// <summary>
        /// Returns a string of information meant to be written to some file.
        /// </summary>
        /// <returns></returns>
        string Print();
    }
}
