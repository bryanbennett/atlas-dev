﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using VintageStudios.Shielder;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace VintageStudios.Planar2D.Shielder
{
/// <summary>
/// Class used to retrieve the monster and item informations from an APX file generated from Shielder.
/// </summary>
    public class ShielderPackage
    {
        private string version;
        private string packagePath;
        private int numOfMonsters;
        private int numOfItems;
        private int numOfQuests;
        private Dictionary<string, MonsterInfo> monsters;
        private Dictionary<string, ItemInfo> items;
        private Dictionary<string, ItemType> itemTypes;
        private Dictionary<string, QuestInfo> quests;
        private string packageName;
        private ContentManager content;
        /// <summary>
        /// Returns the path of the .APX file used within this ShielderPackage object.
        /// </summary>
        public string PackagePath
        {
            get
            {
                return packagePath;
            }
        }
        /// <summary>
        /// Returns the version info of the Shielder application that produced the .APX file.
        /// </summary>
        public string Version
        {
            get
            {
                return version;
            }
        }
        /// <summary>
        /// Returns the amount of monsters contained within the .APX file.
        /// </summary>
        public int MonsterCount
        {
            get
            {
                return numOfMonsters;
            }
        }
        /// <summary>
        /// Returns the amount of items contained within the .APX file.
        /// </summary>
        public int ItemCount
        {
            get
            {
                return numOfItems;
            }
        }
        /// <summary>
        /// Returns the amount of quests contained int he .APX file.
        /// </summary>
        public int QuestCount
        {
            get
            {
                return numOfQuests;
            }
        }
        /// <summary>
        /// Returns the collection of QuestInfo objects found within the .APX file.
        /// </summary>
        public Dictionary<string, QuestInfo> Quests
        {
            get
            {
                return quests;
            }
        }

        /// <summary>
        /// Returns the collection of MonsterInfo objects found within the .APX file.
        /// </summary>
        public Dictionary<string, MonsterInfo> Monsters
        {
            get
            {
                return monsters;
            }
        }
        /// <summary>
        /// Returns the collection of ItemInfo objects found within the .APX file.
        /// </summary>
        public Dictionary<string, ItemInfo> Items
        {
            get
            {
                return items;
            }
        }
        /// <summary>
        /// Instantiate a ShielderPackage object with the path to the .APX file to be read.
        /// </summary>
        /// <param name="path"></param>
        public ShielderPackage(string path, ContentManager content)
        {
            this.content = content;
            itemTypes = new Dictionary<string, ItemType>();
            LoadItemTypes();
           
            string[] split = path.Split('\\');
            packageName = split[split.Length - 1];
            if (!packageName.Contains(".apx"))
            {
                throw new InvalidShielderPackageException("Invalid Shielder package.  Ensure the file is a .APX file and was generated from Shielder. Path: " + path);
            }
            StreamReader reader = new StreamReader(path);
            packagePath = path;
            try
            {
                ReadPackage(reader);
            }
            catch(Exception ex)
            {
                string message = ex.StackTrace;
                throw new InvalidShielderPackageException("Invalid Shielder package.  The file is either corrupted or empty.");
            }
        }
        private void ReadPackage(StreamReader reader)
        {
            monsters = new Dictionary<string, MonsterInfo>();
            items = new Dictionary<string, ItemInfo>();
            quests = new Dictionary<string, QuestInfo>();
            string line = string.Empty;
            using (reader)
            {
                Console.WriteLine(line);
                Console.WriteLine("APX Package: " + packageName);
                line = reader.ReadLine();
                version = line;
                Console.WriteLine(line);    //version
                line = reader.ReadLine();
                string[] monsterLine = Regex.Split(line, @"\:\ ");
                numOfMonsters = Int32.Parse(monsterLine[1]);
                Console.WriteLine(line);    //total monsters
                line = reader.ReadLine();
                string[] itemLine = Regex.Split(line, @"\:\ ");
                numOfItems = Int32.Parse(itemLine[1]);
                line = reader.ReadLine();
                string[] questLine = Regex.Split(line, @"\:\ ");
                numOfQuests = Int32.Parse(itemLine[1]);
                Console.WriteLine(line);    //total items
                while ((line = reader.ReadLine()) != null)
                {
                    string[] split = Regex.Split(line, @"\:\ ");
                    if (split[0].Equals("Monster"))
                    {
                        MonsterInfo m;
                        line = reader.ReadLine();
                        string[] temp1 = Regex.Split(line, @"\:\ ");
                        string name = temp1[1];
                        line = reader.ReadLine();
                        string[] temp2 = Regex.Split(line, @"\:\ ");
                        string[] temp3 = temp2[1].Split('.');
                        string imgName = temp3[0];
                        m = new MonsterInfo(name, imgName);
                        Console.WriteLine(imgName);
                        while ((line = reader.ReadLine()) != null)
                        {
                            string[] split2 = Regex.Split(line, @"\:\ ");
                            if (split2.Length <= 1)
                            {
                                break;
                            }
                            string label = split2[0];
                            string value = split2[1];

                            if (label.Equals("Health"))
                            {
                                m.Health = Int32.Parse(value);
                            }

                            if (label.Equals("MaxDmg"))
                            {
                                m.MaxDamage = Int32.Parse(value);
                            }
                            if (label.Equals("MinDmg"))
                            {
                                m.MinimumDamage = Int32.Parse(value);
                            }
                            if (label.Equals("ASpeed"))
                            {
                                m.AttackSpeed = Decimal.Parse(value);
                            }
                            if (label.Equals("AttackRange"))
                            {
                                m.AttackRange = Decimal.Parse(value);
                            }
                            if (label.Equals("Sight"))
                            {
                                m.Sight = Decimal.Parse(value);
                            }
                            if (label.Equals("MSpeed"))
                            {
                                m.MovementSpeed = Decimal.Parse(value);
                            }
                            if (label.Equals("XP"))
                            {
                                m.Experience = Int32.Parse(value);
                            }
                            if (label.Equals("Gold"))
                            {
                                m.Gold = Int32.Parse(value);
                            }
                            if (label.Equals("Level"))
                            {
                                m.Level = Decimal.Parse(value);
                            }
                            if (label.Equals("Hostile"))
                            {
                                m.Hostile = Boolean.Parse(value);
                            }
                            if (label.Equals("Flying"))
                            {
                                m.Flying = Boolean.Parse(value);
                            }
                            if (label.Equals("Invincible"))
                            {
                                m.Invincible = Boolean.Parse(value);
                            }
                            if (label.Equals("NPC"))
                            {
                                m.NPC = Boolean.Parse(value);
                            }
                            if (label.Equals("Stoic"))
                            {
                                m.Stoic = Boolean.Parse(value);
                            }
                            if (label.Equals("Summoner"))
                            {
                                m.Summoner = Boolean.Parse(value);
                            }
                            if (label.Equals("Familiar"))
                            {
                                m.Familiar = value;
                            }
                            if (label.Equals("ItemDropper"))
                            {
                                m.ItemDropper = Boolean.Parse(value);
                            }
                            if (label.Equals("Item"))
                            {
                                m.DroppedItem = value;
                            }
                            if (label.Equals("DropChance"))
                            {
                                m.DropChance = Decimal.Parse(value);
                            }
                            if (label.Equals("Poisonous"))
                            {
                                m.Poisonous = Boolean.Parse(value);
                            }
                            if (label.Equals("DPT"))
                            {
                                m.DamagePerTick = Int32.Parse(value);
                            }
                            if (label.Equals("TPS"))
                            {
                                m.TicksPerSecond = Int32.Parse(value);
                            }
                            if (label.Equals("Stacks"))
                            {
                                m.StackablePoison = Boolean.Parse(value);
                            }
                            if (label.Equals("Nightcrawler"))
                            {
                                m.NightCrawler = Boolean.Parse(value);
                            }
                            if (label.Equals("StartTime"))
                            {
                                m.StartTime = Decimal.Parse(value);
                            }
                            if (label.Equals("EndTime"))
                            {
                                m.EndTime = Decimal.Parse(value);
                            }
                        }
                        monsters.Add(m.MonsterName, m);
                    }
                    if (split[0].Equals("Item"))
                    {
                        ItemInfo item;
                        line = reader.ReadLine();
                        string[] temp1 = Regex.Split(line, @"\:\ ");
                        string name = temp1[1];
                        line = reader.ReadLine();
                        string[] temp2 = Regex.Split(line, @"\:\ ");
                        string[] temp3 = temp2[1].Split('.');
                        string imgName = temp3[0];
                        item = new ItemInfo(name, imgName);

                        string imgLocation = "Items\\" + item.ImageName;
                        item.Sprite = content.Load<Texture2D>(imgLocation);
                        Console.WriteLine(imgName);
                        while ((line = reader.ReadLine()) != null)
                        {
                            string[] split2 = Regex.Split(line, @"\:\ ");
                            if (split2.Length <= 1)
                            {
                                break;
                            }
                            string label = split2[0];
                            string value = split2[1];

                            if (label.Equals("Level"))
                            {
                                item.Level = Int32.Parse(value);
                            }

                            if (label.Equals("ItemType"))
                            {
                                item.Type = itemTypes[value];
                            }
                            if (label.Equals("MinDmg"))
                            {
                                item.MinDamage = Int32.Parse(value);
                            }
                            if (label.Equals("MaxDmg"))
                            {
                                item.MaxDamage = Int32.Parse(value);
                            }
                            if (label.Equals("AttackSpeed"))
                            {
                                item.AttackSpeed = Decimal.Parse(value);
                            }
                            if (label.Equals("Defense"))
                            {
                                item.Defense = Decimal.Parse(value);
                            }
                            if (label.Equals("Health"))
                            {
                                item.Health = Int32.Parse(value);
                            }
                        }
                        items.Add(item.Name, item);
                    }
                    if (split[0].Equals("Quest"))
                    {
                        QuestInfo quest;
                        line = reader.ReadLine();
                        string[] temp1 = Regex.Split(line, @"\:\ ");
                        string name = temp1[1];
                        quest = new QuestInfo(name);
                        while ((line = reader.ReadLine()) != null)
                        {
                            string[] split3 = Regex.Split(line, @"\:\ ");
                            if (split3.Length <= 1)
                            {
                                break;
                            }
                            string label = split3[0];
                            string value = split3[1];

                            if (label.Equals("MinLvl"))
                            {
                                quest.MinLevel = Decimal.Parse(value);
                            }
                            if (label.Equals("Prompt"))
                            {
                                quest.Prompt = value;
                            }
                            if (label.Equals("RewardItem"))
                            {
                                quest.RewardItem = value;
                            }
                            if (label.Equals("RewardGold"))
                            {
                                quest.RewardGold = Decimal.Parse(value);
                            }
                            if (label.Equals("RewardXP"))
                            {
                                quest.RewardExperience = Decimal.Parse(value);
                            }
                            if (label.Equals("KillCond"))
                            {
                                quest.KillCondition = Boolean.Parse(value);
                            }
                            if (label.Equals("CollectCond"))
                            {
                                quest.CollectCondition = Boolean.Parse(value);
                            }
                            if (label.Equals("TargetMonster"))
                            {
                                quest.TargetMonster = value;
                            }
                            if (label.Equals("TargetItem"))
                            {
                                quest.TargetItem = value;
                            }
                        }
                        quests.Add(quest.Name, quest);
                    }
                }
                if (monsters.Count == 0 && items.Count == 0)
                {
                    throw new InvalidShielderPackageException("No contents found in the .APX file.  The file is either corrupted or empty.");
                }
            }
        }
        private void LoadItemTypes()
        {
            itemTypes.Add("Armor", ItemType.Armor);
            itemTypes.Add("Weapon_1H", ItemType.Weapon_1H);
            itemTypes.Add("Weapon_2H", ItemType.Weapon_2H);
            itemTypes.Add("Necklace", ItemType.Necklace);
            itemTypes.Add("Boots", ItemType.Boots);
            itemTypes.Add("Helm", ItemType.Helm);
            itemTypes.Add("Gloves", ItemType.Gloves);
            itemTypes.Add("Ring", ItemType.Ring);
            itemTypes.Add("Shield", ItemType.Shield);
            itemTypes.Add("Container", ItemType.Container);
        }
    }
}
