﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework.Content;

namespace VintageStudios.Planar2D.GameData
{
    /// <summary>
    /// Static Class used to produce dictionaries for content resources in XNA projects.
    /// </summary>
    public static class ResourceLoader
    {
        /// <summary>
        /// Generates and returns a Dictionary<String, T> that holds all the resources in the particular folder.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="folder"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static Dictionary<String, T> LoadContent<T>(String folder, ContentManager content)
        {
            var dir = new DirectoryInfo(content.RootDirectory + "\\" + folder);
            DirectoryInfo[] dirs = dir.GetDirectories();
            Dictionary<String, T> resources = new Dictionary<String, T>();
            if (dir.Exists)
            {
                FileInfo[] files = dir.GetFiles("*.*");
                foreach (FileInfo file in files)
                {
                    string key = Path.GetFileNameWithoutExtension(file.Name);
                    resources.Add(key, content.Load<T>(folder + "\\" + key));
                }

            }
            return resources;
        }
        /// <summary>
        /// Generates and returns a Dictionary<String,T> that holds all the resources in a particular folder with the specific file extension (filter).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="folder"></param>
        /// <param name="filter"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static Dictionary<String, T> LoadContent<T>(String folder, String filter, ContentManager content)
        {
            var dir = new DirectoryInfo(content.RootDirectory + "\\" + folder);
            DirectoryInfo[] dirs = dir.GetDirectories();
            Dictionary<String, T> resources = new Dictionary<String, T>();
            if (dir.Exists)
            {
                FileInfo[] files = dir.GetFiles(filter);
                foreach (FileInfo file in files)
                {
                    string key = Path.GetFileNameWithoutExtension(file.Name);
                    resources.Add(key, content.Load<T>(folder + "\\" + key));
                }

            }
            return resources;
        }
    }
}
