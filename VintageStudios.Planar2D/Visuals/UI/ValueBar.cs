﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace VintageStudios.Planar2D.Visuals.UI
{
    /// <summary>
    /// Class used for health bars, progress bars, experience bars, etc.
    /// </summary>
    public class ValueBar : VisualComponent
    {
        private decimal value = 0;
        private decimal maxValue;
        private decimal percentage;
        private Color c;
        private Texture2D texture;
        private bool isPercentageOnly = false;

        /// <summary>
        /// Use this constructor if you wish to supply percentages to the bar.  This disables the ValueBar.Value property.
        /// The Point argument (position) is relative to the container this visual component is in.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="barWidth"></param>
        /// <param name="barHeight"></param>
        /// <param name="texture"></param>
        /// <param name="c"></param>
        public ValueBar(Rectangle bounds, Color c, GraphicsDevice device)
            :base(bounds, device)
        {
            value = 0;
            this.texture = new Texture2D(this.Graphics, 1, 1, false, SurfaceFormat.Color);
            this.texture.SetData(new Color[] { c });
            this.c = c;
            isPercentageOnly = true;
        }
        /// <summary>
        /// Use this constructor if you want to customize the maximum value of the ValueBar.  This does not disable the ValueBar.PercentFilled property.
        /// The Point argument (position) is relative to the container this visual component is in.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="maxValue"></param>
        /// <param name="barWidth"></param>
        /// <param name="barHeight"></param>
        /// <param name="texture"></param>
        /// <param name="c"></param>
        public ValueBar(Rectangle bounds, decimal maxValue, Color c, GraphicsDevice device)
            :base(bounds, device)
        {
            value = 0;
            this.texture = new Texture2D(this.Graphics, 1, 1, false, SurfaceFormat.Color);
            this.texture.SetData(new Color[] { c });
            this.c = c;
            this.maxValue = maxValue;
        }
        public override void Draw(SpriteBatch batch)
        {
            if (base.Visible)
            {
                Rectangle rect = new Rectangle(Bounds.X, Bounds.Y, (int)(percentage * Bounds.Width), Bounds.Height);
                Rectangle rect2 = new Rectangle(Bounds.X, Bounds.Y, Bounds.Width, Bounds.Height);
                batch.Draw(texture, rect2, Color.Black);
                batch.Draw(texture, rect, c);
            }
        }
        public override void Update(int delta)
        {
            //nothing
        }


        public override void onClick(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            //not implemented
        }

        public override void onRelease(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            //not implemented
        }

        public override void onDrag(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            //not implemented
        }
        public override void onHover(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            //not implemented
        }
        public override void Reset()
        {
            //not implemented
        }



        //>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>

        /// <summary>
        /// Returns or sets the percentage this bar is to be filled.  You can directly set the percentage with this property.
        /// If direct values are used, setting the percentage of the bar will automatically adjust the Value property.
        /// Domain: [0,100]
        /// </summary>
        public decimal PercentFilled
        {
            get
            {
                return percentage;
            }
            set
            {
                if (value > 100)
                {
                    this.percentage = 100;
                }
                else if (value < 0)
                {
                    this.percentage = 0;
                }
                else
                {
                    this.percentage = value;
                }
                this.percentage /= (decimal)100.0;
                if (!isPercentageOnly)
                {
                    this.value = percentage * maxValue;
                }
            }
        }
        /// <summary>
        /// Returns or sets the value of this bar which in turn calculates the percentage of the bar to be filled (if the max-value constructor was used).  Value will not exceed the maximum value.
        /// Domain: [0, MaxValue]
        /// </summary>
        public decimal Value
        {
            get
            {
                return value;
            }
            set
            {
                if (value > maxValue)
                {
                    this.value = maxValue;
                }
                else if (value < 0)
                {
                    this.value = 0;
                }
                else
                {
                    this.value = value;
                }
                percentage = (this.value / (maxValue));
            }
        }
        public decimal MaxValue { get { return maxValue; } set { maxValue = value; percentage = (this.value / maxValue); } }

        /// <summary>
        /// Returns whether or not this ValueBar is has the value property enabled.  If this is false, it means you used the percentage-only constructor.
        /// </summary>
        public bool ValueEnabled { get { return isPercentageOnly; } }
    }
}
