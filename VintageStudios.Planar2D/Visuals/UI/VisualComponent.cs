﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using VintageStudios.Planar2D.Input;

namespace VintageStudios.Planar2D.Visuals.UI
{
    public abstract class VisualComponent : IClickable
    {
        private Rectangle bounds;
        private Vector2 origin;
        private Point relativePosition;
        private bool isVisible = false;
        private int hoverTime = 200;
        private bool hasParent;
        private VisualComponent parent;
        private GraphicsDevice graphics;

        public VisualComponent(Rectangle bounds, GraphicsDevice device)
        {
            this.bounds = bounds;
            origin = new Vector2(bounds.X + bounds.Width / 2, bounds.Y + bounds.Height / 2);
            relativePosition = new Point(bounds.X, bounds.Y);
            graphics = device;
        }
        /// <summary>
        /// Override this method and supply own drawing logic.  Ensure inherited subclasses check to see if the component is visible before drawing.
        /// </summary>
        /// <param name="batch"></param>
        public abstract void Draw(SpriteBatch batch);
        /// <summary>
        /// Updates the visual component.
        /// </summary>
        /// <param name="delta"></param>
        public abstract void Update(int delta);
        /// <summary>
        /// Override to handle mouse clicks.
        /// </summary>
        /// <param name="mouseState"></param>
        public abstract void onClick(MouseState mouseState);
        public abstract void onRelease(MouseState mouseState);
        public abstract void onDrag(MouseState mouseState);
        public abstract void onHover(MouseState mouseState);
        public abstract void Reset();




        //>>>>>>>>>>>>>>>>Properties>>>>>>>>>>>>>>>>>>>>>>>>



        /// <summary>
        /// Gets or sets the position of the visual component relative to the container it is in.
        /// </summary>
        public Point Position { get { return relativePosition; } set { relativePosition = value; } }

        public Vector2 Origin { get { return origin; } }

        public Rectangle Bounds { get { return bounds; } set { bounds = value; origin = new Vector2(bounds.X + bounds.Width / 2, bounds.Y + bounds.Height / 2); } }

        public VisualComponent Parent
        {
            get
            {
                return parent;
            }
            set
            {
                parent = value;
                hasParent = true;
                bounds = new Rectangle(relativePosition.X + parent.Bounds.X, relativePosition.Y + parent.Bounds.Y, bounds.Width, bounds.Height);
            }
        }
        public bool HasParent { get { return hasParent; } }

        /// <summary>
        /// Gets or sets the visual component's visibility.  Default visibility is set to true.
        /// </summary>
        public bool Visible { get { return isVisible; } set { isVisible = value; } }

        /// <summary>
        /// Gets or sets the amount of time in milliseconds until the hover event is triggered.  Default is 200 milliseconds.
        /// </summary>
        public int HoverSensitity { get { return hoverTime; } set { hoverTime = value; } }

        public GraphicsDevice Graphics { get { return graphics; } set { graphics = value; } }
    }
}
