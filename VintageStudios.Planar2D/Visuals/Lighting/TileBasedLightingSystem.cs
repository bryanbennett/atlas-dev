﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace VintageStudios.Planar2D.Visuals.Lighting
{
    public class TileBasedLightingSystem
    {
        private AlphaTile[,] shades; //too incredibly big.
        private List<Point> lights;
        private float ambient = 1.0f;
        private Texture2D tile;
        private int mapWidth;
        private int mapHeight;
        private int tileSize;

        //properties

        public float AmbientLight
        {
            get
            {
                return ambient;
            }
            set
            {
                ambient = value;
            }
        }
        public AlphaTile[,] AlphaTiles
        {
            get
            {
                return shades;
            }
        }

        public TileBasedLightingSystem(List<Point> lights, Texture2D tile, int mapWidth, int mapHeight, int tileSize)
        {
            this.lights = lights;
            shades = new AlphaTile[mapWidth, mapHeight];
            this.tile = tile;
            this.mapWidth = mapWidth;
            this.mapHeight = mapHeight;
            this.tileSize = tileSize;
            SetAlphaTiles();
        }
        private void SetAlphaTiles()
        {
            for (int i = 0; i < mapWidth; i++)
            {
                for (int j = 0; j < mapHeight; j++)
                {
                    AlphaTile alphaT = new AlphaTile(new Point(i, j), tile, this, tileSize);
                    shades[i, j] = alphaT;
                }
            }

            int count = lights.Count;
            for (int i = 0; i < count; i++)
            {
                Point light = lights[i];
                updateLights(light.X, light.Y, light);
            }
        }
        private void updateLights(int X, int Y, Point light)
        {
            nodeUpdate(X, Y - 1, light);
            nodeUpdate(X, Y + 1, light);
            nodeUpdate(X - 1, Y, light);
            nodeUpdate(X + 1, Y, light);
        }
        private void nodeUpdate(int A, int B, Point light)
        {
            AlphaTile alphaT = shades[A, B];
            int dx = (A - light.X) * (A - light.X);
            int dy = (B - light.Y) * (B - light.Y);
            int distance = dx + dy;
            float alpha = distance / 200f;

            if (alpha < 1.0)
            {
                bool update = alphaT.SetAlpha(light, alpha);
                if (update)
                {
                    updateLights(A, B, light);
                }
            }        
        }
        /// <summary>
        /// Sets the ambient light by the passed in value.
        /// </summary>
        /// <param name="ambient"></param>
        public void SetAmbient(float ambient)
        {
            this.ambient = ambient;
        }
        public void Draw(SpriteBatch batch, Rectangle view)
        {
            int startX = view.X / 16;
            int startY = view.Y / 16;
            int plusX = view.Width / 16;
            int plusY = view.Height / 16;
            for (int i = startX; i < (startX + plusX); i++)
            {
                for (int j = startY; j < (startY + plusY + 1); j++)
                {
                    AlphaTile tile = shades[i,j];
                    tile.DrawAt(batch, view.X, view.Y);
                }
            }
        }
        /// <summary>
        /// Changes the ambient value by the value passed into the method.  A negative float value decreases the ambient
        /// light whereas a positive value increases it.
        /// </summary>
        /// <param name="value"></param>
        public void ChangeAmbient(float value)
        {
            ambient += value;
        }
    }
}
