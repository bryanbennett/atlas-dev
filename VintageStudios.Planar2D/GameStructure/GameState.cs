﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using VintageStudios.Planar2D.Input;
using VintageStudios.Planar2D.Visuals.UI;
using VintageStudios.Planar2D.Triggers;

namespace VintageStudios.Planar2D.GameStructure
{
    public abstract class GameState
    {
        private int stateID;
        protected bool hasFocus;
        protected bool isVisible;
        protected GameStateController gcs;
        protected MouseHandler mh;
        protected WindowManager wm;
        private List<Trigger> triggers;
        private bool keyInputDisabled = false;
        //properties
        /// <summary>
        /// Returns the window manager associated with this game state.
        /// </summary>
        public WindowManager WindowManager
        {
            get
            {
                return wm;
            }
        }
        /// <summary>
        /// Returns the state ID.
        /// </summary>
        public int StateID
        {
            get
            {
                return stateID;
            }
        }
        /// <summary>
        /// Sets or gets the "active" state of the game state, allowing it to be updated.
        /// </summary>
        public bool Focus
        {
            get
            {
                return hasFocus;
            }
            set
            {
                hasFocus = value;
            }
        }
        /// <summary>
        /// Gets or sets the visible state of the gamestate, allowing it to be drawn.
        /// </summary>
        public bool Visible
        {
            get
            {
                return isVisible;
            }
            set
            {
                isVisible = value;
            }
        }
        public MouseHandler MouseHandler
        {
            get
            {
                return mh;
            }
            set
            {
                mh = value;
            }
        }
        public GameStateController Controller
        {
            get
            {
                return gcs;
            }
        }
        public bool KeyInputDisabled
        {
            get
            {
                return keyInputDisabled;
            }
            set
            {
                keyInputDisabled = value;
            }
        }
        public GameState(GameStateController gcs, int stateID)
        {
            this.stateID = stateID;
            this.gcs = gcs;
            mh = new MouseHandler(this);
            wm = new WindowManager();
            triggers = new List<Trigger>();
        }
        /// <summary>
        /// Adds the planar window to both the mouse handler and the window manager.
        /// Registering a window means that the window render calls and update calls
        /// are taken care of.
        /// </summary>
        /// <param name="window"></param>
        public void RegisterWindow(PlanarWindow window)
        {
            mh.AddComponent(window);
            wm.AddWindow(window);
        }
        public void AddTrigger(Trigger trigger)
        {
            triggers.Add(trigger);
        }
        public void RemoveTrigger(Trigger trigger)
        {
            triggers.Remove(trigger);
        }
        /// <summary>
        /// Draws the gamestate.
        /// </summary>
        /// <param name="batch"></param>
        public abstract void Draw(SpriteBatch batch);
        /// <summary>
        /// Updates the gamestate.
        /// </summary>
        /// <param name="delta"></param>
        public abstract void Update(int delta);
        /// <summary>
        /// If the gamestate has the focus, this method is where all the user input will be received from for both mouse and keyboard.
        /// Do not put user input in the update method (this will cause conflict between gamestates when a key is pressed that is bound
        /// to both).
        /// </summary>
        public abstract void KeyInput(int delta);

        /// <summary>
        /// Method automatically called by the game state controller.
        /// </summary>
        /// <param name="delta"></param>
        public void UpdateTriggers(int delta)
        {
            foreach (Trigger entry in triggers)
            {
                entry.Update(delta);
            }
        }
    }
}
