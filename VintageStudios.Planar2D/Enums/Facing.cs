﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VintageStudios.Planar2D
{
    public enum Facing
    {
        North,
        East,
        South,
        West,
    }
}
