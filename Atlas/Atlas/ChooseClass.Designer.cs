﻿namespace Atlas
{
    partial class ChooseClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbxKnight = new System.Windows.Forms.PictureBox();
            this.pbxMage = new System.Windows.Forms.PictureBox();
            this.pbxThief = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radKnight = new System.Windows.Forms.RadioButton();
            this.radThief = new System.Windows.Forms.RadioButton();
            this.radMage = new System.Windows.Forms.RadioButton();
            this.btnStart = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbxKnight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxThief)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbxKnight
            // 
            this.pbxKnight.Location = new System.Drawing.Point(6, 19);
            this.pbxKnight.Name = "pbxKnight";
            this.pbxKnight.Size = new System.Drawing.Size(32, 32);
            this.pbxKnight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxKnight.TabIndex = 0;
            this.pbxKnight.TabStop = false;
            // 
            // pbxMage
            // 
            this.pbxMage.Location = new System.Drawing.Point(6, 131);
            this.pbxMage.Name = "pbxMage";
            this.pbxMage.Size = new System.Drawing.Size(32, 32);
            this.pbxMage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxMage.TabIndex = 1;
            this.pbxMage.TabStop = false;
            // 
            // pbxThief
            // 
            this.pbxThief.Location = new System.Drawing.Point(6, 75);
            this.pbxThief.Name = "pbxThief";
            this.pbxThief.Size = new System.Drawing.Size(32, 32);
            this.pbxThief.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxThief.TabIndex = 2;
            this.pbxThief.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radMage);
            this.groupBox1.Controls.Add(this.radThief);
            this.groupBox1.Controls.Add(this.radKnight);
            this.groupBox1.Controls.Add(this.pbxThief);
            this.groupBox1.Controls.Add(this.pbxKnight);
            this.groupBox1.Controls.Add(this.pbxMage);
            this.groupBox1.Location = new System.Drawing.Point(12, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(147, 172);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // radKnight
            // 
            this.radKnight.AutoSize = true;
            this.radKnight.Location = new System.Drawing.Point(64, 26);
            this.radKnight.Name = "radKnight";
            this.radKnight.Size = new System.Drawing.Size(55, 17);
            this.radKnight.TabIndex = 3;
            this.radKnight.TabStop = true;
            this.radKnight.Text = "Knight";
            this.radKnight.UseVisualStyleBackColor = true;
            // 
            // radThief
            // 
            this.radThief.AutoSize = true;
            this.radThief.Location = new System.Drawing.Point(64, 82);
            this.radThief.Name = "radThief";
            this.radThief.Size = new System.Drawing.Size(49, 17);
            this.radThief.TabIndex = 4;
            this.radThief.TabStop = true;
            this.radThief.Text = "Thief";
            this.radThief.UseVisualStyleBackColor = true;
            // 
            // radMage
            // 
            this.radMage.AutoSize = true;
            this.radMage.Location = new System.Drawing.Point(64, 138);
            this.radMage.Name = "radMage";
            this.radMage.Size = new System.Drawing.Size(52, 17);
            this.radMage.TabIndex = 5;
            this.radMage.TabStop = true;
            this.radMage.Text = "Mage";
            this.radMage.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Location = new System.Drawing.Point(45, 180);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // ChooseClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(167, 209);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ChooseClass";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Choose Class";
            this.Load += new System.EventHandler(this.ChooseClass_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxKnight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxThief)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxKnight;
        private System.Windows.Forms.PictureBox pbxMage;
        private System.Windows.Forms.PictureBox pbxThief;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radMage;
        private System.Windows.Forms.RadioButton radThief;
        private System.Windows.Forms.RadioButton radKnight;
        private System.Windows.Forms.Button btnStart;
    }
}