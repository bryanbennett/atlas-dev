﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Atlas
{
    public partial class Launcher : Form
    {
        public Launcher()
        {
            InitializeComponent();
        }

        private void Launcher_Load(object sender, EventArgs e)
        {
            pbxHelm.Image = Image.FromFile("Launcher Resources\\helmicon.png");
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ChooseClass choose = new ChooseClass(this);
            choose.Show();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
