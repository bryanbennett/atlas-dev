<?xml version="1.0" encoding="UTF-8"?>
<tileset name="masterTileset" tilewidth="16" tileheight="16">
 <image source="../../AtlasContent/Tilesets/masterTileset.png" width="1024" height="1024"/>
 <terraintypes>
  <terrain name="New Terrain" tile="327"/>
 </terraintypes>
 <tile id="2">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="100">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="101">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="145">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="146">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="148">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="151">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="153">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="154">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="162">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="163">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="182">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="183">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="184">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="185">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="277">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="278">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="284">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="285">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="286">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="287">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="288">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="289">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="388">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="389">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="390">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="391">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="401">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="402">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="404">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="407">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="424">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="425">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="601">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="602">
  <properties>
   <property name="c" value="true"/>
  </properties>
 </tile>
 <tile id="640">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="641">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="642">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="643">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="668">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="669">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="670">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="671">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="732">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="735">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="796">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="799">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="860">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="861">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="862">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="863">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="3090">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="3091">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="3092">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="3093">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="3615">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="3616">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="3678">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="3679">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="3680">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
 <tile id="3681">
  <properties>
   <property name="c" value=""/>
  </properties>
 </tile>
</tileset>
