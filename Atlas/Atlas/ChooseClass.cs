﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Atlas
{
    public partial class ChooseClass : Form
    {
        private Launcher launcher;
        public ChooseClass(Launcher launcher)
        {
            InitializeComponent();
            this.launcher = launcher;
        }

        private void ChooseClass_Load(object sender, EventArgs e)
        {
            pbxKnight.Image = Image.FromFile("Launcher Resources\\knightsingle.png");
            pbxThief.Image = Image.FromFile("Launcher Resources\\thiefsingle.png");
            pbxMage.Image = Image.FromFile("Launcher Resources\\magesingle.png");
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            string select = "";

            if (radKnight.Checked)
            {
                select = "knight";
            }
            if (radThief.Checked)
            {
                select = "thief";
            }
            if (radMage.Checked)
            {
                select = "mage";
            }

            string myPath = System.Reflection.Assembly.GetEntryAssembly().Location;
            string myDir = System.IO.Path.GetDirectoryName(myPath);
            string path = System.IO.Path.Combine(myDir, "Atlas.exe");

            ProcessStartInfo startInfo = new ProcessStartInfo(path);
            startInfo.Arguments = select;
            Process.Start(startInfo);
            launcher.Close();
            this.Close();
        }
    }
}
