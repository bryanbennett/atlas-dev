﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atlas.Game.Enums;

namespace Atlas.Game.Data
{
    public class PlayerInfo
    {
        private Character type;
        private string name;
        private decimal maxHP;
        private decimal baseMaxHP;
        private decimal currentHP;
        private decimal maxMP;
        private decimal currentMP;
        private decimal maxStamina;
        private decimal currentStamina;
        private decimal maxDamage;
        private decimal minDamage;
        private decimal defense;
        private int xp = 0;
        private int nextXP = 1000;
        private int level = 1;


        //properties
        /// <summary>
        /// Returns the character type of the player.
        /// </summary>
        public Character Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
        /// <summary>
        /// Returns or sets the name of the player.
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        /// <summary>
        /// Returns or sets the maximum health of the player.
        /// </summary>
        public decimal MaximumHealth
        {
            get
            {
                return maxHP;
            }
            set
            {
                maxHP = value;
            }
        }
        public decimal BaseMaximumHealth
        {
            get
            {
                return baseMaxHP;
            }
            set
            {
                baseMaxHP = value;
            }
        }
        /// <summary>
        /// Returns or sets the current health of the player.
        /// </summary>
        public decimal Health
        {
            get
            {
                return currentHP;
            }
            set
            {
                currentHP = value;
                if (currentHP > maxHP)
                {
                    currentHP = maxHP;
                }
            }
        }
        /// <summary>
        /// Returns or sets the maximum mana of the player.
        /// </summary>
        public decimal MaximumMana
        {
            get
            {
                return maxMP;
            }
            set
            {
                maxMP = value;
            }
        }
        /// <summary>
        /// Returns or sets the current mana of the player.
        /// </summary>
        public decimal Mana
        {
            get
            {
                return currentMP;
            }
            set
            {
                currentMP = value;
            }
        }
        /// <summary>
        /// Returns or sets the maximum stamina of the player.
        /// </summary>
        public decimal MaximumStamina
        {
            get
            {
                return maxStamina;
            }
            set
            {
                maxStamina = value;
            }
        }
        /// <summary>
        /// Returns or sets the current stamina of the player.
        /// </summary>
        public decimal Stamina
        {
            get
            {
                return currentStamina;
            }
            set
            {
                currentStamina = value;
            }
        }
        public decimal Defense
        {
            get
            {
                return defense;
            }
            set
            {
                defense = value;
            }
        }
        public decimal MaxDamage
        {
            get
            {
                return maxDamage;
            }
            set
            {
                maxDamage = value;
            }
        }
        public decimal MinDamage
        {
            get
            {
                return minDamage;
            }
            set
            {
                minDamage = value;
            }
        }
        public int XP
        {
            get
            {
                return xp;
            }
            set
            {
                xp = value;
                if (xp >= nextXP)
                {
                    xp = 0;
                    nextXP = (int)(nextXP * Balance.PLAYER_XP_RATIO);
                    level += 1;
                }
            }
        }
        public int NextXP
        {
            get
            {
                return nextXP;
            }
        }
        public int Level
        {
            get
            {
                return level;
            }
            set
            {
                level = value;
            }
        }

        public PlayerInfo(Character type, string name)
        {
            this.name = name;
            this.type = type;
 
            switch (type)
            {
                case Character.Knight:
                    MaximumHealth = Balance.KNIGHT_START_HP;
                    Health = MaximumHealth;
                    MaximumMana = Balance.KNIGHT_START_MP;
                    Mana = MaximumMana;
                    MaximumStamina = Balance.KNIGHT_START_SP;
                    Stamina = MaximumStamina;
                    break;
                case Character.Mage:
                    MaximumHealth = Balance.MAGE_START_HP;
                    Health = MaximumHealth;
                    MaximumMana = Balance.MAGE_START_MP;
                    Mana = MaximumMana;
                    MaximumStamina = Balance.MAGE_START_SP;
                    Stamina = MaximumStamina;
                    break;
                case Character.Thief:
                    MaximumHealth = Balance.THIEF_START_HP;
                    Health = MaximumHealth;
                    MaximumMana = Balance.THIEF_START_MP;
                    Mana = MaximumMana;
                    MaximumStamina = Balance.THIEF_START_SP;
                    Stamina = MaximumStamina;
                    break;
            }
            //common
            BaseMaximumHealth = MaximumHealth;
        }


    }
}
