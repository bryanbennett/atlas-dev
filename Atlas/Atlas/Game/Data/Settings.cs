﻿
namespace Atlas.Game.Data
{
    public static class Settings
    {
        /// <summary>
        /// The delay in which a key event is fired to toggle a window.
        /// </summary>
        public const int WINDOW_TOGGLE_DELAY = 500;

        /// <summary>
        /// The amount of ambient light change that occurs when the 'sun' is setting or rising.
        /// </summary>
        public const float AMBIENT_LIGHTING_DELTA = .01f;


        /// <summary>
        /// The amount of time the game will tell you that you are dead before restarting.
        /// </summary>
        public const int DEATH_DISPLAY_TIME = 5000;

        /// <summary>
        /// The amount of time a zone will be displayed after entering it.
        /// </summary>
        public const int ZONE_DISPLAY_TIME = 5000;

        /// <summary>
        /// Amount of time in milliseconds before a tooltip is displayed.
        /// </summary>
        public const int TOOLTIP_DELAY = 350;

        /// <summary>
        /// The amount of time in milliseconds before an item despawns from the map.
        /// </summary>
        public const int MAP_ITEM_DESPAWN_TIME = 120000;

        /// <summary>
        /// The volume of sound effects.
        /// </summary>
        public const float SFX_VOLUME = .1f;

        /// <summary>
        /// Debug.
        /// </summary>
        public const bool RUN_LAUNCHER = false;
    }
}
