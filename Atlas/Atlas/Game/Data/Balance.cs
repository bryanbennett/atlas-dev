﻿
namespace Atlas.Game.Data
{
    public static class Balance
    {
        //knight
        public const decimal KNIGHT_START_HP = 75;
        public const decimal KNIGHT_START_MP = 5;
        public const decimal KNIGHT_START_SP = 100;
        public const string KNIGHT_START_WEP = "Broken Sword";
        public const string KNIGHT_START_CHEST = "Ragged Cloak";

        //mage
        public const decimal MAGE_START_HP = 35;
        public const decimal MAGE_START_MP = 100;
        public const decimal MAGE_START_SP = 80;
        public const string MAGE_START_WEP = "Broken Wand";
        public const string MAGE_START_CHEST = "Novice Robe";

        //thief
        public const decimal THIEF_START_HP = 55;
        public const decimal THIEF_START_MP = 40;
        public const decimal THIEF_START_SP = 120;
        public const string THIEF_START_WEP = "Damaged Short Bow";
        public const string THIEF_START_CHEST = "Ragged Cloak";

        //all classes
        public const float PLAYER_MISS_CHANCE = .1f;
        public const float PLAYER_XP_RATIO = 2.1f;

        //mobs
        public const int ITEM_DROP_CHANCE = 10; //percent chance
    }
}
