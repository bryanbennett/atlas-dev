﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Game.Data
{
    public struct SaveGameData
    {
        public string PlayerName;

        //the current map the player is on upon saving.
        public string CurrentMap;

        //the world position of the player upon saving.
        public Vector2 WorldPosition;

        //int is the index of the ItemBox and string is the name of the item that goes there
        public Dictionary<int, string> InventoryItems;

        //stats
        public decimal Health;
        public decimal Mana;
        public decimal Stamina;
        public int Level;
        public decimal XP;

        //the player's completed quests upon saving
        public string[] CompletedQuests;

        //the player's active quests upon saving
        public string[] ActiveQuests;
    }
}
