﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Atlas.Game.Environment;
using VintageStudios.Planar2D;
using VintageStudios.Planar2D.Visuals;
using VintageStudios.Planar2D.Shielder;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
using Atlas.Game.Data;

namespace Atlas.Game.Entity
{
    public class Mob : Agent
    {
        private Vector2 renderPosition;
        private float currentX;
        private float currentY;
        private float targetX;
        private float targetY;
        private Spawner spawn;
        private bool isDespawned;
        private int despawnTimer = 0;
        private bool inView;
        private bool hooked;
        private int idleTime;
        private int idleCount;
        private Player p;
        private GameWorld world;
        private Random r;
        private bool isIdling;
        private bool withinRange;
        private int moveCount = 0;
        private int collisionFlag;
        private int[,] collisionMap;
        MonsterInfo info;
        private Texture2D healthBar;
        private decimal currentHealth;
        private int attackTime = 1000;
        private SoundEffectInstance killInstance;
        private SoundEffectInstance hitInstance;

        public decimal CurrentHealth
        {
            get
            {
                return currentHealth;
            }
            set
            {
                currentHealth = value;
                if (currentHealth <= 0)
                {
                    killInstance.Play();
                }
            }
        }

        /// <summary>
        /// Returns the render position of this monster.
        /// </summary>
        public Vector2 RenderPosition
        {
            get
            {
                return renderPosition;
            }
            set
            {
                renderPosition = value;
            }
        }
        /// <summary>
        /// Determines whether or not the mob is despawned either by death or by time out.
        /// </summary>
        public bool Despawned
        {
            get
            {
                return isDespawned;
            }
            set
            {
                isDespawned = value;
            }
        }
        /// <summary>
        /// Returns or sets whether or not this mob is within the camera view.
        /// </summary>
        public bool InView
        {
            get
            {
                return inView;
            }
            set
            {
                inView = value;
            }
        }
        /// <summary>
        /// Returns the informations specific to this monster.
        /// </summary>
        public MonsterInfo Info
        {
            get
            {
                return info;
            }
        }
        /// <summary>
        /// Returns or sets whether or not this monster is currently hooked onto the player.
        /// </summary>
        public bool Hooked
        {
            get
            {
                return hooked;
            }
            set
            {
                hooked = value;
                if (value)
                {
                    isIdling = false;
                }
            }
        }

        public Mob(MonsterInfo info, Point worldPosition, Spawner spawn, Sprite sprite, Texture2D healthBar, SoundEffectInstance killInstance, SoundEffectInstance hitInstance)
            :base(sprite, worldPosition)
        {
            attackTime = (int)info.AttackSpeed;
            currentHealth = info.Health;
            float speed = (float) info.MovementSpeed;
            Speed = Speed * speed;
            currentX = worldPosition.X * 16;
            currentY = worldPosition.Y * 16;
            renderPosition = new Vector2(currentX, currentY);
            sprite.AnimationSpeed = 100;
            this.spawn = spawn;
            p = spawn.World.Player;
            world = spawn.World;
            collisionMap = world.CollisionMap;
            this.info = info;
            r = new Random(worldPosition.X + worldPosition.Y);
            collisionFlag = 1;
            this.healthBar = healthBar;
            this.killInstance = killInstance;
            this.hitInstance = hitInstance;
            AttackBox = new Rectangle((int)renderPosition.X - sprite.FrameSize.Width, (int)renderPosition.Y - sprite.FrameSize.Height, sprite.FrameSize.Width * 3, sprite.FrameSize.Height * 3);

            if (info.Flying)
            {
                sprite.ContinuousAnimation = true;
                sprite.AnimationSpeed = 150;
                collisionFlag = 1;
            }
        }
        public override void DrawAt(SpriteBatch batch, float offX, float offY)
        {
            float x = currentX - offX;
            float y = currentY - offY;
            RenderPosition = new Vector2(x, y);
            sprite.DrawAt(batch, (int)x, (int)y);

            if (Hooked)
            {
                Rectangle rect = new Rectangle((int)x, (int)y - 4, (int)(currentHealth / info.Health * sprite.FrameSize.Width),2);
                batch.Draw(healthBar, rect, Color.White);
            }
        }
        public override void Update(int delta)
        {
            attackTime += delta;

            if (attackTime >= (int)info.AttackSpeed)
            {
                attackTime = (int)info.AttackSpeed;
            }
            HitBox = new Rectangle((int)RenderPosition.X, (int)RenderPosition.Y, sprite.FrameSize.Width, sprite.FrameSize.Height);
            AttackBox = new Rectangle((int)renderPosition.X - sprite.FrameSize.Width/2, (int)renderPosition.Y - sprite.FrameSize.Height/2, sprite.FrameSize.Width * 2, sprite.FrameSize.Height * 2);
            Rectangle camView = spawn.World.CameraView;

            if (camView.Contains(TileX * 16, TileY * 16))
            {
                this.inView = true;
            }
            else if(camView.Contains(TileX * 16 + this.SpriteWidth, TileY * 16 + this.SpriteHeight))
            {
                this.inView = true;
            }
            else
            {
                this.inView = false;
            }

            int distance;
            int pX = p.TileX;
            int pY = p.TileY;
            int dX = pX - this.TileX;
            int dY = pY - this.TileY;

            if (this.inView)
            {
                dX *= dX;
                dY *= dY;
                distance = dX + dY;

                if (distance <= 100 && info.Hostile && !p.Dead)
                {
                    if (!Hooked)
                    {
                        Hooked = true;
                        moveCount = 0;
                        Speed = .25f;
                        this.sprite.AnimationSpeed = 50;
                        p.AddMob(this);
                    }
                    if (AttackBox.Contains(p.HitBox) || AttackBox.Intersects(p.HitBox))
                    {
                        withinRange = true;
                        moveCount = 0;
                    }
                    else
                    {
                        withinRange = false;
                    }
                }
            }

            if (!Moving && !hooked)
            {
                Idle(delta);
            }
            else if (Moving)
            {
                MovingUpdate(delta);
            }
            else if (Hooked)
            {
                HookedUpdate(delta);
            }
            sprite.Update(delta);
        }
        public override void Move(Facing face)
        {
            base.Move(face);
                if (!info.Flying && !world.hasCollided(this, collisionFlag, spawn.Map))
                {
                    switch (face)
                    {
                        case Facing.North:
                            targetX = currentX;
                            targetY = currentY - 16;
                            break;
                        case Facing.East:
                            targetX = currentX + 16;
                            targetY = currentY;
                            break;
                        case Facing.South:
                            targetX = currentX;
                            targetY = currentY + 16;
                            break;
                        case Facing.West:
                            targetX = currentX - 16;
                            targetY = currentY;
                            break;
                    }
                }
                else if (!info.Flying && world.hasCollided(this, collisionFlag, spawn.Map) && moveCount > 0)
                {
                    moveCount = 0;
                    Moving = false;
                }
                else if (info.Flying)
                {
                    switch (face)
                    {
                        case Facing.North:
                            targetX = currentX;
                            targetY = currentY - 16;
                            break;
                        case Facing.East:
                            targetX = currentX + 16;
                            targetY = currentY;
                            break;
                        case Facing.South:
                            targetX = currentX;
                            targetY = currentY + 16;
                            break;
                        case Facing.West:
                            targetX = currentX - 16;
                            targetY = currentY;
                            break;
                    }
                }
                else
                {
                    Moving = false;
                }
        }
        /// <summary>
        /// Overridable method for customized attacking logic.  Don't override for generic melee hits.
        /// </summary>
        public void Attack(int delta)
        {
            if (attackTime >= (int)info.AttackSpeed)
            {
                attackTime = 0;
                decimal php = (decimal) p.Info.Health;
                php -= Info.MaxDamage;
                p.Info.Health = (decimal)php;
                hitInstance.Play();
                despawnTimer = 0;
            }
        }
        /// <summary>
        /// Handles idle behavior
        /// </summary>
        /// <param name="delta"></param>
        private void Idle(int delta)
        {
            Point spawnPos = spawn.WorldPosition;
            int dx = spawnPos.X - this.TileX;
            int dy = spawnPos.Y - this.TileY;
            int distance = dx * dx + dy * dy;
            if (distance >= 400)
            {
                if (Math.Abs(dx) >= Math.Abs(dy))
                {
                    if (dx > 0)
                    {
                        Facing = Facing.East;
                        moveCount = 1;
                        Move(Facing.East);
                    }
                    else
                    {
                        Facing = Facing.West;
                        moveCount = 1;
                        Move(Facing.West);
                    }
                }
                else
                {
                    if (dy > 0)
                    {
                        Facing = Facing.South;
                        moveCount = 1;
                        Move(Facing.South);
                    }
                    else
                    {
                        Facing = Facing.North;
                        moveCount = 1;
                        Move(Facing.North);
                    }
                }
                return;
            }

            int num;
            if (!isIdling)
            {
                if (!info.Flying)
                {
                    num = r.Next(0, 10) + 1;
                }
                else
                {
                    num = 6;
                }

                if (num <= 5)
                {
                    idleTime = r.Next(0, 5) * 500;
                    isIdling = true;
                    idleCount = 0;
                }
                else
                {
                    num = r.Next(0, 4);
                    Facing temp = Facing.North;
                    if (num == 0)
                    {
                        if (Facing == Facing.South)
                        {
                            temp = Facing.East;
                        }
                    }
                    if (num == 1)
                    {
                        temp = Facing.South;
                        if (Facing == Facing.North)
                        {
                            temp = Facing.West;
                        }
                    }
                    if (num == 2)
                    {
                        temp = Facing.East;
                        if (Facing == Facing.West)
                        {
                            temp = Facing.South;
                        }
                    }
                    if (num == 3)
                    {
                        temp = Facing.West;
                        if (Facing == Facing.East)
                        {
                            temp = Facing.North;
                        }
                    }
                    Facing = temp;
                    Move(temp);
                    moveCount = r.Next(0, 7) + 1;
                }
            }
            else
            {
                idleCount += delta;
                if (idleCount >= idleTime)
                {
                    isIdling = false;
                }
            }
        }
        /// <summary>
        /// Handles the moving update.
        /// </summary>
        /// <param name="delta"></param>
        private void MovingUpdate(int delta)
        {
            Facing face = Facing;
            Point tile = TilePosition;
            switch (face)
            {
                case Facing.North:
                    currentY -= Speed * delta;
                    if (currentY <= targetY)
                    {
                        Moving = false;
                        currentY = targetY;
                        tile.Y -= 1;
                        TilePosition = tile;
                        if (moveCount > 0)
                        {
                            moveCount -= 1;
                            Move(Facing);
                        }
                    }
                    break;
                case Facing.East:
                    currentX += Speed * delta;
                    if (currentX >= targetX)
                    {
                        Moving = false;
                        currentX = targetX;
                        tile.X += 1;
                        TilePosition = tile;
                        if (moveCount > 0)
                        {
                            moveCount -= 1;
                            Move(Facing);
                        }
                    }
                    break;
                case Facing.South:
                    currentY += Speed * delta;
                    if (currentY >= targetY)
                    {
                        Moving = false;
                        currentY = targetY;
                        tile.Y += 1;
                        TilePosition = tile;
                        if (moveCount > 0)
                        {
                            moveCount -= 1;
                            Move(Facing);
                        }
                    }
                    break;
                case Facing.West:
                    currentX -= Speed * delta;
                    if (currentX <= targetX)
                    {
                        Moving = false;
                        currentX = targetX;
                        tile.X -= 1;
                        TilePosition = tile;
                        if (moveCount > 0)
                        {
                            moveCount -= 1;
                            Move(Facing);
                        }
                    }
                    break;
            }
            
        }
        /// <summary>
        /// Handles the hooked update.
        /// </summary>
        /// <param name="delta"></param>
        private void HookedUpdate(int delta)
        {
            despawnTimer += delta;
            if (despawnTimer >= 20000 && !isDespawned)
            {
                isDespawned = true;
                despawnTimer = 0;
                p.RemoveMob(this);
            }


            int pX = p.TileX;
            int pY = p.TileY;
            int dX = pX - this.TileX;
            int dY = pY - this.TileY;

            if (!Moving && !withinRange && moveCount <= 0)
            {
                dX = pX - TileX;
                dY = pY - TileY;
                bool positiveX = false;
                bool positiveY = false;

                if (dX >= 0)
                {
                    positiveX = true;
                }
                if (dY >= 0)
                {
                    positiveY = true;
                }

                if (info.Flying)
                {
                    if (Math.Abs(dX) >= Math.Abs(dY))
                    {
                        if (positiveX)
                        {
                            Facing = Facing.East;
                            Move(Facing);
                        }
                        else
                        {
                            Facing = Facing.West;
                            Move(Facing);
                        }
                    }
                    else
                    {
                        if (positiveY)
                        {
                            Facing = Facing.South;
                            Move(Facing);
                        }
                        else
                        {
                            Facing = Facing.North;
                            Move(Facing);
                        }
                    }
                }
                else
                {
                    if (Math.Abs(dX) >= Math.Abs(dY))
                    {
                        if (positiveX)
                        {
                            Facing = Facing.East;
                            if (!world.hasCollided(this, collisionFlag, spawn.Map))
                            {
                                Move(Facing);
                            }
                            else
                            {
                                if (positiveY)
                                {
                                    Facing = Facing.South;
                                    moveCount = sprite.TileSize.Y;
                                    Move(Facing);
                                }
                                else
                                {
                                    Facing = Facing.North;
                                    moveCount = sprite.TileSize.Y;
                                    Move(Facing);
                                }
                            }
                        }
                        else
                        {
                            Facing = Facing.West;
                            if (!world.hasCollided(this, collisionFlag, spawn.Map))
                            {
                                Move(Facing);
                            }
                            else
                            {
                                if (positiveY)
                                {
                                    Facing = Facing.South;
                                    moveCount = sprite.TileSize.Y;
                                    Move(Facing);
                                }
                                else
                                {
                                    Facing = Facing.North;
                                    moveCount = sprite.TileSize.Y;
                                    Move(Facing);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (positiveY)
                        {
                            Facing = Facing.South;
                            if (!world.hasCollided(this, collisionFlag, spawn.Map))
                            {
                                Move(Facing);
                            }
                            else
                            {
                                if (positiveX)
                                {
                                    Facing = Facing.East;
                                    moveCount = sprite.TileSize.X;
                                    Move(Facing);
                                }
                                else
                                {
                                    Facing = Facing.West;
                                    moveCount = sprite.TileSize.X;
                                    Move(Facing);
                                }
                            }
                        }
                        else
                        {
                            Facing = Facing.North;
                            if (!world.hasCollided(this, collisionFlag, spawn.Map))
                            {
                                Move(Facing);
                            }
                            else
                            {
                                if (positiveX)
                                {
                                    Facing = Facing.East;
                                    moveCount = sprite.TileSize.X;
                                    Move(Facing);
                                }
                                else
                                {
                                    Facing = Facing.West;
                                    moveCount = sprite.TileSize.X;
                                    Move(Facing);
                                }
                            }
                        }
                    }

                }

            }

            if (withinRange)
            {
                Attack(delta);

            }
            else
            {
                //do nothing
            }
        }
        public void GenerateItem()
        {
            Random r = new Random();
            if (info.ItemDropper)
            {
                int chance = (int) info.DropChance;
                int rand = r.Next(100) + 1;
                if (rand <= chance)
                {
                    world.GenerateItem(this.TilePosition, info.DroppedItem);
                }
            }
            else
            {
                int rand = r.Next(100) + 1;
                if (rand <= Balance.ITEM_DROP_CHANCE)
                {
                    world.GenerateItem(this.TilePosition, (int)info.Level);
                }
            }
        }
    }
}
