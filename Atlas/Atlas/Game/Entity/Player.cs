﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using VintageStudios.Planar2D;
using VintageStudios.Planar2D.Visuals;
using Atlas.Game.Environment;
using Atlas.Game.Data;
using Atlas.Game.Enums;
using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Atlas.Game.Items;
using VintageStudios.Planar2D.Shielder;

namespace Atlas.Game.Entity
{
    public class Player : Agent
	{
		private GameWorld world;
		private Camera camera;
        private static Point renderPosition = new Point(1280 / 2 - 16, 720 / 2 - 16);
        private Vector2 worldPosition;
		private Point targetPosition;
        private Point startPos;
        private PlayerInfo info;
        private List<Mob> hookedMobs;
        private int attackTime = 1000;
        private int attackSpeed = 1000;
        private bool attacking;
        private SoundEffect attack;
        private SoundEffectInstance attackInstance;
        private SoundEffect miss;
        private SoundEffectInstance missInstance;
        private SoundEffect stepSound;
        private SoundEffectInstance stepSoundInstance;
        private SoundEffect deathSound;
        private SoundEffectInstance deathSoundInstance;
        private Texture2D blood;
        private bool playStepSound = false;
        private bool deathSoundPlayed;
        private int stepCount = 0;
        private float missChance = Balance.PLAYER_MISS_CHANCE;
        private int deathTime = 0;
        private Rectangle facingRect;
        private string zone = "";
        private bool showZone;
        private int showZoneTime = 0;
        private InventoryManager inv;
        private List<ItemBox> boxes;
        private int equippedIndex;
        private bool hasInventory;
        private string[] startGear;
        private bool teleported;

        public string[] StartGear
        {
            get
            {
                return startGear;
            }
            set
            {
                startGear = value;
            }
        }
        public bool Attacking
        {
            get { return attacking; }
			set { attacking = value; }
        }
        public bool Teleported
        {
            get { return teleported; }
            set { teleported = value; }
        }

        public InventoryManager Inventory
        {
			get { return inv; }
            set
            {
                inv = value;
                boxes = inv.ItemBoxes;
                equippedIndex = inv.EquippedIndex;
                hasInventory = true;
            }
        }
        /// <summary>
        /// Returns or sets the world position of the player (tile coordinates).
        /// </summary>
        public Vector2 WorldPosition
        {
			get { return worldPosition; }
			set { worldPosition = value; }
        }

		public Camera Camera
		{
			set { this.camera = value; }
		}

        public PlayerInfo Info
        {
			get { return info; }
        }

        public bool PlayStepSound
        {
			get { return playStepSound; }
            set
            {
                if (value)
                    stepCount++;
                if (stepCount == 3)
                    playStepSound = value;
            }
        }

        public bool Dead
        {
            get
            {
                if (info.Health <= 0)
                    return true;
                else
                    return false;
            }
        }

        public bool ShowZone
        {
			get { return showZone; }
            set
            {
                showZone = value;
                showZoneTime = 0;
            }
        }

        public string Zone
        {
			get { return zone; }
			set { zone = value; }
        }

        public int HookedMobCount
        {
			get { return hookedMobs.Count; }
        }


        public Player(Sprite sprite, Point worldLocation, GameWorld world, Character type, string name, ContentManager content)
            :base(sprite, worldLocation)
        {
            int x = TileX;
            int y = TileY;
			this.world = world;
            startPos = worldLocation;
			worldPosition = new Vector2(worldLocation.X, worldLocation.Y);
			targetPosition = worldLocation;
			this.Speed = 0.0000001f;
            sprite.AnimationSpeed = 25;
            info = new PlayerInfo(type, name);
            hookedMobs = new List<Mob>();
            HitBox = new Rectangle(renderPosition.X, renderPosition.Y, 32, 32);
            AttackBox = new Rectangle(renderPosition.X - sprite.FrameSize.Width, renderPosition.Y - sprite.FrameSize.Height, sprite.FrameSize.Width * 3, sprite.FrameSize.Height * 3);
            attack = content.Load<SoundEffect>("Sound\\sfx\\sound2");
            attackInstance = attack.CreateInstance();
            miss = content.Load<SoundEffect>("Sound\\sfx\\sound3");
            missInstance = miss.CreateInstance();
            stepSound = content.Load<SoundEffect>("Sound\\sfx\\sound4");
            stepSoundInstance = stepSound.CreateInstance();
            deathSound = content.Load<SoundEffect>("Sound\\sfx\\sound8");
            deathSoundInstance = deathSound.CreateInstance();
            attackInstance.Volume = Settings.SFX_VOLUME;
            missInstance.Volume = Settings.SFX_VOLUME;
            stepSoundInstance.Volume = Settings.SFX_VOLUME;
            deathSoundInstance.Volume = Settings.SFX_VOLUME;

            blood = content.Load<Texture2D>("Images\\bloodSplat");
            zone = "Homestead";
            showZone = true;
            SetFacingRect();
        }


        public void AddMob(Mob m)
        {
            hookedMobs.Add(m);
        }

        public void RemoveMob(Mob m)
        {
            hookedMobs.Remove(m);
        }

        public bool AddItem(Item item)
        {
            return inv.AddItem(item);
        }

        public override void DrawAt(SpriteBatch batch, float offX, float offY)
        {
            if (info.Health > 0)
            {
				renderPosition.X = (int)(worldPosition.X * 16 - offX);
				renderPosition.Y = (int)(worldPosition.Y * 16 - offY);
                sprite.DrawAt(batch, renderPosition.X, renderPosition.Y);
				
				sprite.DrawAt(batch, (int)(worldPosition.X * 16 - offX), (int)(worldPosition.Y * 16 - offY));
				//Texture2D attackBoxDraw = new Texture2D(world.Device, AttackBox.Width, AttackBox.Height);
				//batch.Draw(attackBoxDraw, AttackBox, Color.Black);
            }
            else
            {
                batch.Draw(blood, HitBox, Color.White);
            }
        }


        public override void Update(int delta)
        {
            sprite.Update(delta);
            if (teleported)
            {
                targetPosition.X = (int)worldPosition.X;
                targetPosition.Y = (int)worldPosition.Y;
                TileX = targetPosition.X;
                TileY = targetPosition.Y;
                teleported = false;
                Moving = false;
            }
			if (targetPosition.X != worldPosition.X || targetPosition.Y != worldPosition.Y)
			{
				bool moveCompleted = false;

				switch (Facing)
				{
					case Facing.North:
						worldPosition.Y -= Speed * delta; //changes the player's offset (the positivity or negativity of X or Y being determined by the Facing value) based on the speed of the player and how much time has passed
						if (worldPosition.Y <= targetPosition.Y)          //if the offset is greater than 16 (the width of a tile, aka 1 complete movement, wrap up the movement).
						{
							moveCompleted = true;
							worldPosition.Y = targetPosition.Y;   //changes the tile location (world location) of the player.
							TileY = targetPosition.Y;
						}
						break;
					case Facing.East:
						worldPosition.X += Speed * delta;
						if (worldPosition.X >= targetPosition.X)
						{
							moveCompleted = true;
							worldPosition.X = targetPosition.X;
							TileX = targetPosition.X;
						}
						break;
					case Facing.South:
						worldPosition.Y += Speed * delta;
						if (worldPosition.Y >= targetPosition.Y)
						{
							moveCompleted = true;
							worldPosition.Y = targetPosition.Y;
							TileY = targetPosition.Y;
						}
						break;
					case Facing.West:
						worldPosition.X -= Speed * delta;
						if (worldPosition.X <= targetPosition.X)
						{
							moveCompleted = true;
							worldPosition.X = targetPosition.X;
							TileX = targetPosition.X;
						}
						break;
				}
				if (moveCompleted)
				{
					PlayStepSound = true;
					Moving = false;
				}

			}

			HitBox = new Rectangle((int)worldPosition.X * 16 - camera.View.X, (int)worldPosition.Y * 16 - camera.View.Y, 32, 32);
			AttackBox = new Rectangle((int)worldPosition.X * 16 - sprite.FrameSize.Width - camera.View.X, (int)worldPosition.Y * 16 - sprite.FrameSize.Height - camera.View.Y, sprite.FrameSize.Width * 3, sprite.FrameSize.Height * 3);


            if (hasInventory)
            {
                CalculateEquippedStats();
            }

            if (Dead && !deathSoundPlayed)
            {
                deathSoundInstance.Play();
                deathSoundPlayed = true;
            }
            else if (Dead)
            {
                deathTime += delta;
                if (deathTime >= Settings.DEATH_DISPLAY_TIME)
                {
                    Reset();
                }
            }

            if (showZone)
            {
                showZoneTime += delta;
                if (showZoneTime >= Settings.ZONE_DISPLAY_TIME)
                {
                    showZone = false;
                }
            }

            if (playStepSound)
            {
                stepSoundInstance.Play();
                playStepSound = false;
                stepCount = 0;
            }

            attackTime += delta;
            if (attackTime >= attackSpeed)
            {
                attackTime = attackSpeed;
            }

            ValidateMobs();

            if (attacking)
            {
                AttackUpdate();
            }
        }
        /// <summary>
        /// Calculates the buffs received from equipped items and sets the player's values appropriately.
        /// </summary>
        private void CalculateEquippedStats()
        {
            boxes = inv.ItemBoxes;
            int count = boxes.Count;
            decimal maxHP = 0;
            decimal maxDamage = 0;
            decimal minDamage = 0;
            decimal atkSpeed = 0;
            decimal defense = 0;
            for (int i = equippedIndex; i < count; i++)
            {
                if (boxes[i].HasItem)
                {
                    ItemInfo temp = boxes[i].Item.Info;
                    maxHP += temp.Health;
                    maxDamage += temp.MaxDamage;
                    minDamage += temp.MinDamage;
                    atkSpeed += temp.AttackSpeed;
                    defense += temp.Defense;
                }
            }
            info.MaximumHealth = maxHP + info.BaseMaximumHealth;
            info.MaxDamage = maxDamage;
            info.MinDamage = minDamage;
            //info.AttackSpeed
            info.Defense = defense;
        }
        /// <summary>
        /// Method used to 'clean' up dead or despawned mobs from the hooked mobs list in a safe way.
        /// </summary>
        private void ValidateMobs()
        {
            List<Mob> removeMobs = new List<Mob>();
            foreach (Mob mob in hookedMobs)
            {
                if (Dead)
                {
                    removeMobs.Add(mob);
                }
                else
                {
                    //this might be redundant
                    if (mob.Despawned)
                    {
                        removeMobs.Add(mob);
                    }
                }
            }
            foreach (Mob mob in removeMobs)
            {
                mob.Hooked = false;
                hookedMobs.Remove(mob);
            }
        }
        /// <summary>
        /// Toggles the attacking boolean so that the update method will handle the attacking stuff.
        /// </summary>
        public void Attack()
        {
            if (!attacking)
            {
                attacking = true;
            }
        }
        /// <summary>
        /// The update logic for the player when attacking.
        /// </summary>
        private void AttackUpdate()
        {
            attacking = false;
            if (attackTime >= attackSpeed)
            {
                attackTime = 0;
                bool playHit = false;
                bool playMiss = false;
                List<Mob> killedMobs = new List<Mob>();
                SetFacingRect();
                foreach (Mob mob in hookedMobs)
                {
                    if (facingRect.Intersects(mob.HitBox) || facingRect.Contains(mob.HitBox))
                    {
                        Random r = new Random();
                        int rand = r.Next(100) + 1;
                        if (rand >= missChance * 100)
                        {
                            rand = r.Next((int)info.MaxDamage - (int)info.MinDamage + 1) + (int)info.MinDamage;
                            playHit = true;
                            decimal mhp = mob.CurrentHealth;
                            mhp -= rand;
                            mob.CurrentHealth = mhp;
                            if (mob.CurrentHealth <= 0)
                            {
                                killedMobs.Add(mob);
                            }
                        }
                        else
                        {
                            playMiss = true;
                        }
                    }
                }
                foreach (Mob mob in killedMobs)
                {
                    mob.GenerateItem();
                    hookedMobs.Remove(mob);
                    mob.Despawned = true;
                    info.XP = (int) (info.XP + mob.Info.Experience);
                }
                if (playHit)
                {
                    attackInstance.Play();
                }
                if (playMiss)
                {
                    missInstance.Play();
                }
            }
        }
        /// <summary>
        /// Resets certain values.  (usually called after death).
        /// </summary>
        private void Reset()
        {
            WorldPosition = new Vector2(startPos.X, startPos.Y);
            TileX = (int)WorldPosition.X;
            TileY = (int)WorldPosition.Y;
            targetPosition = TilePosition;
            info.Health = info.MaximumHealth;
            deathSoundPlayed = false;
            deathTime = 0;   
        }
        /// <summary>
        /// Returns whether or not the player can move based on the player's interaction with monsters
        /// that are currently hooked.
        /// </summary>
        /// <returns></returns>
        public bool CanMove()
        {
            if (Dead)
            {
                return false;
            }
            else
            {
                SetFacingRect();
                foreach (Mob mob in hookedMobs)
                {
                    if (mob.HitBox.Contains(facingRect) || mob.HitBox.Intersects(facingRect))
                    {
                        return false;
                    }
                }
                return true;
            }
        }

		public bool canMove()
		{
			bool worldSaysOk = !world.hasCollided(this, 1, world.CurrentMap);

			if (worldSaysOk && this.CanMove())
				return true;
			else
				return false;
		}

		public override void Move(VintageStudios.Planar2D.Facing face)
		{
			this.Facing = face;
			switch (face)
			{
				case Facing.North:
					targetPosition.Y--;
					break;
				case Facing.East:
					targetPosition.X++;
					break;
				case Facing.South:
					targetPosition.Y++;
					break;
				case Facing.West:
					targetPosition.X--;
					break;
			}
			base.Move(face);
		}
		
        /// <summary>
        /// Sets the rectangle that is "in front of the player" (depending on which way it is facing) to determine some types
        /// of collision like determining if a monster is within melee distance.
        /// </summary>
        private void SetFacingRect()
        {
            switch (Facing)
            {
                case Facing.North:
					facingRect = new Rectangle((int)worldPosition.X * 16, (int)worldPosition.Y * 16 - 32, 32, 32);
                    break;
                case Facing.South:
					facingRect = new Rectangle((int)worldPosition.X * 16, (int)worldPosition.Y * 16 + 32, 32, 32);
                    break;
                case Facing.East:
					facingRect = new Rectangle((int)worldPosition.X * 16 + 32, (int)worldPosition.Y * 16, 32, 32);
                    break;
                case Facing.West:
					facingRect = new Rectangle((int)worldPosition.X * 16 - 32, (int)worldPosition.Y * 16, 32, 32);
                    break;
            }
        }
    }
}
