﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using VintageStudios.Planar2D;
using VintageStudios.Planar2D.Visuals;
using Atlas.Game.Environment;

namespace Atlas.Game.Entity
{
    public abstract class Agent
    {
        protected Sprite sprite;
        private Point tilePosition;
        private Facing facing = Facing.South;
        private float speed = .125f;
        private bool isMoving;
        private int attackRange = 0;
        private Rectangle hitBox;
        private Rectangle atkBox;

        public int AttackRange
        {
			set { attackRange = value; }
        }

        public Rectangle HitBox
        {
			get { return hitBox; }
			set { hitBox = value; }
        }

        public Rectangle AttackBox
        {
			get { return atkBox; }
			set { atkBox = value; }
        }

        /// <summary>
        /// Returns or sets the X-coordinate position of this agent within world space.
        /// </summary>
        public int TileX
        {
            get { return tilePosition.X;}
			set { tilePosition.X = value; }
        }

        /// <summary>
        /// Returns or sets the Y-coordinate position of this agent within world space.
        /// </summary>
        public int TileY
        {
			get { return tilePosition.Y; }
			set { tilePosition.Y = value; }
        }

        /// <summary>
        /// Returns or sets the Point (x,y) positions of this agent within world space.
        /// </summary>
        public Point TilePosition
        {
			get { return tilePosition; }
			set { tilePosition = value; }
        }

        /// <summary>
        /// Returns or sets the Facing value (North, West, South, East) of this agent.
        /// </summary>
        public Facing Facing
        {
			get { return facing; }
            set 
			{
                facing = value;
                sprite.Facing = value;
            }
        }

        /// <summary>
        /// Returns or sets the current speed at which this agent moves.  Default is .25f.
        /// </summary>
        public float Speed
        {
			get { return speed; }
			set { speed = value; }
        }

        /// <summary>
        /// Sets the animation speed of the sprite.
        /// </summary>
        public int AnimationSpeed
        {
			set { sprite.AnimationSpeed = value; }
        }

        /// <summary>
        /// Returns or sets whether or not this agent is moving.  If it is set to true, the sprite is automatically set to moving as well.  False disables sprite movement.
        /// </summary>
        public bool Moving
        {
			get { return isMoving; }
            set
            {
                isMoving = value;
                if (isMoving)
                    sprite.Moving = true;
                else
                    sprite.Moving = false;
            }
        }

        /// <summary>
        /// Returns the height of the sprite.
        /// </summary>
        public int SpriteHeight
        {
			get { return sprite.FrameSize.Height; }
        }

        /// <summary>
        /// Returns the width of the sprite.
        /// </summary>
        public int SpriteWidth
        {
			get { return sprite.FrameSize.Width; }
        }

        /// <summary>
        /// Returns or sets the sprite associated with this agent.
        /// </summary>
        public Sprite Sprite
        {
			get { return sprite; }
			set { sprite = value; }
        }

        public Agent(Sprite sprite, Point tilePosition) 
        {
            this.sprite = sprite;
            this.tilePosition = tilePosition;
            hitBox = new Rectangle(tilePosition.X * 16, tilePosition.Y * 16, sprite.FrameSize.Width, sprite.FrameSize.Height);
        }

        /// <summary>
        /// Main movement method to be overwritten in subclasses (base.Move(Facing) should still be called, otherwise manually set Facing and Moving properties).
        /// </summary>
        /// <param name="face"></param>
        public virtual void Move(Facing face)
        {
            this.Facing = face;
            this.Moving = true;
        }

        /// <summary>
        /// Draws the sprite at a specific location.  Useful if you need to do calculations elsewhere to determine offset.
        /// </summary>
        /// <param name="batch"></param>
        /// <param name="offX"></param>
        /// <param name="offY"></param>
        public abstract void DrawAt(SpriteBatch batch, float offX, float offY);

        public abstract void Update(int delta);

    }
}
