﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuncWorks.XNA.XTiled;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections;
using Atlas.Game.Entity;
using VintageStudios.Planar2D.Shielder;
using VintageStudios.Planar2D.Visuals.Lighting;
using Atlas.Game.Items;

namespace Atlas.Game.Environment
{
    public class PlanarMap
    {
        private Map map;
        private int[,] collisionMap;
        private GraphicsDevice device;
        private Point playerEntryPoint;
        private GameWorld world;
        private List<Point> lights;
        private List<Spawner> allSpawners;
        private List<Spawner> zoneSpawners;
        private List<Healer> healers;
        private List<Zone> zones;
        private List<Item> itemsOnMap;
        private List<Teleporter> teles;
        private Dictionary<string, List<Spawner>> spawnersByZones;
        private Dictionary<string, List<Healer>> healersByZones;
        private Dictionary<string, List<Teleporter>> telesByZones;
        private int numOfSpawns;
        private TileBasedLightingSystem sys;
        private Rectangle camView;
        private Queue<Spawner> flyingMobs;
        //private List<Mob> mobsInView;
        private bool effectedByAmbientLight = true;
        private float ambient;

        public PlanarMap(GameWorld world, Map map)
        {
            this.map = map;
            this.device = world.Device;
            this.world = world;
            flyingMobs = new Queue<Spawner>();
            //mobsInView = new List<Mob>();
            SetCollisionMap();
            GenerateMapObjects();
            SetLightMap();
        }
        private void SetCollisionMap()
        {
            int mapWidthInTiles = map.Width;
            int mapHeightInTiles = map.Height;
            collisionMap = new int[mapWidthInTiles, mapHeightInTiles];
            ArrayList collisionIDs = new ArrayList();


            TileLayerList layers = map.TileLayers;
            int numOfLayers = layers.Count;
            Tileset[] sets = map.Tilesets;
            int numOfSets = sets.Length;
            Tile[] sourceTiles = map.SourceTiles;
            int numOfSourceTiles = sourceTiles.Length;

            //builds the tile ID's that are collidable
            for (int i = 0; i < numOfSourceTiles; i++)
            {
                if (sourceTiles[i].Properties.Count == 0)
                {
                    continue;
                }
                else
                {
                    Dictionary<string, Property> props = sourceTiles[i].Properties;
                    if (props.ContainsKey("c"))
                    {
                        collisionIDs.Add(i);
                    }
                }
            }

            //builds the actual collision map by iterating through the layers, finding tiles with the collidable IDs, and inputs a 1 into the array
            for (int h = 0; h < 4; h++)
            {
                TileData[][] tiles = layers[h].Tiles;
                //column-major
                for (int i = 0; i < mapWidthInTiles; i++)
                {
                    TileData[] temp = (TileData[])tiles[i];
                    for (int j = 0; j < mapHeightInTiles; j++)
                    {
                        if (temp[j] == null)
                        {
                            continue;
                        }
                        int tileID = temp[j].SourceID;

                        for (int k = 0; k < collisionIDs.Count; k++)
                        {
                            int id = (int)collisionIDs[k];
                            if (tileID == id)
                            {
                                collisionMap[i, j] = 1;
                                break;
                            }
                            else if (collisionMap[i, j] != 1)
                            {
                                collisionMap[i, j] = 0;
                            }
                        }
                    }
                }
            }
        }
        private void GenerateMapObjects()
        {
            allSpawners = new List<Spawner>();
            lights = new List<Point>();
            healers = new List<Healer>();
            zones = new List<Zone>();
            itemsOnMap = new List<Item>();
            teles = new List<Teleporter>();

            telesByZones = new Dictionary<string, List<Teleporter>>();
            spawnersByZones = new Dictionary<string, List<Spawner>>();
            healersByZones = new Dictionary<string, List<Healer>>();

            int numOfObjLayers = map.ObjectLayers.Count;

            for (int i = 0; i < numOfObjLayers; i++)
            {
                int numOfObjects = map.ObjectLayers[i].MapObjects.Length;
                for (int j = 0; j < numOfObjects; j++)
                {
                    string objType = map.ObjectLayers[i].MapObjects[j].Type;
                    if (objType.Equals("Start"))
                    {
                        MapObject mo = map.ObjectLayers[0].MapObjects[j];
                        Rectangle rect = mo.Bounds;
                        int x = rect.X / 16;
                        int y = rect.Y / 16;
                        playerEntryPoint= new Point(x, y);
                    }
                    if (objType.Equals("Spawn"))
                    {
                        MapObject mo = map.ObjectLayers[0].MapObjects[j];
                        Rectangle rect = mo.Bounds;
                        int x = rect.X / 16;
                        int y = rect.Y / 16;
                        Point position = new Point(x, y);
                        string name = mo.Name;
                        string zone = mo.Properties["zone"].Value;
                        MonsterInfo mp = world.Manager.GetMonsterInfo(name);
                        Spawner spawn = new Spawner(mp, this, position, rect, zone);
                        allSpawners.Add(spawn);
                    }
                    if (objType.Equals("Light"))
                    {
                        MapObject mo = map.ObjectLayers[0].MapObjects[j];
                        Rectangle rect = mo.Bounds;
                        int x = rect.X / 16;
                        int y = rect.Y / 16;
                        Point light = new Point(x, y);
                        lights.Add(light);
                    }
                    if (objType.Equals("Healer"))
                    {
                        MapObject mo = map.ObjectLayers[0].MapObjects[j];
                        Rectangle rect = mo.Bounds;
                        string zone = mo.Properties["zone"].Value;
                        Healer h = new Healer(zone, rect);
                        healers.Add(h);
                    }
                    if (objType.Equals("Area"))
                    {
                        MapObject mo = map.ObjectLayers[0].MapObjects[j];
                        Rectangle rect = mo.Bounds;
                        string zoneName = mo.Name;
                        Zone zone = new Zone(rect, zoneName);
                        zones.Add(zone);
                    }
                    if (objType.Equals("Teleport"))
                    {
                        MapObject mo = map.ObjectLayers[0].MapObjects[j];
                        Rectangle rect = mo.Bounds;
                        Property toMap =  mo.Properties["toMap"];
                        string targetMap = toMap.Value;
                        string zone = mo.Properties["zone"].Value;
                        Property tile = mo.Properties["tile"];
                        string temp = tile.Value;
                        string[] temp2 = temp.Split(new Char[] {','});
                        Point targetTile = new Point(Int32.Parse(temp2[0]), Int32.Parse(temp2[1]));
                        Teleporter tele = new Teleporter(targetMap, targetTile, rect, zone);
                        teles.Add(tele);
                    }
                }
            }
            numOfSpawns = allSpawners.Count;

            //generate lists by zones

            //healers

            foreach (Healer h in healers)
            {
                string zone = h.Zone;
                if (healersByZones.ContainsKey(zone))
                {
                    List<Healer> temp = healersByZones[zone];
                    temp.Add(h);
                }
                else
                {
                    List<Healer> temp = new List<Healer>();
                    healersByZones.Add(zone, temp);
                    temp.Add(h);
                }
            }
            foreach (Spawner s in allSpawners)
            {
                string zone = s.Zone;
                if (spawnersByZones.ContainsKey(zone))
                {
                    List<Spawner> temp = spawnersByZones[zone];
                    temp.Add(s);
                }
                else
                {
                    List<Spawner> temp = new List<Spawner>();
                    spawnersByZones.Add(zone, temp);
                    temp.Add(s);
                }
            }
            foreach (Teleporter t in teles)
            {
                string zone = t.Zone;
                if (telesByZones.ContainsKey(zone))
                {
                    List<Teleporter> temp = telesByZones[zone];
                    temp.Add(t);
                }
                else
                {
                    List<Teleporter> temp = new List<Teleporter>();
                    telesByZones.Add(zone, temp);
                    temp.Add(t);
                }
            }

        }
        private void SetLightMap()
        {
            Texture2D tile = new Texture2D(world.Device, 1, 1);
            tile.SetData(new[] { Color.Black });
            sys = new TileBasedLightingSystem(lights, tile, map.Width, map.Height, 16);
        }
        public void DrawBG(SpriteBatch batch, Rectangle view)
        {
            //Map.DrawLayer(batch, 0, view, 0); //(alpha layer)
            Map.DrawLayer(batch, 1, view, 0);
            Map.DrawLayer(batch, 2, view, 0);
            Map.DrawLayer(batch, 3, view, 0);

            int count = ItemsOnMap.Count;
            for (int i = 0; i < count; i++)
            {
                Item item = itemsOnMap[i];
                item.DrawAt(batch, view.X, view.Y);
            }
        }
        public void DrawFG(SpriteBatch batch, Rectangle view)
        {
            camView = view;
            for (int i = 0; i < numOfSpawns; i++)
            {
                Spawner s = allSpawners[i];
                if (!s.HasMob)
                {
                    continue;
                }
                Mob m = s.Mob;
                if (!m.Info.Flying)
                {
                    allSpawners[i].DrawMob(batch, view.X, view.Y);
                }
                else
                {
                    flyingMobs.Enqueue(allSpawners[i]);
                }
            }
            Map.DrawLayer(batch, 4, view, .5f);
            Map.DrawLayer(batch, 5, view, .5f);

            int numOfFlyingMobs = flyingMobs.Count;

            for (int i = 0; i < numOfFlyingMobs; i++)
            {
                Spawner s = flyingMobs.Dequeue();
                s.DrawMob(batch, view.X, view.Y);
            }
            sys.Draw(batch, view);
        }
        public void Update(int delta)
        {
            Player p = world.Player;
            string zone = p.Zone;

            if (!p.Dead)
            {
                if (healersByZones.ContainsKey(zone))
                {
                    healers = healersByZones[zone];
                }
                if (spawnersByZones.ContainsKey(zone))
                {
                    zoneSpawners = spawnersByZones[zone];
                }
                if (telesByZones.ContainsKey(zone))
                {
                    teles = telesByZones[zone];
                }
            }


            for (int i = 0; i < numOfSpawns; i++)
            {
                Spawner s = allSpawners[i];
                s.Update(delta);
            }

            List<Item> itemsToRemove = new List<Item>();
            for (int i = 0; i < itemsOnMap.Count; i++)
            {
                Item item = itemsOnMap[i];
                item.Update(delta);
                if (item.Despawned)
                {
                    itemsToRemove.Add(item);
                }
            }
            for (int i = 0; i < itemsToRemove.Count; i++)
            {
                Item item = itemsToRemove[i];
                itemsOnMap.Remove(item);
            }

            if (effectedByAmbientLight)
            {
                Point time = world.Time;
                int hour = time.X;
                int min = time.Y;
                if (hour >= 19 || hour < 6)
                {
                    float currentAmbient = sys.AmbientLight;
                    if (currentAmbient > .05f)
                    {
                        currentAmbient -= delta * .00001f;
                        sys.AmbientLight = currentAmbient;
                    }
                }
                else if (hour >= 6 || hour < 19)
                {
                    float currentAmbient = sys.AmbientLight;
                    if (currentAmbient < 1.0f)
                    {
                        currentAmbient += delta * .00001f;
                        sys.AmbientLight = currentAmbient;
                    }
                }
            }
            else
            {
                sys.AmbientLight = ambient;
            }
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>>>

        /// <summary>
        /// Returns the XTiled map.
        /// </summary>
        public Map Map
        {
            get
            {
                return map;
            }
        }
        public int[,] Collision
        {
            get
            {
                return collisionMap;
            }
        }
        public Point PlayerEntry
        {
            get
            {
                return playerEntryPoint;
            }
        }
        public GameWorld World
        {
            get
            {
                return world;
            }
        }
        public List<Item> ItemsOnMap
        {
            get
            {
                return itemsOnMap;
            }
        }
        public List<Zone> Zones
        {
            get
            {
                return zones;
            }
        }
        public List<Healer> Healers
        {
            get
            {
                return healers;
            }
        }
        public AlphaTile[,] AlphaTiles
        {
            get
            {
                return sys.AlphaTiles;
            }
        }
        public List<Teleporter> Teleporters
        {
            get
            {
                return teles;
            }
        }
        public TileBasedLightingSystem Lighting
        {
            get
            {
                return sys;
            }
        }
        public List<Spawner> ZoneSpawners
        {
            get
            {
                return spawnersByZones[world.Player.Zone];
            }
        }
        public List<Spawner> Spawners
        {
            get
            {
                return allSpawners;
            }
        }
        public bool Outside
        {
            get
            {
                return effectedByAmbientLight;
            }
            set
            {
                effectedByAmbientLight = value;
            }
        }
        public float AmbientLight
        {
            get
            {
                return ambient;
            }
            set
            {
                ambient = value;
            }
        }

    }
}
