﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Atlas.Game.Environment
{
    public class Zone
    {
        private string zoneName;
        private Rectangle bounds;
        public Zone(Rectangle bounds, string zoneName)
        {
            this.bounds = bounds;
            this.zoneName = zoneName;
        }

        //properties
        public string ZoneName
        {
            get
            {
                return zoneName;
            }
        }
        public Rectangle Bounds
        {
            get
            {
                return bounds;
            }
        }
    }
}
