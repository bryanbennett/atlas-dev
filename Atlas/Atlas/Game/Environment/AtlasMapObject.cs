﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Atlas.Game.Environment
{
    public abstract class AtlasMapObject
    {
        private string zone;
        private Rectangle bounds;


        public AtlasMapObject(string zone, Rectangle bounds)
        {
            this.zone = zone;
            this.bounds = bounds;
        }


        //>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>

        public Rectangle Bounds
        {
            get
            {
                return bounds;
            }
        }
        public string Zone
        {
            get
            {
                return zone;
            }
        }
    }
}
