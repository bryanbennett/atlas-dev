﻿using System;
using System.Collections.Generic;
using FuncWorks.XNA.XTiled;
using System.Collections;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Atlas.Game.Manager;
using Atlas.Game.Entity;
using VintageStudios.Planar2D;
using VintageStudios.Planar2D.Visuals;
using VintageStudios.Planar2D.Shielder;
using VintageStudios.Planar2D.Visuals.Lighting;
using Atlas.Game.Items;
using Microsoft.Xna.Framework.Audio;
using Atlas.Game.Data;
using Atlas.Game.Exceptions;

namespace Atlas.Game.Environment
{
    /// <summary>
    /// Class handles and manages the updates, draws, and collision of all objects on the map including monsters.
    /// </summary>
    public class GameWorld : GameComponent
    {
        private Dictionary<String, PlanarMap> maps;
        private ResourceManager rm;
        private Player player;
        private AtlasClient atlas;
        private GraphicsDevice device;
        private PlanarMap currentMap;
        private Rectangle camView;
        private SoundEffect teleSound;
        private SoundEffectInstance teleSoundInstance;

        //time
        private int minute = 0;
        private int hour = 8;
        private int timeCount = 0;

        public GraphicsDevice Device
        {
            get
            {
                return device;
            }
        }
        /// <summary>
        /// Returns the time in a Point object.  X is the hour, Y is the minute.
        /// </summary>
        public Point Time
        {
            get
            {
                return new Point(hour, minute);
            }
        }
        /// <summary>
        /// Returns the active map.
        /// </summary>
        public PlanarMap CurrentMap
        {
            get
            {
                return currentMap;
            }
        }
        /// <summary>
        /// Returns the start location for new players.
        /// </summary>
        public Point PlayerStart
        {
            get
            {
                return currentMap.PlayerEntry;
            }
        }
        /// <summary>
        /// Returns the resource manager this GameWorld was generated from.
        /// </summary>
        public ResourceManager Manager
        {
            get
            {
                return rm;
            }
        }
        /// <summary>
        /// Gets or sets the Player associated with this GameWorld.
        /// </summary>
        public Player Player
        {
            get
            {
                return player;
            }
            set
            {
                player = value;
            }
        }
        /// <summary>
        /// Returns the bounds of the current camera view.
        /// </summary>
        public Rectangle CameraView
        {
            get
            {
                return camView;
            }
        }
        /// <summary>
        /// Returns the collision map associated with this map.
        /// </summary>
        public int[,] CollisionMap
        {
            get
            {
                return currentMap.Collision;
            }
        }
        /// <summary>
        /// Returns the tiles that are rendered for the lighting.
        /// </summary>
        public AlphaTile[,] AlphaTiles
        {
            get
            {
                return currentMap.AlphaTiles;
            }
        }
        /// <summary>
        /// Returns the Game object.
        /// </summary>
        public AtlasClient AtlasClient
        {
            get
            {
                return atlas;
            }
        }
        public GameWorld(ResourceManager rm, AtlasClient game)
            :base(game)
        {
            this.rm = rm;
            device = game.GraphicsDevice;
            this.atlas = game;
            teleSound = game.Content.Load<SoundEffect>("Sound\\sfx\\sound6");
            teleSoundInstance = teleSound.CreateInstance();
            teleSoundInstance.Volume = Settings.SFX_VOLUME;
            GenerateMaps();
        }
        private void GenerateMaps()
        {
            maps = new Dictionary<string, PlanarMap>();

            foreach (KeyValuePair<string, Map> entry in rm.Maps)
            {
                Map map = entry.Value;
                PlanarMap newMap = new PlanarMap(this, map);
                string errorString = "Undefined error.";
                try
                {
                    Property prop = map.Properties["outside"];
                    bool ignoreMe;
                    bool parseSuccessful = Boolean.TryParse(prop.Value, out ignoreMe);
                    if (parseSuccessful)
                    {
                        newMap.Outside = Boolean.Parse(prop.Value);
                        if (!newMap.Outside)
                        {
                            prop = map.Properties["ambient"];
                            float ignoreMe2;
                            parseSuccessful = float.TryParse(prop.Value, out ignoreMe2);
                            if (parseSuccessful)
                            {
                                newMap.AmbientLight = float.Parse(prop.Value);
                            }
                            else
                            {
                                errorString = "Could not parse or find the 'ambient' property.  If outside property is set to false, an ambient property must be defined (Map: " + entry.Key + ").";
                                throw new Exception();
                            }
                        }
                    }
                    else
                    {
                        errorString = "Could not parse or find the 'outside' property.  Ensure the map property is properly defined for " + entry.Key + ".";
                        throw new Exception();
                    }
                }
                catch (Exception ex)
                {
                    throw new PropertyNotDefinedException(errorString);
                }
                
                maps.Add(entry.Key, newMap);
            }
            currentMap = GetMap("main_map");
        }
        /// <summary>
        /// Sets the current map.
        /// </summary>
        /// <param name="mapName"></param>
        public void SetCurrentMap(String mapName)
        {
            //currentMap = (PlanarMap)maps[mapName];
        }
        /// <summary>
        /// Returns a TMX map based on the provided map name.
        /// </summary>
        /// <param name="mapName"></param>
        /// <returns></returns>
        public PlanarMap GetMap(String mapName)
        {
			Console.WriteLine(mapName);
            return maps[mapName];
        }
        /// <summary>
        /// Draws the background layers of the game world.
        /// </summary>
        /// <param name="batch"></param>
        /// <param name="view"></param>
        public void DrawBG(SpriteBatch batch, Rectangle view)
        {
            camView = view;
            currentMap.DrawBG(batch, view);
        }
        /// <summary>
        /// Draws the foreground layers of the game world.
        /// </summary>
        /// <param name="batch"></param>
        /// <param name="view"></param>
        public void DrawFG(SpriteBatch batch, Rectangle view)
        {
            camView = view;
            currentMap.DrawFG(batch, view);
        }
        /// <summary>
        /// Updates all map objects such as mobs and spawners.
        /// </summary>
        /// <param name="time"></param>
        public override void Update(GameTime time)
        {
            int delta = time.ElapsedGameTime.Milliseconds;
            if (!atlas.Loading)
            {
                //update the time
                timeCount += delta;
                if (timeCount >= 1000)
                {
                    timeCount = 0;
                    minute++;
                    if (minute >= 60)
                    {
                        minute = 0;
                        hour++;

                        if (hour >= 24)
                        {
                            hour = 0;
                        }
                    }
                }

                //update the maps
                foreach (KeyValuePair<string, PlanarMap> entry in maps)
                {
                    entry.Value.Update(delta);
                }

                //healers
                foreach (Healer healer in currentMap.Healers)
                {
                    Rectangle offRect = new Rectangle(healer.Bounds.X - camView.X, healer.Bounds.Y - camView.Y, healer.Bounds.Width, healer.Bounds.Height);
                    if ((offRect.Contains(Player.HitBox) || offRect.Intersects(Player.HitBox)) && Player.HookedMobCount == 0)
                    {
                        Player.Info.Health += (decimal).5;
                    }
                }

                //zones
                foreach (Zone zone in currentMap.Zones)
                {
                    Rectangle offRect = new Rectangle(zone.Bounds.X - camView.X, zone.Bounds.Y - camView.Y, zone.Bounds.Width, zone.Bounds.Height);
                    bool zoneFound = false;
                    string nameOfZone = "";
                    if (!zone.ZoneName.Equals(Player.Zone) && offRect.Contains(Player.HitBox))
                    {
                        nameOfZone = zone.ZoneName;
                        zoneFound = true;
                    }
                    if (zoneFound)
                    {
                        Player.ShowZone = true;
                        Player.Zone = nameOfZone;
                    }
                }

                //teleporters
                foreach (Teleporter tele in currentMap.Teleporters)
                {
                    Rectangle offRect = new Rectangle(tele.Bounds.X - camView.X, tele.Bounds.Y - camView.Y, tele.Bounds.Width, tele.Bounds.Height);
                    if (offRect.Intersects(Player.HitBox) || offRect.Contains(Player.HitBox))
                    {
                        PlanarMap newMap = maps[tele.TargetMap];
                        Point tile = tele.TargetTile;
                        Player.WorldPosition = new Vector2(tile.X, tile.Y);
                        Player.Teleported = true;
                        currentMap = newMap;
                        teleSoundInstance.Play();
                        break;
                    }
                }

          }
            
       }
        /// <summary>
        /// Determines if an agent has collided with a map object.  
        /// The algorithm is based on the sprite's width and height, determining 
        /// how many elements in the collision array that needs to be checked.
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public bool hasCollided(Agent a, int collisionFlag, PlanarMap map)
        {
            int spriteWidth = a.SpriteWidth / 16;
            int spriteHeight = a.SpriteHeight / 16;
            int x = a.TileX;
            int y = a.TileY;
            Facing face = a.Facing;
            int[,] collision = currentMap.Collision;
            int width = currentMap.Map.Width;
            int height = currentMap.Map.Height;
            int UIBorderOffset = 2;
            int UIMainBarOffset = 8;

            switch (face)
            {
                case Facing.North:
                    for (int i = 0; i < spriteWidth; i++)
                    {
                        if (x + i > width - UIBorderOffset || y - UIBorderOffset < 0)
                        {
                            return true;
                        }
                        if (collision[x + i, y - 1] >= collisionFlag)
                        {
                            return true;
                        }
                    }
					//if (y - 1 < 22)
					//{
					//	return true;
					//}
                    break;
                case Facing.East:
                    for (int i = 0; i < spriteHeight; i++)
                    {
                        if (x + spriteWidth > width - UIBorderOffset || y + i > height - UIMainBarOffset)
                        {
                            return true;
                        }
                        if (collision[x + spriteWidth, y + i] >= collisionFlag)
                        {
                            return true;
                        }
                    }
					//if (x + spriteWidth > 1038)
					//{
					//	return true;
					//}
                    break;
                case Facing.South:
                    for (int i = 0; i < spriteWidth; i++)
                    {
                        if (x + i > width - UIBorderOffset || y + spriteHeight > height - UIMainBarOffset)
                        {
                            return true;
                        }
                        if (collision[x + i, y + spriteHeight] >= collisionFlag)
                        {
                            return true;
                        }
                    }
					//if (y + spriteHeight > 1021)
					//{
					//	return true;
					//}
                    break;
                case Facing.West:
                    for (int i = 0; i < spriteHeight; i++)
                    {
                        if (x - UIBorderOffset < 0 || y + i > height - UIMainBarOffset)
                        {
                            return true;
                        }
                        if (collision[x - 1, y + i] >= collisionFlag)
                        {
                            return true;
                        }
                    }
					//if (x - 1 < 39)
					//{
					//	return true;
					//}
                    break;
            }
            return false;
        }
        /// <summary>
        /// Sets the collision value in the collision map array.
        /// </summary>
        /// <param name="tileX"></param>
        /// <param name="tileY"></param>
        /// <param name="value"></param>
        public void SetCollisionByValue(int tileX, int tileY, int value)
        {
            currentMap.Collision[tileX, tileY] = value;
        }
        /// <summary>
        /// Changes the ambient light.
        /// </summary>
        /// <param name="value"></param>
        public void ChangeAmbient(float value)
        {
            currentMap.Lighting.ChangeAmbient(value);
        }

        public void GenerateItem(Point tile, int level)
        {
            Dictionary<string, ItemInfo> items = rm.Items;
            List<ItemInfo> subSet = new List<ItemInfo>();

            foreach (KeyValuePair<string, ItemInfo> item in items)
            {
                if (item.Value.Level <= level)
                {
                    subSet.Add(item.Value);
                }
            }

            Random r = new Random();
            int count = subSet.Count;

            int index = r.Next(count);

            Item selectedItem = new Item(new Rectangle(0, 0, 32, 32), device, subSet[index]);
            selectedItem.Tile = tile;
            currentMap.ItemsOnMap.Add(selectedItem);
        }
        public void GenerateItem(Point tile, string item)
        {
            Dictionary<string, ItemInfo> items = rm.Items;
            Item selectedItem = new Item(new Rectangle(0, 0, 32, 32), device, items[item]);
            selectedItem.Tile = tile;
            currentMap.ItemsOnMap.Add(selectedItem);
        }
        public void GetItem()
        {
            Rectangle rect = player.HitBox;
            int count = currentMap.ItemsOnMap.Count;

            for (int i = 0; i < count; i++)
            {
                Item item = currentMap.ItemsOnMap[i];
                Point offset = item.OffsetPosition;
                int x = offset.X;
                int y = offset.Y;
                if (rect.Contains(x, y))
                {
                    bool itemAdded = player.AddItem(item);
                    if (itemAdded)
                    {
                        currentMap.ItemsOnMap.RemoveAt(i);
                    }
                    break;
                }
            }
        }
        public void AddItem(Item item)
        {
            item.Tile = player.TilePosition;
            currentMap.ItemsOnMap.Add(item);
        }
    }
}
