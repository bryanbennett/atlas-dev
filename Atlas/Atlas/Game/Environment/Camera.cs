﻿using Atlas.Game.Entity;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections;
using VintageStudios.Planar2D;
using System;
using VintageStudios.Planar2D.Visuals.Lighting;
using System.Collections.Generic;

namespace Atlas.Game.Environment
{
    public class Camera
    {
        private GameWorld world;
        private Rectangle view;
        private Player p;
        private Vector2 target;
        private Vector2 offset;
        private bool isMoving;
		private bool validView;
        private Facing face;
        private bool backgroundRendered = false;
        private float saveOffsetX;
        private float saveOffsetY;

        /// <summary>
        /// Returns or sets the view in which this camera encompasses.
        /// </summary>
        public Rectangle View
        {
			get { return view; }
			set { view = value; }
        }

        /// <summary>
        /// Returns the game world instance of this Camera.
        /// </summary>
        public GameWorld World
        {
			get { return world; }
        }

        /// <summary>
        /// Returns whether or not this camera is in the process of moving.
        /// </summary>
        public bool CameraMoving
        {
			get { return isMoving; }
        }


        //takes a player object to determine the starting view
        public Camera(Player p, GameWorld world)
        {
            this.p = p;
            this.world = world;
            view = new Rectangle((p.TileX - 40)* 16 + 16, (p.TileY - 23) * 16 + 24, 1280, 720);
            offset = new Vector2(0, 0);
        }


        /// <summary>
        /// Draws the camera data.  A transform matrix is provided if the camera should be moving.
        /// Because the background must be drawn before the foreground, two Draw calls are necessary.  The first call
        /// draws the background, the second call draws the foreground.  Both renderings use the same offset
        /// provided by the transform matrix.
        /// </summary>
        /// <param name="batch"></param>
        public void Draw(SpriteBatch batch)
        {
			if (!backgroundRendered)
			{
                Matrix Transform = Matrix.CreateTranslation(offset.X, offset.Y, 0);
                saveOffsetX = offset.X;
                saveOffsetY = offset.Y;

                batch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null, Transform);
                world.DrawBG(batch, view);
                batch.End();
                backgroundRendered = true;
			}
			else
			{
				Matrix Transform = Matrix.CreateTranslation(saveOffsetX, saveOffsetY, 0);

				batch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null, Transform);
				world.DrawFG(batch, view);
				batch.End();
				backgroundRendered = false;
			}
        }

        /// <summary>
        /// Updates the camera's movement if necessary.
        /// </summary>
        /// <param name="delta"></param>
        public void Update(int delta)
        {
			view = new Rectangle((int)((p.WorldPosition.X - 40) * 16 + 16), (int)((p.WorldPosition.Y - 23) * 16 + 24), 1280, 720);

			if (view.X < 0)
				view.X = 0;
			else if (view.X > World.CurrentMap.Map.Width * 16 - view.Width)
				view.X = world.CurrentMap.Map.Width * 16 - view.Width;

			if (view.Y < 0)
				view.Y = 0;
			else if (view.Y > World.CurrentMap.Map.Height * 16 - view.Height - 1)		// BUG: for some reason, drawing the very bottom of the map throws out of bounds error
				view.Y = world.CurrentMap.Map.Height * 16 - view.Height - 1;

			//Console.WriteLine("view.Y={0}, view.Y bottom={1}", view.Y, view.Bottom);
			//Console.WriteLine("height={0}, width={1}", world.CurrentMap.Map.Height*16, world.CurrentMap.Map.Width*16);
        }
    }
}
