﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Atlas.Game.Environment
{
    public class Teleporter : AtlasMapObject
    {
        private Point targetTile;
        private string targetMap;


        public Teleporter(string targetMap, Point targetTile, Rectangle bounds, string zone)
            :base(zone, bounds)
        {
            this.targetTile = targetTile;
            this.targetMap = targetMap;
        }


        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        public Point TargetTile
        {
            get
            {
                return targetTile;
            }
        }
        public string TargetMap
        {
            get
            {
                return targetMap;
            }
        }
    }
}
