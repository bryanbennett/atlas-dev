﻿using Atlas.Game.Entity;
using Microsoft.Xna.Framework;
using Atlas.Game.Manager;
using VintageStudios.Planar2D.Shielder;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Atlas.Game.Environment
{
    /// <summary>
    /// Class primarily used as the waypoint between the monster itself and the game world.
    /// It handles the respawning of monsters and also provides an anchor point to determine
    /// how far the monster has travelled away from its intended area.
    /// </summary>
    public class Spawner : AtlasMapObject
    {
        private MonsterInfo info;
        private Texture2D healthbar;
        private GameWorld world;
        private PlanarMap map;
        private Point worldPosition;
        private Mob mob;
        private int respawnTime = 5000;
        private int count;
        private bool hasMob;
        private ResourceManager rm;
        private AtlasClient atlas;
        private SoundEffect killedSound;
        private SoundEffectInstance killInstance;
        private SoundEffect hitSound;
        private SoundEffectInstance hitInstance;

        //properties

        /// <summary>
        /// Returns the GameWorld this spawner is attached to.
        /// </summary>
        public GameWorld World
        {
            get
            {
                return world;
            }
            set
            {
                world = value;
            }
        }
        /// <summary>
        /// Returns the monster associated with this spawner.
        /// </summary>
        public Mob Mob
        {
            get
            {
                return mob;
            }
        }
        public bool HasMob
        {
            get
            {
                return hasMob;
            }
        }
        public Point WorldPosition
        {
            get
            {
                return worldPosition;
            }
        }
        public PlanarMap Map
        {
            get
            {
                return map;
            }
        }

        public Spawner(MonsterInfo info, PlanarMap map, Point worldPosition, Rectangle bounds, string zone)
            :base(zone, bounds)
        {
            rm = map.World.Manager;
            this.World = map.World;
            this.info = info;
            this.map = map;
            this.worldPosition = worldPosition;
            count = respawnTime;

            atlas = map.World.AtlasClient;
            healthbar = new Texture2D(atlas.GraphicsDevice, 1, 1);
            healthbar.SetData(new Color[] { Color.LightGreen });
            killedSound = atlas.Content.Load<SoundEffect>("Sound\\sfx\\sound9");
            killInstance = killedSound.CreateInstance();
            killInstance.Volume = .1f;
            hitSound = atlas.Content.Load<SoundEffect>("Sound\\sfx\\sound2");
            hitInstance = hitSound.CreateInstance();
            hitInstance.Volume = .1f;

        }
        public void DrawMob(SpriteBatch batch, float offX, float offY)
        {
            if (hasMob)
            {
                if (mob.InView)
                {
                    mob.DrawAt(batch, offX, offY);
                }
            }
        }
        public void Update(int delta)
        {
            if (!hasMob)
            {
                count += delta;
            }
            else
            {
                if (!mob.Despawned)
                {
                   mob.Update(delta);
                }
                else
                {
                    hasMob = false;
                }
            }
            if (count >= respawnTime)
            {
                count = 0;
                mob = new Mob(info, worldPosition,this, rm.GetSprite(info.ImageName), healthbar, killInstance, hitInstance);
                hasMob = true;
            }
        }
    }
}
