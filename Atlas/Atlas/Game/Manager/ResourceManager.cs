﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using FuncWorks.XNA.XTiled;
using VintageStudios.Planar2D.Shielder;
using Atlas.Game.Exceptions;
using VintageStudios.Planar2D.GameData;
using VintageStudios.Planar2D.Visuals;

namespace Atlas.Game.Manager
{
    public class ResourceManager
    {
        private Dictionary<String, Sprite> sprites;
        private Dictionary<String, Texture2D> sheets;
        private Dictionary<String, Map> maps;
        private ShielderPackage package;
        private ContentManager content;

        /// <summary>
        /// Returns the Dictionary<String, Map> containing all the maps.
        /// </summary>
        public Dictionary<String, Map> Maps
        {
            get
            {
                return maps;
            }
        }

        public Dictionary<string, ItemInfo> Items
        {
            get
            {
                return package.Items;
            }
        }

        public ResourceManager(AtlasClient game)
        {
            this.content = game.Content;

            //LOAD SPRITES
            sheets = ResourceLoader.LoadContent<Texture2D>("Sprites", content);
            sprites = new Dictionary<String, Sprite>();

            foreach (KeyValuePair<String, Texture2D> entry in sheets)
            {
                Sprite sprite = new Sprite(entry.Value);
                sprites.Add(entry.Key, sprite);
            }
            //LOAD MOBS AND ITEMS
            LoadAPX();

            //LOAD MAPS
            maps = ResourceLoader.LoadContent<Map>("Tilesets","*_map.xnb", content);
        }
        private void LoadAPX()
        {
            package = new ShielderPackage("Packages\\main.apx", content);
        }
        /// <summary>
        /// Returns the sprite based on the provided key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Sprite GetSprite(String key)
        {
            try
            {
                return new Sprite(sheets[key]);
            }
            catch 
            {
                throw new SpriteDoesNotExistException("Sprite (" + key + ") not found.");
            }
        }
        /// <summary>
        /// Returns the MonsterInfo based on the key provided.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public MonsterInfo GetMonsterInfo(String key)
        {
            try
            {
                return new MonsterInfo(package.Monsters[key]);
            }
            catch
            {
                throw new MonsterNotFoundException("Monster (" + key + ") not found in Shielder Package.  Ensure spawners only reference already-existing monsters.");
            }
        }
        public ItemInfo GetItemInfo(String key)
        {
            try
            {
                return new ItemInfo(package.Items[key]);
            }
            catch
            {
                throw new ItemNotFoundException("Item (" + key + ") not found in Shielder Package.");
            }
        }
        public QuestInfo GetQuestInfo(string key)
        {
            try
            {
                return new QuestInfo(package.Quests[key]);
            }
            catch
            {
                throw new QuestNotFoundException("Quest (" + key + ") not found in Shielder Package.  Ensure map objects that reference this quest have the correct properties.");
            }
        }



    }
}
