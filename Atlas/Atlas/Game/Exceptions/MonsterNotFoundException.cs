﻿using System;

namespace Atlas.Game.Exceptions
{
    public class MonsterNotFoundException : Exception
    {
        /// <summary>
        /// Exception class used to flag missing monsters from shielder packages if a spawner requests a monster that does not exist in the package.
        /// </summary>
        public MonsterNotFoundException()
        {
        }
        public MonsterNotFoundException(string message)
            :base(message)
        {
        }
        public MonsterNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
