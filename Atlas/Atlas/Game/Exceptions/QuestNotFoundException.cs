﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Game.Exceptions
{
    public class QuestNotFoundException : Exception
    {
        public QuestNotFoundException()
        {
        }
        public QuestNotFoundException(string message)
            :base(message)
        {
        }
        public QuestNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
