﻿using System;

namespace Atlas.Game.Exceptions
{
    /// <summary>
    /// Exception class used to flag when a sprite is requested that does not exist.
    /// </summary>
    public class SpriteDoesNotExistException : Exception
    {
        public SpriteDoesNotExistException()
        {
        }
        public SpriteDoesNotExistException(string message)
            :base(message)
        {
        }
        public SpriteDoesNotExistException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
