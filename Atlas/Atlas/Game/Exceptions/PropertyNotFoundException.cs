﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Game.Exceptions
{
    public class PropertyNotDefinedException : Exception
    {

                /// <summary>
        /// Exception class used to flag missing properties from the Tiled maps or objects contained therein.
        /// </summary>
        public PropertyNotDefinedException()
        {
        }
        public PropertyNotDefinedException(string message)
            :base(message)
        {
        }
        public PropertyNotDefinedException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
