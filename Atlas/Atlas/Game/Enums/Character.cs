﻿
namespace Atlas.Game.Enums
{
    /// <summary>
    /// All the different class types in Atlas.
    /// </summary>
    public enum Character
    {
        Knight,
        Mage,
        Thief,
    }
}
