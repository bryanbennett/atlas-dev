﻿using Microsoft.Xna.Framework.Graphics;
using VintageStudios.Planar2D.GameStructure;

namespace Atlas.Game.States
{
    public class InventoryState : GameState
    {
        public InventoryState(GameStateController gcs)
            :base(gcs, 1)
        {

        }
        public override void Draw(SpriteBatch batch)
        {

        }

        public override void Update(int delta)
        {
            
        }

        public override void KeyInput(int delta)
        {
            
        }
    }
}
