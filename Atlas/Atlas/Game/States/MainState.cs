﻿using Atlas.Game.Environment;
using Microsoft.Xna.Framework.Graphics;
using Atlas.Game.Entity;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using VintageStudios.Planar2D;
using VintageStudios.Planar2D.GameStructure;
using VintageStudios.Planar2D.Visuals.UI;
using Microsoft.Xna.Framework.Audio;
using System.IO;
using System;
using Atlas.Game.Items;
using Atlas.Game.Manager;
using Atlas.Game.Data;

namespace Atlas.Game.States
{
    public class MainState : GameState
    {
        private Camera cam;
        private Player p;
        private Texture2D bottom;
        private Texture2D top;
        private Texture2D side1;
        private Texture2D side2;
        private Texture2D deathImage;
        private PlanarWindow stats;
        private PlanarWindow inventory;
        private PlanarButton btnStats;
        private PlanarButton btnInventory;
        private PlanarButton btnQuests;
        private PlanarButton btnMenu;
        private PlanarLabel label;
        private PlanarPanel pnlButtons;
        private SpriteFont font;
        private ValueBar health;
        private ValueBar mana;
        private ValueBar stam;
        private ValueBar xp;
        private Texture2D zoneWindow;
        private InventoryManager invMan;
        private ResourceManager rm;
        private int toggleTime = 0;
        private bool windowToggled = false;

        //properties
        public MainState(GameStateController gcs, Camera cam, Player p, ResourceManager rm)
            :base(gcs, 0)
        {
            this.rm = rm;
            this.cam = cam;
            this.p = p;
            WindowManager.ExclusiveWindows = true;
            bottom = gcs.Content.Load<Texture2D>("Images\\bottom");
            top = gcs.Content.Load<Texture2D>("Images\\top");
            side1 = gcs.Content.Load<Texture2D>("Images\\side1");
            side2 = gcs.Content.Load<Texture2D>("Images\\side2");
            deathImage = gcs.Content.Load<Texture2D>("Images\\window_3");
            font = gcs.Content.Load<SpriteFont>("Verdana");

            Rectangle rect = new Rectangle(548,618, 174,32);
            health = new ValueBar(rect, p.Info.MaximumHealth, Color.DarkRed, gcs.Game.GraphicsDevice);
            health.Visible = true;
            health.PercentFilled = 100;

            rect = new Rectangle(551, 655, 168, 15);
            mana = new ValueBar(rect, p.Info.MaximumMana, Color.DarkBlue, gcs.Game.GraphicsDevice);
            mana.Visible = true;
            mana.PercentFilled = 100;

            rect = new Rectangle(550, 672, 168, 17);
            stam = new ValueBar(rect, p.Info.MaximumStamina, Color.DarkGreen, gcs.Game.GraphicsDevice);
            stam.Visible = true;
            stam.PercentFilled = 100;

            rect = new Rectangle(500, 690, 270, 15);
            xp = new ValueBar(rect, 1000, Color.DarkGoldenrod, gcs.Game.GraphicsDevice);
            xp.Visible = true;
            xp.PercentFilled = 0;

            pnlButtons = new PlanarPanel(new Rectangle(875, 672, 600, 50), gcs.Game.GraphicsDevice);

            stats = new PlanarWindow(new Rectangle(863, 20, 375, 650), this, 0, gcs.Game.GraphicsDevice);
            inventory = new PlanarWindow(new Rectangle(863, 20, 375, 650), this, 1, gcs.Game.GraphicsDevice);
            inventory.Anchored = true;

            invMan = new InventoryManager(new Rectangle(0, 0, 375, 650), gcs.Game.GraphicsDevice, gcs.Content, inventory, p, cam.World);

            string[] startGear = p.StartGear;

            for (int i = 0; i < startGear.Length; i++)
            {
                Item item = new Item(new Rectangle(50, 50, 32, 32), gcs.Game.GraphicsDevice, rm.GetItemInfo(startGear[i]));
                p.AddItem(item);
            }

            btnStats = new PlanarButton(new Rectangle(0, 0, 85, 25), font, Color.DarkRed, gcs.Game.GraphicsDevice);
            btnStats.Label = "Stats";
            btnStats.Visible = true;
            btnStats.RegisterEvent(ClickEventStats);
            pnlButtons.AddComponent(btnStats);

            btnInventory = new PlanarButton(new Rectangle(90, 0, 85, 25), font, Color.DarkRed, gcs.Game.GraphicsDevice);
            btnInventory.Label = "Inventory";
            btnInventory.Visible = true;
            btnInventory.RegisterEvent(ClickEventInventory);
            pnlButtons.AddComponent(btnInventory);

            btnQuests = new PlanarButton(new Rectangle(180, 0, 85, 25), font, Color.DarkRed, gcs.Game.GraphicsDevice);
            btnQuests.Label = "Quests";
            btnQuests.Visible = true;
            //need event registry
            pnlButtons.AddComponent(btnQuests);

            btnMenu = new PlanarButton(new Rectangle(270, 0, 85, 25), font, Color.DarkRed, gcs.Game.GraphicsDevice);
            btnMenu.Label = "Main Menu";
            btnMenu.Visible = true;
            //need event registry
            pnlButtons.AddComponent(btnMenu);

            label = new PlanarLabel(new Vector2(2,2), font, gcs.Game.GraphicsDevice);
            label.Color = Color.White;
            string text = "Name: " + p.Info.Name + "\n";
            text += "Level: 1" + "\n";
            text += "Maximum Damage: " + p.Info.MaxDamage + "\n";
            text += "Minimum Damage: " + p.Info.MinDamage + "\n";
            text += "Defense: " + p.Info.Defense + "\n";
            label.Text = text;


            stats.AddComponent(label);


            //add window to mouse handler and window manager.
            RegisterWindow(stats);
            RegisterWindow(inventory);
            MouseHandler.AddComponent(pnlButtons);

            zoneWindow = gcs.Content.Load<Texture2D>("Images\\window_1");
        }
        public override void Draw(SpriteBatch batch)
        {
            cam.Draw(batch); //draws background;

            batch.Begin();
            p.DrawAt(batch, cam.View.X, cam.View.Y);
            batch.End();

            cam.Draw(batch);  //draws foreground;

            batch.Begin();
            if (p.ShowZone)
            {
                Rectangle zoneRect = new Rectangle(0, 650, 208, 188);
                Vector2 zoneOrigin = new Vector2(zoneRect.X + (zoneRect.Width / 2), zoneRect.Y + (zoneRect.Height / 2));
                Vector2 stringLength = font.MeasureString(p.Zone);
                Vector2 stringOrigin = stringLength / 2;
                Vector2 stringPos = new Vector2(100, 720 - 40);
                batch.Draw(zoneWindow, zoneRect, Color.White);
                batch.DrawString(font, p.Zone, stringPos, Color.White, 0, stringOrigin, 1.0f, SpriteEffects.None, 0.5f);
            }
            DrawInterface(batch);
            if (p.Dead)
            {
                Rectangle rect = new Rectangle(1280 / 2 - 118, 60, 236, 150);
                batch.Draw(deathImage, rect, Color.White);
                batch.DrawString(font, "You Have Died.", new Vector2(586, 120), Color.White);
            }
            batch.End();
        }
        private void DrawInterface(SpriteBatch batch)
        {
            health.Draw(batch);
            mana.Draw(batch);
            stam.Draw(batch);
            xp.Draw(batch);
            batch.Draw(bottom, new Rectangle(0, 720 - 110, 1280, 110), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
            batch.Draw(top, new Rectangle(0, 0, 1280, 16), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
            batch.Draw(side1, new Rectangle(0, 0, 16, 720), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
            batch.Draw(side2, new Rectangle(1280 - 16, 0, 16, 720), null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);
            pnlButtons.Draw(batch);
        }
        public override void Update(int delta)
        {
            p.Update(delta);
            cam.Update(delta);
            //p.Update(delta);
            health.Value = p.Info.Health;
            health.MaxValue = p.Info.MaximumHealth;
            xp.Value = p.Info.XP;
            xp.MaxValue = p.Info.NextXP;
            this.KeyInputDisabled = p.Dead;

            string text = "Name: " + p.Info.Name + "\n";
            text += "Level: 1" + "\n";
            text += "Maximum Damage: " + p.Info.MaxDamage + "\n";
            text += "Minimum Damage: " + p.Info.MinDamage + "\n";
            text += "Defense: " + p.Info.Defense + "\n";
            label.Text = text;

            if (MouseHandler.Clicked && !MouseHandler.HasClickedComponent)
            {
                MouseState mouseState = Mouse.GetState();
                Rectangle box = p.HitBox;
                int pX = box.X;
                int pY = box.Y;
                int dX = box.X - mouseState.X;
                int dY = box.Y - mouseState.Y;
                if (Math.Abs(dX) >= Math.Abs(dY))
                {
                    if (dX >= 0)
                    {
                        p.Facing = Facing.West;
                    }
                    else
                    {
                        p.Facing = Facing.East;
                    }
                }
                else
                {
                    if (dY >= 0)
                    {
                        p.Facing = Facing.North;
                    }
                    else
                    {
                        p.Facing = Facing.South;
                    }
                }
            }

        }
        public void ClickEventStats(Object sender, EventArgs e)
        {
            WindowManager.ToggleWindow(0);
        }
        public void ClickEventInventory(Object sender, EventArgs e)
        {
            WindowManager.ToggleWindow(1);
        }
        public override void KeyInput(int delta)
        {
            KeyboardState keys = Keyboard.GetState();
            if (windowToggled)
            {
                toggleTime += delta;
                if (toggleTime >= Settings.WINDOW_TOGGLE_DELAY)
                {
                    toggleTime = 0;
                    windowToggled = false;
                }
            }
            if (keys.IsKeyDown(Keys.Escape))
            {
                gcs.Game.Exit();
            }

            int change = 16;

            if (keys.IsKeyDown(Keys.S) && !p.Moving)
            {
                //move down
                p.Facing = Facing.South;
                if (p.canMove())
                {
                    p.Move(Facing.South);
                }
            }
            if (keys.IsKeyDown(Keys.W) && !p.Moving)
            {
                //move up
                p.Facing = Facing.North;
                if (p.canMove())
                {
                    p.Move(Facing.North);
                }
            }
            if (keys.IsKeyDown(Keys.D) && !p.Moving)
            {
                //move right
                p.Facing = Facing.East;
                if (p.canMove())
                {
                    p.Move(Facing.East);
                }
            }
            if (keys.IsKeyDown(Keys.A) && !p.Moving)
            {
                //move left
                p.Facing = Facing.West;
                if (p.canMove())
                {
                    p.Move(Facing.West);
                }
            }
            if (keys.IsKeyDown(Keys.LeftShift))
            {
                p.Speed = 5.3f;
                p.AnimationSpeed = 25;
            }
            else
            {
                p.Speed = .17f;
                p.AnimationSpeed = 50;
            }
            if (keys.IsKeyDown(Keys.Space))
            {
                if (!p.Attacking)
                {
                    p.Attack();
                }
            }
            if (keys.IsKeyDown(Keys.E))
            {
                GameWorld world = cam.World;
                world.GetItem();
            }
            if (keys.IsKeyDown(Keys.Z))
            {
                p.ShowZone = true;
            }
            if (keys.IsKeyDown(Keys.Tab))
            {
                if (!windowToggled)
                {
                    WindowManager.ToggleWindow(1);
                    windowToggled = true;
                }
            }
            if (keys.IsKeyDown(Keys.Down))
            {
                GameWorld world = cam.World;
                world.ChangeAmbient(-Settings.AMBIENT_LIGHTING_DELTA);
            }
            if (keys.IsKeyDown(Keys.Up))
            {
                GameWorld world = cam.World;
                world.ChangeAmbient(Settings.AMBIENT_LIGHTING_DELTA);
            }
        }
    }
}
