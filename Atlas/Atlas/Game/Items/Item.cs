﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VintageStudios.Planar2D.Visuals.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using VintageStudios.Planar2D.Shielder;
using Atlas.Game.Data;

namespace Atlas.Game.Items
{
    public class Item : VisualComponent
    {
        private ItemInfo info;
        private Point tile;
        private bool onMap;
        private int despawnTimer;
        private bool despawned;
        private Point offsetPosition;
        private ItemToolTip tip;

        public Item(Rectangle bounds, GraphicsDevice device, ItemInfo info)
            : base(bounds, device)
        {
            this.info = info;
            GenerateToolTip();
        }
        private void GenerateToolTip()
        {
            tip = new ItemToolTip();
            tip.DisplayImage = info.Sprite;
            string text = info.Name + "\n";
            text += "Level: " + info.Level + "\n";
            switch (info.Type)
            {
                case ItemType.Armor:
                    text += "Chest" + "\n";
                    text += "Defense: " + info.Defense + "\n";
                    if (info.Health > 0)
                    {
                        text += "+Health: " + info.Health + "\n";
                    }
                    break;
                case ItemType.Weapon_1H:
                    text += "Main Hand" + "\n";
                    text += "MaxDmg: " + info.MaxDamage + "\n";
                    text += "MinDmg: " + info.MinDamage + "\n";
                    if (info.AttackSpeed > 0)
                    {
                        text += "+AtkSpd: " + info.AttackSpeed + "\n";
                    }
                    break;
                case ItemType.Weapon_2H:
                    text += "Two-Handed" + "\n";
                    text += "MaxDmg: " + info.MaxDamage + "\n";
                    text += "MinDmg: " + info.MinDamage + "\n";
                    if (info.AttackSpeed > 0)
                    {
                        text += "+AtkSpd: " + info.AttackSpeed + "\n";
                    }
                    break;
                case ItemType.Helm:
                    text += "Helm" + "\n";
                    text += "Defense: " + info.Defense + "\n";
                    if (info.Health > 0)
                    {
                        text += "+Health: " + info.Health + "\n";
                    }
                    break;
                case ItemType.Gloves:
                    text += "Gloves" + "\n";
                    text += "Defense: " + info.Defense + "\n";
                    if (info.Health > 0)
                    {
                        text += "+Health: " + info.Health + "\n";
                    }
                    break;
                case ItemType.Boots:
                    text += "Boots" + "\n";
                    text += "Defense: " + info.Defense + "\n";
                    if (info.Health > 0)
                    {
                        text += "+Health: " + info.Health + "\n";
                    }
                    break;
                case ItemType.Shield:
                    text += "OffHand/Shield" + "\n";
                    text += "Defense: " + info.Defense + "\n";
                    if (info.Health > 0)
                    {
                        text += "+Health: " + info.Health + "\n";
                    }
                    break;
                case ItemType.Necklace:
                    text += "Necklace" + "\n";
                    if (info.Health > 0)
                    {
                        text += "+Health: " + info.Health + "\n";
                    }
                    break;
                case ItemType.Ring:
                    text += "Ring" + "\n";
                    if (info.MinDamage > 0)
                    {
                        text += "+MinDamage: " + info.MinDamage + "\n";
                    }
                    if (info.MaxDamage > 0)
                    {
                        text += "+MaxDamage: " + info.MaxDamage + "\n";
                    }
                    break;
                case ItemType.Belt:
                    text += "Belt" + "\n";
                    text += "Defense: " + info.Defense + "\n";
                    if (info.Health > 0)
                    {
                        text += "+Health: " + info.Health + "\n";
                    }
                    break;
            }
            tip.DisplayText = text;
        }
        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            batch.Draw(info.Sprite, Bounds, Color.White);
        }
        public void Draw(SpriteBatch batch, Color tint)
        {
            batch.Draw(info.Sprite, Bounds, tint);
        }
        public void DrawAt(SpriteBatch batch, int x, int y)
        {
            if (!onMap)
            {
                Rectangle rect = new Rectangle(x, y, Bounds.Width, Bounds.Height);
                batch.Draw(info.Sprite, rect, Color.White);
            }
            else
            {
                int currentX = tile.X * 16;
                int currentY = tile.Y * 16;
                int offsetX = currentX - x;
                int offsetY = currentY - y;
                offsetPosition = new Point(offsetX, offsetY);
                Rectangle rect = new Rectangle(offsetX, offsetY, Bounds.Width, Bounds.Height);
                batch.Draw(info.Sprite, rect, Color.White);
            }
        }
        public void DrawAt(SpriteBatch batch, int x, int y, Color tint)
        {
            Rectangle rect = new Rectangle(x, y, Bounds.Width, Bounds.Height);
            batch.Draw(info.Sprite, rect, tint);
        }
        public override void Update(int delta)
        {
            if (onMap)
            {
                despawnTimer += delta;
                if (despawnTimer >= Settings.MAP_ITEM_DESPAWN_TIME)
                {
                    despawned = true;
                }
            }
        }

        public override void onClick(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            
        }

        public override void onRelease(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            
        }

        public override void onDrag(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            Rectangle rect = new Rectangle(mouseState.X, mouseState.Y, Bounds.Width, Bounds.Height);
            Bounds = rect;
        }

        public override void onHover(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {

        }

        public override void Reset()
        {

        }
        private void DrawToolTip(SpriteBatch batch)
        {

        }

        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>>>

        /// <summary>
        /// Returns the ItemInfo associated with this item.
        /// </summary>
        public ItemInfo Info { get { return info; } }

        public Point Tile
        {
            get
            {
                return tile;
            }
            set
            {
                tile = value;
                OnMap = true;
            }
        }
        public bool OnMap
        {
            get
            {
                return onMap;
            }
            set
            {
                onMap = value;
                if (value)
                {
                    despawnTimer = 0;
                }
            }
        }
        public Point OffsetPosition
        {
            get
            {
                return offsetPosition;
            }
        }
        public bool Despawned
        {
            get
            {
                return despawned;
            }
        }
        public ItemToolTip ToolTip
        {
            get
            {
                return tip;
            }
        }
    }
}
