﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Atlas.Game.Items
{
    public class ItemToolTip
    {
        private string displayText;
        private Texture2D displayImage;

        public ItemToolTip(Texture2D displayImage, string displayText)
        {
            this.displayText = displayText;
            this.displayImage = displayImage;
        }
        public ItemToolTip()
        {

        }

        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>
        public string DisplayText
        {
            get
            {
                return displayText;
            }
            set
            {
                displayText = value;
            }
        }
        public Texture2D DisplayImage
        {
            get
            {
                return displayImage;
            }
            set
            {
                displayImage = value;
            }
        }
    }
}
