﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VintageStudios.Planar2D.Visuals.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Atlas.Game.Entity;
using Microsoft.Xna.Framework.Input;
using Atlas.Game.Environment;
using VintageStudios.Planar2D.Shielder;
using Atlas.Game.Data;

namespace Atlas.Game.Items
{
    public class InventoryManager : VisualComponent
    {
        private List<Item> items;
        private List<ItemBox> boxes;
        private int count;
        private Texture2D boxImage;
        private Texture2D boxHover;
        private Texture2D boxSelect;
        private Texture2D goldCoins;
        private Texture2D helm;
        private Texture2D gloves;
        private Texture2D armor;
        private Texture2D shield;
        private Texture2D weapon;
        private Texture2D boots;
        private Texture2D belt;
        private Texture2D ring;
        private Texture2D necklace;
        private Texture2D toolTip;
        private ItemBox sourceBox;
        private GameWorld world;
        private bool hasDrag;
        private Item draggedItem;
        private int equippedIndex; //starting index for equipped items
        private SpriteFont font;
        private bool showTip;
        private int tipTimer = 0;
        private bool tipTimerPassed;
        private ItemToolTip currentTip;


        public InventoryManager(Rectangle bounds, GraphicsDevice device, ContentManager content, PlanarWindow parent, Player p, GameWorld world)
            : base(bounds, device)
        {
            items = new List<Item>();
            boxes = new List<ItemBox>();
            boxImage = content.Load<Texture2D>("Images//boxNormal");
            boxHover = content.Load<Texture2D>("Images//boxHover");
            boxSelect = content.Load<Texture2D>("Images//boxSelect");
            goldCoins = content.Load<Texture2D>("Images//CoinsGoldLarge");
            helm = content.Load<Texture2D>("Items//CapSteel");
            armor = content.Load<Texture2D>("Items//ArmorChainMailBar");
            weapon = content.Load<Texture2D>("Items//Axe");
            gloves = content.Load<Texture2D>("Images//gloves");
            shield = content.Load<Texture2D>("Items//ShieldCrestedLion");
            ring = content.Load<Texture2D>("Items//RingBronze");
            necklace = content.Load<Texture2D>("Items//NecklaceSilverJewelOrange");

            belt = content.Load<Texture2D>("Items//Belt1");
            boots = content.Load<Texture2D>("Items//BootsLeatherSoft");
            toolTip = content.Load<Texture2D>("Images//tooltip");
            font = content.Load<SpriteFont>("Verdana");
            Visible = true;
            parent.AddComponent(this);
            this.world = world;
            GenerateBoxes();
            p.Inventory = this;
        }
        private void GenerateBoxes()
        {
            Point topLeft = new Point(Bounds.X + 24, Bounds.Y + 260);
            Texture2D[] images = new Texture2D[3];
            images[0] = boxImage;
            images[1] = boxHover;
            images[2] = boxSelect;
            //constructs inventory boxes
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    int offsetX = topLeft.X + (j * 55);
                    int offsetY = topLeft.Y + (i * 55);
                    Rectangle rect = new Rectangle(offsetX, offsetY, 50, 50);
                    ItemBox box = new ItemBox(rect, this.Graphics, images, this);
                    boxes.Add(box);
                }
            }
            equippedIndex = boxes.Count;  //the starting index for equipped items.

            //constructs unique boxes
            ItemBox uniBox;
            Rectangle temp;

            Point anchor = new Point(Bounds.X + Bounds.Width / 2 - 25, Bounds.Y + 25);
            temp = new Rectangle(anchor.X, anchor.Y, 50,50);
            uniBox = new ItemBox(temp, this.Graphics, images, this);
            uniBox.SetUniqueType(ItemType.Helm, helm);
            boxes.Add(uniBox);

            temp = new Rectangle(anchor.X, anchor.Y + 55, 50, 50);
            uniBox = new ItemBox(temp, this.Graphics, images, this);
            uniBox.SetUniqueType(ItemType.Armor, armor);
            boxes.Add(uniBox);

            temp = new Rectangle(anchor.X - 55, anchor.Y + 55, 50, 50);
            uniBox = new ItemBox(temp, this.Graphics, images, this);
            uniBox.SetUniqueType(ItemType.Gloves, gloves);
            boxes.Add(uniBox);

            temp = new Rectangle(anchor.X - 110, anchor.Y + 55, 50, 50);
            uniBox = new ItemBox(temp, this.Graphics, images, this);
            uniBox.SetUniqueType(ItemType.Weapon_1H,weapon);
            boxes.Add(uniBox);

            temp = new Rectangle(anchor.X + 55, anchor.Y + 55, 50, 50);
            uniBox = new ItemBox(temp, this.Graphics, images, this);
            uniBox.SetUniqueType(ItemType.Shield, shield);
            boxes.Add(uniBox);

            temp = new Rectangle(anchor.X, anchor.Y + 110, 50, 50);
            uniBox = new ItemBox(temp, this.Graphics, images, this);
            uniBox.SetUniqueType(ItemType.Belt, belt);
            boxes.Add(uniBox);

            temp = new Rectangle(anchor.X, anchor.Y + 165, 50, 50);
            uniBox = new ItemBox(temp, this.Graphics, images, this);
            uniBox.SetUniqueType(ItemType.Boots, boots);
            boxes.Add(uniBox);

            temp = new Rectangle(anchor.X + 155, anchor.Y, 50, 50);
            uniBox = new ItemBox(temp, this.Graphics, images, this);
            uniBox.SetUniqueType(ItemType.Necklace, necklace);
            boxes.Add(uniBox);

            temp = new Rectangle(anchor.X + 155, anchor.Y + 55, 50, 50);
            uniBox = new ItemBox(temp, this.Graphics, images, this);
            uniBox.SetUniqueType(ItemType.Ring, ring);
            boxes.Add(uniBox);

            temp = new Rectangle(anchor.X + 155, anchor.Y + 110, 50, 50);
            uniBox = new ItemBox(temp, this.Graphics, images, this);
            uniBox.SetUniqueType(ItemType.Ring, ring);
            boxes.Add(uniBox);

        }
        public bool AddItem(Item item)
        {
            foreach (ItemBox box in boxes)
            {
                if (box.HasItem)
                {
                    continue;
                }
                else
                {
                    bool temp = box.AddItem(item);
                    return temp;
                }
            }
            return false;
        }
        public void RemoveItem(Item item)
        {
            items.Remove(item);
            count = items.Count;
        }
        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            int count = boxes.Count;
            for (int i = 0; i < count; i++)
            {
                ItemBox temp = boxes[i];
                temp.Draw(batch);
            }
            Rectangle rect = new Rectangle(Bounds.X + 5, Bounds.Y + 618, 32, 32);
            batch.Draw(goldCoins, rect, Color.White);

            if (hasDrag)
            {
                draggedItem.Draw(batch);
            }

            if (tipTimerPassed && showTip)
            {
                Rectangle temp = new Rectangle(Bounds.X - 260, Bounds.Y + 250, 250, 188);
                batch.Draw(toolTip, temp, Color.White);
                batch.DrawString(font, currentTip.DisplayText, new Vector2(temp.X + 15, temp.Y + 42), Color.White);
                temp = new Rectangle(temp.X + 108, temp.Y + 7, 32, 32);
                batch.Draw(currentTip.DisplayImage, temp, Color.White);
            }
        }

        public override void Update(int delta)
        {
            if (showTip && !tipTimerPassed)
            {
                tipTimer += delta;
                if (tipTimer >= Settings.TOOLTIP_DELAY)
                {
                    tipTimerPassed = true;
                    tipTimer = 0;
                }
            }
        }

        public override void onClick(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (mouseState.LeftButton == ButtonState.Pressed)
            {

            }
            else if (mouseState.RightButton == ButtonState.Pressed)
            {
                int count = boxes.Count;

                for (int i = 0; i < count; i++)
                {
                    ItemBox box = boxes[i];
                    if (i < equippedIndex && box.HasItem && box.Bounds.Contains(mouseState.X, mouseState.Y))
                    {
                        Item item = box.Item;
                        box.RemoveItem();
                        world.AddItem(item);
                    }
                    else if (i >= equippedIndex && box.HasItem && box.Bounds.Contains(mouseState.X, mouseState.Y))
                    {
                        Item item = box.Item;
                        bool itemAdded = AddItem(item);
                        if (itemAdded)
                        {
                            box.RemoveItem();
                        }
                    }
                }
            }
        }

        public override void onRelease(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (hasDrag)
            {
                bool boxFound = false;
                foreach (ItemBox box in boxes)
                {
                    if (box.Bounds.Contains(mouseState.X, mouseState.Y) && !box.Equals(sourceBox))
                    {
                        if (box.HasItem)
                        {
                            Item sourceItem = sourceBox.Item;
                            Item targetItem = box.Item;
                            bool itemAddedToSource = sourceBox.AddItem(targetItem);
                            bool itemAddedToTarget = box.AddItem(sourceItem);

                            if (itemAddedToSource && itemAddedToTarget)
                            {
                                boxFound = true;
                                hasDrag = false;
                                sourceBox.Dragging = false;
                            }
                            else
                            {
                                sourceBox.AddItem(sourceItem);
                                box.AddItem(targetItem);
                                hasDrag = false;
                                sourceBox.Dragging = false;
                                boxFound = false;
                            }
                        }
                        else
                        {
                            bool itemAdded = box.AddItem(draggedItem);
                            if (itemAdded)
                            {
                                sourceBox.RemoveItem();
                                sourceBox.Dragging = false;
                                box.AddItem(draggedItem);
                                boxFound = true;
                                hasDrag = false;
                            }
                        }
                    }
                }
                if (!boxFound)
                {
                    sourceBox.AddItem(draggedItem);
                    hasDrag = false;
                    sourceBox.Dragging = false;
                }
            }
        }

        public override void onDrag(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (!hasDrag)
            {
                foreach (ItemBox entry in boxes)
                {
                    entry.onDrag(mouseState);
                }
            }
            else
            {
                draggedItem.onDrag(mouseState);
            }

        }

        public override void onHover(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            foreach (ItemBox entry in boxes)
            {
                entry.onHover(mouseState);
            }
        }

        public override void Reset()
        {
            foreach (ItemBox box in boxes)
            {
                box.Reset();
            }
            if (hasDrag)
            {
                hasDrag = false;
                sourceBox.AddItem(draggedItem);
                sourceBox.Dragging = false;
            }
        }



        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PROPERTIES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        public List<ItemBox> ItemBoxes
        {
            get
            {
                return boxes;
            }
        }
        public int EquippedIndex
        {
            get
            {
                return equippedIndex;
            }
        }
        public bool HasDragged
        {
            set
            {
                hasDrag = value;
            }
            get
            {
                return hasDrag;
            }
        }
        public ItemBox SourceBox
        {
            set
            {
                draggedItem = value.Item;
                sourceBox = value;
                hasDrag = true;
            }
        }
        public bool ShowTip
        {
            get
            {
                return showTip;
            }
            set
            {
                showTip = value;
                if (!value)
                {
                    tipTimerPassed = false;
                }
            }
        }
        public ItemToolTip CurrentTip
        {
            get
            {
                return currentTip;
            }
            set
            {
                currentTip = value;
                ShowTip = true;
            }
        }
    }
}
