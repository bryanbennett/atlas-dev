﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VintageStudios.Planar2D.Visuals.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using VintageStudios.Planar2D.Shielder;

namespace Atlas.Game.Items
{
    public class ItemBox : PlanarImage
    {
        private Item item;
        private bool hasItem;
        private bool isDragging;
        private InventoryManager im;
        private Point topLeft;
        private bool unique;
        private ItemType type;
        private Texture2D displayImage;
        private bool showTip;

        public bool HasItem
        {
            get
            {
                return hasItem;
            }
        }
        public Item Item
        {
            get
            {
                return item;
            }
            set
            {
                item = value;
            }
        }
        public bool Dragging
        {
            set
            {
                isDragging = value;
            }
            get
            {
                return isDragging;
            }
        }
        /// <summary>
        /// Gets or sets the type of item this box allows to be stored in it.
        /// </summary>
        public ItemType UniqueType
        {
            set
            {
                type = value;
                unique = true;
            }
            get
            {
                return type;
            }
        }
        public bool Unique
        {
            get
            {
                return unique;
            }
        }

        public ItemBox(Rectangle bounds, GraphicsDevice device, Texture2D[] images, InventoryManager im)
            : base(bounds, device, images)
        {
            this.im = im;
            topLeft = new Point(Bounds.X + 9, Bounds.Y + 9);
        }
        public void RemoveItem()
        {
            item = null;
            hasItem = false;
            if (showTip)
            {
                showTip = false;
                im.ShowTip = false;
            }
        }
        public void SetUniqueType(ItemType type, Texture2D displayImage)
        {
            UniqueType = type;
            this.displayImage = displayImage;
        }

        /// <summary>
        /// Adds the item to the ItemBox.  Returns true if it is successful, false if it failed.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool AddItem(Item item)
        {
            if (!unique)
            {
                hasItem = true;
                this.item = item;
                Rectangle rect = new Rectangle(topLeft.X, topLeft.Y, item.Bounds.Width, item.Bounds.Height);
                item.Bounds = rect;
                return true;
            }
            else
            {
                if (item.Info.Type == type)
                {
                    hasItem = true;
                    this.item = item;
                    Rectangle rect = new Rectangle(topLeft.X, topLeft.Y, item.Bounds.Width, item.Bounds.Height);
                    item.Bounds = rect;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            batch.Draw(CurrentImage, Bounds, Color.White);
            if (hasItem)
            {
                if (isDragging)
                {
                    item.DrawAt(batch, topLeft.X, topLeft.Y, Color.Black);
                }
                else
                {
                    item.Draw(batch);
                }
            }
            else
            {
                if (unique)
                {
                    batch.Draw(displayImage, new Rectangle(topLeft.X, topLeft.Y, 32, 32), Color.Black);
                }
            }
        }
        public override void onHover(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (Bounds.Contains(mouseState.X, mouseState.Y))
            {
                CurrentImage = HoverImage;
                if (hasItem && !showTip)
                {
                    im.CurrentTip = item.ToolTip;
                    showTip = true;
                }
            }
            else
            {
                this.Reset();
                if (hasItem && showTip)
                {
                    showTip = false;
                    im.ShowTip = false;
                }
            }
        }
        public override void onDrag(Microsoft.Xna.Framework.Input.MouseState mouseState)
        {
            if (hasItem && Bounds.Contains(mouseState.X, mouseState.Y))
            {
                im.SourceBox = this;
                isDragging = true;
                if (showTip)
                {
                    showTip = false;
                    im.ShowTip = false;
                }
            }
            else
            {
                //do nothing
            }
        }

    }
}
