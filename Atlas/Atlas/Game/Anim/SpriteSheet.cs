﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Atlas.Game.Interface;
using Atlas.Game.Enums;

namespace Atlas.Game.Anim
{
    public class SpriteSheet
    {
        private Point bounds;
        private Point frameSize;
        private Point currentFrame;
        private Point sheetFrames = new Point(3, 4);
        private int timeSinceLastFrame = 0;
        private int animSpeed = 50;
        private Texture2D sprite;
        private Facing face = Facing.South;
        private bool continuousAnimation = false;
        private bool isMoving = false;

        //Properties
        public bool ContinuousAnimation
        {
            set
            {
                continuousAnimation = value;
            }
        }
        public int AnimationSpeed
        {
            set
            {
                animSpeed = value;
            }
        }
        public Facing Facing
        {
            set
            {
                face = value;
                switch (face)
                {
                    case Facing.South:
                        currentFrame.Y = 0;
                        break;
                    case Facing.West:
                        currentFrame.Y = 1;
                        break;
                    case Facing.East:
                        currentFrame.Y = 2;
                        break;
                    case Facing.North:
                        currentFrame.Y = 3;
                        break;
                }
            }
        }
        public bool Moving
        {
            set
            {
                isMoving = value;
            }
        }
        public Rectangle FrameSize
        {
            get
            {
                return new Rectangle(0, 0, frameSize.X, frameSize.Y);
            }
        }

        public SpriteSheet(Texture2D sprite)
        {
            this.sprite = sprite;
            bounds = new Point(sprite.Width, sprite.Height);
            frameSize = new Point(sprite.Width / 3, sprite.Height / 4);
            currentFrame = new Point(0, 0);
        }
        public void DrawAt(SpriteBatch batch, int x, int y)
        {
            Rectangle r = new Rectangle(currentFrame.X * frameSize.X, currentFrame.Y * frameSize.Y, frameSize.X, frameSize.Y);
            Vector2 renderPosition = new Vector2(x, y);
            batch.Draw(sprite, renderPosition, r, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
        }

        public void Update(int delta)
        {
            if (isMoving)
            {
                timeSinceLastFrame += delta;
                if (timeSinceLastFrame >= animSpeed)
                {
                    timeSinceLastFrame -= animSpeed;
                    ++currentFrame.X;
                    if (currentFrame.X >= sheetFrames.X)
                    {
                        currentFrame.X = 0;
                    }
                }
            }
            else if (!isMoving && continuousAnimation)
            {
                timeSinceLastFrame += delta;
                if (timeSinceLastFrame >= animSpeed)
                {
                    timeSinceLastFrame -= animSpeed;
                    ++currentFrame.X;
                    if (currentFrame.X >= sheetFrames.X)
                    {
                        currentFrame.X = 0;
                    }
                }
            }

        }
    }
}
