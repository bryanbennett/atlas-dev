using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Atlas.Game.Environment;
using Atlas.Game.Entity;
using Atlas.Game.Manager;
using Atlas.Game.States;
using VintageStudios.Planar2D.GameStructure;
using System.Threading;
using VintageStudios.Planar2D.Visuals.UI;
using VintageStudios.Planar2D.Input;
using Atlas.Game.Enums;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Atlas.Game.Data;

namespace Atlas
{
    public class AtlasClient : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        GameStateController stateController;
        ResourceManager rm;
        SpriteBatch spriteBatch;
        Texture2D bar;
        SpriteFont font;
        Vector2 fontPos;
        Vector2 origin;
        Vector2 meas;
        Point rectPos;
        Character type;
        ValueBar valBar;
        SoundEffect mainsong;
        SoundEffectInstance songInstance;
        string[] startGear;
        bool songstart = false;
        string output;
        string outputTarget;
        int outputTime;
        int percentDone = 0;
        bool isLoading = true;
        string[] args;
        string spriteName;

        public bool Loading
        {
            get
            {
                return isLoading;
            }
        }
        public AtlasClient(String[] args)
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.IsFullScreen = false;
            graphics.PreferMultiSampling = false;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            Content.RootDirectory = "Content";
            this.args = args;
            this.IsMouseVisible = true;
            this.IsFixedTimeStep = false;  //why does setting this to false reduce FPS stutter?

            if (args.Length <= 0)
            {
                ///this is debug
                spriteName = "knight";
                type = Character.Knight;
                startGear = new string[] {Balance.KNIGHT_START_WEP, Balance.KNIGHT_START_CHEST};
            }
            else
            {
                spriteName = args[0];
                if (spriteName.Equals("knight"))
                {
                    type = Character.Knight;
                    startGear = new string[] { Balance.KNIGHT_START_WEP, Balance.KNIGHT_START_CHEST };
                }
                else if (spriteName.Equals("mage"))
                {
                    type = Character.Mage;
                    startGear = new string[] { Balance.MAGE_START_WEP, Balance.MAGE_START_CHEST };
                }
                else if (spriteName.Equals("thief"))
                {
                    type = Character.Thief;
                    startGear = new string[] { Balance.THIEF_START_WEP, Balance.THIEF_START_CHEST };
                }
            }
        }
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            //much of this method is simply setting up the loading screen.
            output = "Loading";
            font = Content.Load<SpriteFont>("Verdana");
            fontPos = new Vector2(graphics.GraphicsDevice.Viewport.Width / 2, graphics.GraphicsDevice.Viewport.Height / 2);
            
            origin = font.MeasureString(output) / 2;
            meas = font.MeasureString(output);
            int x = (int) (fontPos.X- 200);
            int y = (int)(fontPos.Y + meas.Y - 5);
            rectPos = new Point(x, y);
            bar = new Texture2D(this.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            valBar = new ValueBar(new Rectangle(rectPos.X, rectPos.Y, 400, 5), Color.White, graphics.GraphicsDevice);
            valBar.Visible = true;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mainsong = Content.Load<SoundEffect>("Sound\\music\\mus2");
            songInstance = mainsong.CreateInstance();
            songInstance.Volume = .1f;
            songInstance.IsLooped=true;
            ThreadPool.QueueUserWorkItem(o => LoadingResources());
        }
        private void LoadingResources()
        {
            outputTarget = "Resource Manager";
            rm = new ResourceManager(this);
            percentDone += 20;
            Thread.Sleep(200);

            outputTarget = "Game World";
            GameWorld world = new GameWorld(rm, this);
            percentDone += 30;
            this.Components.Add(world);
            Thread.Sleep(300);

            outputTarget = "Player";
            Player p = new Player(rm.GetSprite(spriteName), world.PlayerStart, world, type, "Herobrine", Content);
            world.Player = p;
            p.StartGear = startGear;
            percentDone += 10;
            Thread.Sleep(100);

            outputTarget = "Camera";
            Camera cam = new Camera(p, world);
			p.Camera = cam;
            percentDone += 10;
            Thread.Sleep(100);

            //gamestates//
            outputTarget = "Game States";
            stateController = new GameStateController(this);
            Components.Add(stateController);
            percentDone += 5;
            Thread.Sleep(50);

            MainState main = new MainState(stateController, cam, p, rm);
            stateController.AddGameState(main, true, true);
            percentDone += 20;
            Thread.Sleep(200);

            InventoryState inv = new InventoryState(stateController);
            stateController.AddGameState(inv, false, false);
            isLoading = false;
            percentDone += 5;
            Thread.Sleep(50);
            stateController.Loading = false;
        }
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            if (isLoading)
            {
                spriteBatch.Begin();
                spriteBatch.DrawString(font, output, fontPos, Color.DarkRed, 0, origin, 1.0f, SpriteEffects.None, 0.5f);
                spriteBatch.DrawString(font, outputTarget, new Vector2(graphics.GraphicsDevice.Viewport.Width / 2 + 75, graphics.GraphicsDevice.Viewport.Height / 2), Color.DarkRed, 0, origin, 1.0f, SpriteEffects.None, 0.5f);
                valBar.Draw(spriteBatch);
                spriteBatch.End();
            }
            else
            {
                stateController.Draw(spriteBatch);
            }

            base.Draw(gameTime);
        }
        protected override void Update(GameTime gameTime)
        {
            if (isLoading)
            {
                valBar.PercentFilled = percentDone;
                outputTime += gameTime.ElapsedGameTime.Milliseconds;
                if (outputTime < 500)
                {
                    output = "Loading..";
                }
                else if (outputTime >= 500 && outputTime < 1000)
                {
                    output = "Loading......";
                }
                else if (outputTime >= 1000 && outputTime < 1500)
                {
                    output = "Loading...................";
                }
                else
                {
                    outputTime = 0;
                }
            }
            else
            {
                if (!songstart)
                {
                    songInstance.Play();
                    songstart = true;
                }
                KeyboardState keys = Keyboard.GetState();
                if (keys.IsKeyDown(Keys.I))
                {
                    
                }
                if (keys.IsKeyDown(Keys.F1))
                {
                    
                }
            }

            base.Update(gameTime);
        }
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }
    }
}
