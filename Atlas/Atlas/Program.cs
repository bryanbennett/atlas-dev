using System;
using System.Diagnostics;
using System.Windows.Forms;
using Atlas.Game.Data;

namespace Atlas
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (Settings.RUN_LAUNCHER)
            {
                if (args.Length <= 0)
                {
                    Launcher launch = new Launcher();
                    launch.ShowDialog();
                }
                else
                {
                    using (AtlasClient game = new AtlasClient(args))
                    {
                        game.Run();
                    }
                }
            }
            else
            {
                using (AtlasClient game = new AtlasClient(args))
                {
                    game.Run();
                }
            }
        }
    }
#endif
}

