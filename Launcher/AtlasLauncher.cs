﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Launcher
{
    public partial class AtlasLauncher : Form
    {
        public AtlasLauncher()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            exit();
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            NewGame ng = new NewGame(this);
            this.Visible = false;
            ng.Show();
        }

        private void btnLoadGame_Click(object sender, EventArgs e)
        {

        }

        private void AtlasLauncher_Load(object sender, EventArgs e)
        {
            Random r = new Random();
            int num = r.Next(0,2);
            if (num == 0)
            {
                pbxHelm.BackgroundImage = Launcher.Properties.Resources.helmIcon2Dark;
            }
            else
            {
                pbxHelm.BackgroundImage = Launcher.Properties.Resources.helmicon;
            }
        }
        public void exit()
        {
            this.Close();
        }
    }
}
