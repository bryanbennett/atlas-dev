﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Launcher
{
    public partial class NewGame : Form
    {
        private AtlasLauncher parent;
        public NewGame(AtlasLauncher parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            string select = "";
            if (radKnight.Checked)
            {
                select = "knight_ss";
            }
            if (radMage.Checked)
            {
                select = "mage_ss";
            }
            if (radThief.Checked)
            {
                select = "thief_ss";
            }

            //path from launcher
            string myPath = System.Reflection.Assembly.GetEntryAssembly().Location;
            Console.WriteLine(myPath);
            string myDir = System.IO.Path.GetDirectoryName(myPath);
            Console.WriteLine(myDir);

            //path to atlas
            string path = System.IO.Path.Combine(myDir, "Atlas.exe");

            ProcessStartInfo startInfo = new ProcessStartInfo(path);
            startInfo.Arguments = select;
            Process.Start(startInfo);
            this.Close();
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {

        }

        private void NewGame_FormClosed(object sender, FormClosedEventArgs e)
        {
            parent.exit();
        }

        private void NewGame_Load(object sender, EventArgs e)
        {

        }
    }
}
